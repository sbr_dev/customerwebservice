﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;

public partial class Report : System.Web.UI.Page
{
     //resolved exception of missing form tag when rendering user control
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		//This will hold all the data that we retrieve from the report.
        DataTable dtAllData = new DataTable();
        //  To Debug
        // Use the URL below.
        //  Comment out call to Reporting.ReportNotificationComplete() below

        //http://localhost/CustomerExtranetWebService/Report.aspx?ReportId=377&Email=michael.evanko@sunbeltrentals.com&ScheduleId=2628
        //http://customerextranetservice/Report.aspx?ReportId=3830&Email=michael.evanko@sunbeltrentals.com&ScheduleId=156786
        //http://localhost/CustomerExtranetWebService/Report.aspx?ReportId=393&Email=michael.evanko@sunbeltrentals.com&ScheduleId=411
        if (Request.QueryString["ReportId"] != null)
        {
			int ReportId = Convert.ToInt32(Request.QueryString["ReportId"]);
			string email = Convert.ToString(Request.QueryString["Email"]);
			int ScheduleId = Convert.ToInt32(Request.QueryString["ScheduleId"]);
			
            bool isStanleySlidingDateWeeklyReport = ReportInfo1.isStanleySlidingDateWeeklyReport(ReportId);
			
			string refId = Request.QueryString["referenceId"];
			Guid? referenceId = null;
			Guid tempGuid;
			if (Guid.TryParse(refId, out tempGuid))
				referenceId = tempGuid;

			try
			{
				DataTable dtUserReport = Reporting.GetUserReportByID(ReportId);
				if (dtUserReport.Rows.Count > 0)
				{
					Guid UserId = new Guid(dtUserReport.Rows[0]["UserID"].ToString());
					int AccountNumber = Convert.ToInt32(dtUserReport.Rows[0]["CustomerNumber"].ToString());
					bool IsCorpLink = Convert.ToBoolean(dtUserReport.Rows[0]["IsCorpLink"].ToString());

					DataTable dtUserReportColumns = Reporting.GetUserReportColumnsByUserReportID(ReportId);
					DataTable dtUserReportFilters = Reporting.GetUserReportFiltersByUserReportID(ReportId);

					bool blnShowTotals = Convert.ToBoolean(dtUserReport.Rows[0]["ShowTotals"].ToString());
					List<SqlParameter> pList = new List<SqlParameter>();
					string strSorting = "";

                foreach (DataRow dr in dtUserReportFilters.Rows)
                {
                    if (dr["Filter"].ToString() == "Range")
                    {
						if (dr["Filter_Range_Begin"].ToString() != "")
						{
							// DKI override
							if( ( AccountNumber == 401270 && (ReportId == 417 || ReportId == 6886 || ReportId == 6523))  ||  // 6523 is washburns for dki, 6886 evanko in production, 417 evanko qa 
                                                              ReportId == 13403 ||                                                                       // Corina Byrd sliding month reports
                                                              ReportId == 10968 )                                                                        

							{
								var today = DateTime.Today;
								var month = new DateTime(today.Year, today.Month, 1);
								var first = month.AddMonths(-1);
								var last = month.AddDays(-1);
								string lastMonthDateRange = "'" + first.ToShortDateString() + " 12:00:00 AM" + "' And '" + last.ToShortDateString() + " 11:59:59 PM'";
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", lastMonthDateRange));
							} 
							else if(isStanleySlidingDateWeeklyReport == true || ReportId == 14391)  // Corina ADM 422734 
							{
								var today = DateTime.Today;

								var oneWeekAgo = today.AddDays(-7);
								var oneWeekAgoSunday = oneWeekAgo.AddDays(-1 * (double)oneWeekAgo.DayOfWeek);
								var weekLater = new DateTime(oneWeekAgoSunday.Ticks);
								weekLater = weekLater.AddDays(6);
								string lastWeekDateRange = "'" + oneWeekAgoSunday.ToShortDateString() + " 12:00:00 AM" + "' And '" + weekLater.ToShortDateString() + " 11:59:59 PM'";
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", lastWeekDateRange));
							}
							else
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", dr["Filter_Range_Begin"].ToString()));


							}
						}
						else if (dr["Filter"].ToString() == "DaysBack")
						{
							if (dr["Filter_List"].ToString() != "")
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_DaysBack", dr["Filter_List"].ToString()));
						}


						else if (dr["Filter"].ToString() == "GreaterThan")
						{
							if (dr["Filter_Range_Begin"].ToString() != "")
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_GreaterThan", dr["Filter_Range_Begin"].ToString()));
						}

						else if (dr["Filter_List"].ToString() != "")
							pList.Add(new SqlParameter(dr["ColumnName"].ToString(), dr["Filter_List"].ToString()));
						else if (dr["Filter_Like"].ToString() != "")
							pList.Add(new SqlParameter(dr["ColumnName"].ToString(), dr["Filter_Like"].ToString()));
					}

					foreach (DataRow dr in dtUserReportColumns.Rows)
					{
						if (dr["SortIndex"].ToString() != "0")
						{
							if (strSorting == "")
								strSorting = "[" + dr["Header"].ToString() + "]";
							else
								strSorting += ",[" + dr["Header"].ToString() + "]";
						}
					}

					//Get the data for the report
					string sMessage = "";
					dtAllData = Reporting.GetUserReportData(ReportId, IsCorpLink, UserId, blnShowTotals, AccountNumber, pList, dtUserReportColumns, strSorting);
					dtAllData = Reporting.FormatDataTable(dtAllData);
					dtAllData = Reporting.ReorderColumns(dtAllData, dtUserReportColumns);
					sMessage += "<br/><br/>";

					ReportInfo1.SetReportInfo(ReportId);
					ReportInfo1.RecordCount = dtAllData.Rows.Count;

					StringWriter sw = new StringWriter();
					HtmlTextWriter htw = new HtmlTextWriter(sw);

					ReportInfo1.RenderControl(htw);
					sMessage += sw.ToString();

					StringWriter sw1 = new StringWriter();
					HtmlTextWriter htw1 = new HtmlTextWriter(sw1);

					bool emailReportEmbedded = false;
					bool emailReportAttached = true;
				bool IgnoreEmptyReports = false;

					try
					{
						DataTable dtFrequencyDetails = Reporting.GetUserReportFrequency(ReportId);
						if (dtFrequencyDetails.Rows.Count > 0)
						{
							emailReportEmbedded = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["EmailEmbedReport"].ToString());
							emailReportAttached = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["EmailAttachReport"].ToString());
						try{IgnoreEmptyReports = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["IgnoreEmptyReports"].ToString());} 
						catch{}
                    }
                }
                catch (Exception ex)
                {
                    Sunbelt.Log.LogError(ex, "CustomerExtranetWebService generated error");
                }

				Sunbelt.Log.LogActivity("Report", string.Format("emailEmbedReport:{0}, emailAttachReport:{1}, IgnoreEmptyReports:{2}", emailReportEmbedded, emailReportAttached, IgnoreEmptyReports));

					if (emailReportEmbedded == true) // Exception for LIN R ROGERS ELECTRICAL CNTRS. Embedded reports causing locking for cbradley@LRogersElectric.com
					{
						GridView gv = (GridView)ReportInfo1.FindControl("gvGridView");
						gv.DataSource = dtAllData;
						gv.DataBind();
						gv.RenderControl(htw1);
						sMessage = sMessage.Replace("ReportGridView", sw1.ToString());
					}

					//Replace invalid characters with underscores.
					string fileName = SBNet.PathSanitizer.SanitizeFilename(dtUserReport.Rows[0]["Title"].ToString(), '_');
					fileName = fileName.Replace(',', '_');
				if (IgnoreEmptyReports && dtAllData.Rows.Count == 0)
				{
					string msg = string.Format("Intended Recipient(s): {0}<br/>IgnoreEmptyReports flag: {1}<br/>Row Count: {2}<br/>", email, IgnoreEmptyReports, dtAllData.Rows.Count);
					sMessage = msg + sMessage;
					email = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
					Notification.EmailReportAsExcel(dtAllData, fileName, email, sMessage, emailReportAttached);

					//Mark the report complete.
					Reporting.ReportNotificationComplete(ScheduleId, "Email has been sent to " + email);
				}
				else
					throw new Exception("No report found");
			}
			catch (Exception ex)
			{
				Sunbelt.Log.LogError(ex, "Error in Report.aspx", referenceId);
				throw;
			}
        }
		else
			throw new Exception("No report id was passed");
    }
}

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertInfo.ascx.cs" Inherits="AlertInfo" %>
<%@ Register src="~/usercontrols/EquipmentOnRentPastDate.ascx" tagname="EquipmentOnRentPastDate" tagprefix="uc1" %>
<%@ Register src="~/UserControls/AnyNewInvoice.ascx" tagname="AnyNewInvoice" tagprefix="uc2" %>
<%@ Register src="~/UserControls/AnyInvoicePastDue.ascx" tagname="AnyInvoicePastDue" tagprefix="uc3" %>
<%@ Register src="~/UserControls/BillingPOMoreThan.ascx" tagname="BillingPOMoreThan" tagprefix="uc4" %>
<%@ Register src="~/UserControls/JobsitePickupOrDelivery.ascx" tagname="JobsitePickupOrDelivery" tagprefix="uc5" %>
<%@ Register src="UserControls/AnyNewReservation.ascx" tagname="AnyNewReservation" tagprefix="uc6" %>

<div style="font-family: Tahoma, Arial; font-size:smaller" ><asp:Literal ID="litMsg" runat="server" Text=""></asp:Literal></div>
<table id="tblAlertsDetail" runat="server" class="tblDetailInfo" cellpadding="0" cellspacing="8" border="0">
    <tr>
        <td colspan="2" style="vertical-align: top;">
            <table cellpadding="2" style="background-color: #EAE8E4; border: solid 1px #EBEBEB;">
                <tr id="trReference" runat="server">
                    <td style="font-family:Tahoma, Arial;"><asp:Label ID="lblReference" runat="server" Text="Reference Number:" Font-Bold="true"/></td>
                    <td style="font-family:Tahoma, Arial;"><asp:Label ID="lblReferenceValue" runat="server"></asp:Label></td>
                </tr>
                <tr id="trReferenceDetail" runat="server" style="vertical-align: top;">
                    <td style="font-family:Tahoma, Arial;"><asp:Label ID="lblReferenceDetail" runat="server" Text="" Font-Bold="true" /></td>
                    <td style="font-family:Tahoma, Arial;"><asp:Literal ID="lblReferenceDetailValue" runat="server" Text="" /></td>
                </tr>
                <tr id="trParameter" runat="server">
                     <td style="font-family: Tahoma, Arial;"><asp:Label ID="lblParameter" runat="server" Text="Parameter:" Font-Bold="true" />&nbsp;</td>
                     <td style="font-family: Tahoma, Arial;"><asp:Label ID="lblParameterValue" runat="server" /></td>
                </tr>
                <tr id="trCreatedBy" runat="server">
                    <td id="tdCreatedBy" runat="server" style="font-family: Tahoma, Arial;"><b>Created By:</b>&nbsp;</td>
                    <td style="font-family: Tahoma, Arial;"><asp:Label ID="lblCreatedBy" runat="server" Font-Names="Tahoma,Arial" /></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br />
<div style="font-family: Tahoma, Arial; width: 500px"><asp:Literal ID="litLowerMsg" runat="server" Text="" /></div>



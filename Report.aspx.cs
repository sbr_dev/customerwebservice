﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;

public partial class Report : System.Web.UI.Page
{
     //resolved exception of missing form tag when rendering user control
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		//This will hold all the data that we retrieve from the report.
        DataTable dtAllData = new DataTable();
        //  To Debug
        // Use the URL below.
        //  Comment out call to Reporting.ReportNotificationComplete() below
		//http://localhost/CustomerWebService/Report.aspx?ReportId=600&Email=michael.evanko@sunbeltrentals.com&ScheduleId=-1
        //http://localhost/CustomerExtranetWebService/Report.aspx?ReportId=377&Email=michael.evanko@sunbeltrentals.com&ScheduleId=2628
		//http://customerextranetservice/Report.aspx?ReportId=19511&Email=michael.evanko@sunbeltrentals.com&ScheduleId=-1
        //http://localhost/CustomerExtranetWebService/Report.aspx?ReportId=393&Email=michael.evanko@sunbeltrentals.com&ScheduleId=411
		//http://localhost/CustomerWebService/report.aspx?ReportId=523&Email=michael.evanko@sunbeltrentals.com&ScheduleId=7847
		//http://qacustomerwebservice/report.aspx?ReportId=497&Email=michaelx.evanko@sunbeltrentals.com&ScheduleId=7849
		//http://qacustomerwebservice/Report.aspx?ReportId=693&Email=michael.evanko@sunbeltrentals.com&ScheduleId=-1   // The Superior Group
        // new qa URL http://10.8.12.19:3008/report.aspx?ReportId=497&Email=michael.evanko@sunbeltrentals.com&ScheduleId=7849
		// Disney
		//http://localhost/CustomerExtranetWebService/Report.aspx?ReportId=76412&Email=michael.evanko@sunbeltrentals.com&ScheduleId=5416870

        if (Request.QueryString["ReportId"] != null)
        {
			int ReportId = Convert.ToInt32(Request.QueryString["ReportId"]);
			string email = Convert.ToString(Request.QueryString["Email"]);
			int ScheduleId = Convert.ToInt32(Request.QueryString["ScheduleId"]);
			
            bool isStanleySlidingDateWeeklyReport = ReportInfo1.isStanleySlidingDateWeeklyReport(ReportId);
			
			string refId = Request.QueryString["referenceId"];
			Guid? referenceId = null;
			Guid tempGuid;
			if (Guid.TryParse(refId, out tempGuid))
				referenceId = tempGuid;

			Stopwatch watch = new Stopwatch();
			watch.Start();

			try
			{
            DataTable dtUserReport = Reporting.GetUserReportByID(ReportId);
            if (dtUserReport.Rows.Count > 0)
            {
                Guid UserId = new Guid(dtUserReport.Rows[0]["UserID"].ToString());
                int AccountNumber = Convert.ToInt32(dtUserReport.Rows[0]["CustomerNumber"].ToString());
                bool IsCorpLink = Convert.ToBoolean(dtUserReport.Rows[0]["IsCorpLink"].ToString());
                int CompanyID = Convert.ToInt32(dtUserReport.Rows[0]["CompanyID"]);

                DataTable dtUserReportColumns = Reporting.GetUserReportColumnsByUserReportID(ReportId);
                DataTable dtUserReportFilters = Reporting.GetUserReportFiltersByUserReportID(ReportId);

				bool blnShowTotals = Convert.ToBoolean(dtUserReport.Rows[0]["ShowTotals"].ToString());
                List<SqlParameter> pList = new List<SqlParameter>();
                string strSorting = "";

                foreach (DataRow dr in dtUserReportFilters.Rows)
                {
                    if (dr["Filter"].ToString() == "Range")
                    {
						if (dr["Filter_Range_Begin"].ToString() != "")
						{
							pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", dr["RunTimeDateFilterRange"].ToString()));

#if false		
							// DKI override
							if( ( AccountNumber == 401270 && (ReportId == 417 || ReportId == 6886 || ReportId == 6523))  ||  // 6523 is washburns for dki, 6886 evanko in production, 417 evanko qa 
                                                              ReportId == 13403 ||                                                                       // Corina Byrd sliding month reports
                                                              ReportId == 10968 )                                                                        

							{
								var today = DateTime.Today;
								var month = new DateTime(today.Year, today.Month, 1);
								var first = month.AddMonths(-1);
								var last = month.AddDays(-1);
								string lastMonthDateRange = "'" + first.ToShortDateString() + " 12:00:00 AM" + "' And '" + last.ToShortDateString() + " 11:59:59 PM'";
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", lastMonthDateRange));
							} 
							else if(isStanleySlidingDateWeeklyReport == true || ReportId == 14391)  // Corina ADM 422734 
							{
								var today = DateTime.Today;

								var oneWeekAgo = today.AddDays(-7);
								var oneWeekAgoSunday = oneWeekAgo.AddDays(-1 * (double)oneWeekAgo.DayOfWeek);
								var weekLater = new DateTime(oneWeekAgoSunday.Ticks);
								weekLater = weekLater.AddDays(6);
								string lastWeekDateRange = "'" + oneWeekAgoSunday.ToShortDateString() + " 12:00:00 AM" + "' And '" + weekLater.ToShortDateString() + " 11:59:59 PM'";
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", lastWeekDateRange));
							}
							else
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_Range", dr["Filter_Range_Begin"].ToString()));
#endif

							}
						}
						else if (dr["Filter"].ToString() == "DaysBack")
						{
							if (dr["Filter_List"].ToString() != "")
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_DaysBack", dr["Filter_List"].ToString()));
						}


						else if (dr["Filter"].ToString() == "GreaterThan")
						{
							if (dr["Filter_Range_Begin"].ToString() != "")
								pList.Add(new SqlParameter(dr["ColumnName"].ToString() + "_GreaterThan", dr["Filter_Range_Begin"].ToString()));
						}

						else if (dr["Filter_List"].ToString() != "")
							pList.Add(new SqlParameter(dr["ColumnName"].ToString(), dr["Filter_List"].ToString()));
						else if (dr["Filter_Like"].ToString() != "")
							pList.Add(new SqlParameter(dr["ColumnName"].ToString(), dr["Filter_Like"].ToString()));
					}

					foreach (DataRow dr in dtUserReportColumns.Rows)
					{
						if (dr["SortIndex"].ToString() != "0")
						{
							if (strSorting == "")
								strSorting = "[" + dr["Header"].ToString() + "]";
							else
								strSorting += ",[" + dr["Header"].ToString() + "]";
						}
					}

                //Get the data for the report
                string sMessage = "";
                dtAllData = Reporting.GetUserReportData(ReportId, IsCorpLink, UserId, blnShowTotals, AccountNumber, pList, dtUserReportColumns, strSorting, CompanyID);
                dtAllData = Reporting.FormatDataTable(dtAllData);
                dtAllData = Reporting.ReorderColumns(dtAllData, dtUserReportColumns);
                sMessage += "<br/><br/>";

				//ReportInfo1.ReportID = ReportId;
				string filterText = ReportInfo1.SetReportInfo(ReportId);  // filters
                ReportInfo1.RecordCount = dtAllData.Rows.Count;

                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                ReportInfo1.RenderControl(htw);
                sMessage += sw.ToString();

                StringWriter sw1 = new StringWriter();
                HtmlTextWriter htw1 = new HtmlTextWriter(sw1);

                bool emailReportEmbedded = false;
                bool emailReportAttached = true;
				bool IgnoreEmptyReports = false;
				bool formatReportXML = false;

                try
                {
                    DataTable dtFrequencyDetails = Reporting.GetUserReportFrequency(ReportId);
                    if (dtFrequencyDetails.Rows.Count > 0)
                    {
                        emailReportEmbedded = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["EmailEmbedReport"].ToString());
                        emailReportAttached = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["EmailAttachReport"].ToString());
						try
						{
							IgnoreEmptyReports = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["IgnoreEmptyReports"].ToString());
							formatReportXML = Convert.ToBoolean(dtFrequencyDetails.Rows[0]["FormatReportXML"].ToString());
							//formatReportXML = true;
						} 
						catch{}
                    }
                }
                catch (Exception ex)
                {
                    Sunbelt.Log.LogError(ex, "CustomerExtranetWebService generated error");
                }

				Sunbelt.Log.LogActivity("Report", string.Format("emailEmbedReport:{0}, emailAttachReport:{1}, IgnoreEmptyReports:{2}, formatReportXML:{3}", emailReportEmbedded, emailReportAttached, IgnoreEmptyReports, formatReportXML));

                System.Text.StringBuilder sb = DataTableToCSV3(dtAllData);

                if (sb.Length > 1700000) //3456912
                {
                    // embedding large reports in the body is causing failures, just force an attachment.
                    emailReportEmbedded = false;
                    emailReportAttached = true;
                    Sunbelt.Log.LogActivity("Report", string.Format("Report Embedded Size Exceeds 2 megs, no embedding report, forcing attachment - Report: {0}, Schedule: {1}, email: {2}, Size {3}", ReportId, ScheduleId, email, sb.Length));
                }
                else
                    Sunbelt.Log.LogActivity("Report", string.Format("Report Embedded size good - {0}, Schedule: {1}, email: {2}, Size {3}", ReportId, ScheduleId, email, sb.Length));

                if (emailReportEmbedded == true) // Exception for LIN R ROGERS ELECTRICAL CNTRS. Embedded reports causing locking for cbradley@LRogersElectric.com
                {
                    GridView gv = (GridView)ReportInfo1.FindControl("gvGridView");
                    gv.DataSource = dtAllData;
                    gv.DataBind();
                    gv.RenderControl(htw1);
                    sMessage = sMessage.Replace("ReportGridView", sw1.ToString());
                }

				//Replace invalid characters with underscores.
				string fileName = SBNet.PathSanitizer.SanitizeFilename(dtUserReport.Rows[0]["Title"].ToString(), '_');
				fileName = fileName.Replace(',', '_');
				if (IgnoreEmptyReports && dtAllData.Rows.Count == 0)
				{
					string msg = string.Format("Intended Recipient(s): {0}<br/>IgnoreEmptyReports flag: {1}<br/>Row Count: {2}<br/>", email, IgnoreEmptyReports, dtAllData.Rows.Count);
					sMessage = msg + sMessage;
					email = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
					//email += ";mevanko@gmail.com";
				}
				string tempReportFullName = string.Empty;

				DataTable dtReportMetaData = dtUserReport.Copy();
				if (formatReportXML)
				{
					dtReportMetaData.TableName = "Report MetaData";

					dtReportMetaData.Columns.Add("Filters");
					dtReportMetaData.Rows[0]["Filters"] = filterText;

					dtReportMetaData.Columns.Add("UserReportID");
					dtReportMetaData.Rows[0]["UserReportID"] = ReportId.ToString();

					DataTable dtR = Reporting.GetReportById(Convert.ToInt32(dtUserReport.Rows[0]["ReportID"]));
					dtReportMetaData.Columns.Add("ReportName");
					dtReportMetaData.Rows[0]["ReportName"] = dtR.Rows[0]["ReportName"].ToString();

					dtReportMetaData.Columns.Add("DataRowType");
					dtReportMetaData.Rows[0]["DataRowType"] = dtR.Rows[0]["DataRowType"].ToString();

					DataTable dtReportUserInfo = Reporting.GetUserEmailAddress(UserId);

					dtReportMetaData.Columns.Add("UserReportOwner");
					dtReportMetaData.Rows[0]["UserReportOwner"] = dtReportUserInfo.Rows[0]["UserName"].ToString();

					dtReportMetaData.Columns.Add("RunDate");
					dtReportMetaData.Rows[0]["RunDate"] = DateTime.Now.ToString();
				}

                bool emailedReport = Notification.EmailReportAsExcel(dtAllData, fileName, email, sMessage, emailReportAttached, out tempReportFullName, dtReportMetaData, formatReportXML);
                if (emailedReport == false)     // error encountered, bale
                {
                    watch.Stop();
                        throw new Exception("EmailReportAsExcel Failed");

                        return;
                }
#if false
//No response from Tony Capalino and FTP started failing.
				try
				{
					if( ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true" )
					{
						var TheSuperiorGroupFTPReportIDs = new List<int> { 18630, 18631, 19109 }; // The Superior Group  19109 is evanko
						if (TheSuperiorGroupFTPReportIDs.Contains(ReportId))
						{
							ReportFTP reportFTP = new ReportFTP("ftp://70.61.47.205", "/ReportUploads/", "sunbelt", "j0BfpooiEtvr1zOBwQre", tempReportFullName);
							reportFTP.SendFile();
						}
					}
					else
					{
						var TheSuperiorGroupFTPReportIDs = new List<int> { 693, 694, 695, 696 };// The Superior Group
						if (TheSuperiorGroupFTPReportIDs.Contains(ReportId))
						{
							ReportFTP reportFTP1 = new ReportFTP("ftp://sunwebapp01.sunbeltrentals.com", "/invoiceReprint/", "as400", "Sun400", tempReportFullName);
							reportFTP1.SendFile();
							//ReportFTP reportFTP = new ReportFTP("ftp://70.61.47.205", "/ReportUploads/", "sunbelt", "j0BfpooiEtvr1zOBwQre", tempReportFullName);
							//reportFTP.SendFile();
						}
					}
				}
				catch (Exception ex)
				{
					Sunbelt.Email.SendEmail("michael.evanko@sunbeltrentals.com", "noreply@sunbeltrentals.com", "", "", "FTP FAILED for Report " + ReportId.ToString(), ex.Message, false);
				}
#endif
				// debug
				if (Request.QueryString["ScheduleId"] == "-1")
				{
					DataTable dtReport = Reporting.GetReportById(Convert.ToInt32(dtUserReport.Rows[0]["ReportId"]));

					string sp = dtReport.Rows[0]["ProcedureFullName"].ToString();
					string databaseCall = Reporting.getDatabaseCall(sp, pList, strSorting, AccountNumber, IsCorpLink, CompanyID);

					Sunbelt.Email.SendEmail("michael.evanko@sunbeltrentals.com", "noreply@sunbeltrentals.com", "", "", "Reports database call", databaseCall, false);
				}
				//Mark the report complete.
				watch.Stop();
				Reporting.ReportNotificationComplete(ScheduleId, "Email has been sent to " + email, watch.Elapsed.TotalSeconds );
            }
			else
				throw new Exception("No report found");
        }
		catch (Exception ex)
		{
				Sunbelt.Log.LogError(ex, "Error in Report.aspxx", referenceId);
				throw;
		}

	}
				else
			throw new Exception("No report id was passed");
    }
    static public System.Text.StringBuilder DataTableToCSV3(DataTable dt)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        for (int i = 0; i < dt.Columns.Count; i++)
        {
            sb.Append(FormatText3(dt.Columns[i].ColumnName));
            if (i < dt.Columns.Count - 1)
                sb.Append(",");
        }
        sb.Append(Environment.NewLine);

        if (dt.Rows.Count == 0)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append("There were no records returned!");
                if (i < dt.Columns.Count - 1)
                    sb.Append(",");
            }
            return sb;
        }

        for (int j = 0; j < dt.Rows.Count; j++)
        {
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append(FormatText3(dt.Rows[j][i].ToString().Trim()));
                if (i < dt.Columns.Count - 1)
                    sb.Append(",");
            }
            sb.Append(Environment.NewLine);
        }
        return sb;
    }
    static private string FormatText3(string text)
    {
        return (char)34 + text.Replace("\"", "\"\"") + (char)34;
    }
}

﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Sunbelt.Tools;
using Sunbelt;
using Sunbelt.Queue;

public partial class _Default : System.Web.UI.Page
{
    // resolved exception of missing form tag when rendering user control
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
		try
		{
			log("New Alert - AlertID ->" + Request.QueryString["AlertId"]);
			if (Request.QueryString["AlertId"] != null)
			{
				int AlertId = Convert.ToInt32(Request.QueryString["AlertId"]);
				string sMessage = "";//Convert.ToString(Request.QueryString["Message"]);
				string email = Convert.ToString(Request.QueryString["Email"]);
				int ScheduleId = Convert.ToInt32(Request.QueryString["ScheduleId"]);


				//  To test any alert locally:
				//  Uncomment these override values and update them to agree with a scheduled alerts in the alerts.schedulepending table
				//     AlertId = 416;
				//     ScheduleId = 953;
				//     email = "michael.evanko@sunbeltrentals.com";

				//  Make the default.aspx your default start-up page.
				//  Run these sql statments with the alert id to reset.  New alerts are trickier to get triggered and may not work.
				//     UPDATE Alerts.UserSettings SET LastRunDate=null, LastTriggeredDate=null WHERE alertid IN (371)
				//     DELETE Alerts.ScheduleHistory WHERE alertid IN (371)
				//     Alerts.sb_PrepareSchedulePending  '2010-01-20 09:00:00'

                //  Comment out the Alerts.AlertNotificationComplete() call towards the end of this function to rerun for testing.
				//  Run project which will exception error, but make sure url resemebles this 
				//     http://localhost/CustomerExtranetWebService/Default.aspx?AlertId=341&ScheduleId=621&Email=michael.evanko@sunbeltrentals.com

				// To test a reservation
				// http://localhost/CustomerExtranetWebService/reservations.aspx?Email=michael.evanko@sunbeltrentals.com&FilePath=\\\\SUNWEBAPPDEV\\InvoiceReprint\\24957854000.pdf&StrAcct=false&OrderId=1000699&RatePC=154
				if (email == null)
					log(string.Format("Alert Error - No email passed.   AlertID: {0}, ScheduleID: {1}, Email: {2} ", AlertId.ToString(), ScheduleId.ToString(), email));

				log(string.Format("Alert to process - AlertID: {0}, ScheduleID: {1}, Email: {2} ", AlertId.ToString(), ScheduleId.ToString(), email));

				// EVANKO:  New stuff.  Iterate over emailaddress list and send each one individually
				string[] sTo = email.Split(';');
				foreach (string whoTo in sTo)
				{
					AlertInfo AlertInfo2 = (AlertInfo)LoadControl("AlertInfo.ascx");

					AlertInfo2.SetAlertInfo(AlertId, whoTo, ScheduleId);

					DataTable dtAlertInfo = Alerts.GetAlertById(AlertId);
					int typeId = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["TypeID"], 0);
					string AlertName = ConvertTools.ToString(dtAlertInfo.Rows[0]["AlertName"]);
					string TypeName = ConvertTools.ToString(dtAlertInfo.Rows[0]["TypeName"]);

					StringWriter sw = new StringWriter();
					HtmlTextWriter htw = new HtmlTextWriter(sw);

					AlertInfo2.RenderControl(htw);
					sMessage = sw.ToString();

					string Subject = "Sunbelt Rentals Alert: " + AlertName;

					//if (typeId == (int)Alerts.TypeID.EquipmentOnRent)
					//    Subject = "Sunbelt Rentals Alert: " + AlertName;
					//else if (typeId == (int)Alerts.TypeID.InvoicePastDue)
					//    Subject = "Sunbelt Rentals Alert: " + AlertName;
					//else if (typeId == 3)
					//    Subject = "Sunbelt Rentals Alert: " + AlertName;
					//else if (typeId == 6)
					//    Subject = "Sunbelt Rentals Alert: Jobsite Monitor";
					//else if (typeId == (int)Alerts.TypeID.InvoiceNew)
					//    Subject = "Sunbelt Rentals Alert: " + AlertName;
					//else if (typeId == (int)Alerts.TypeID.ContractEquipmentOnRent)
					//    Subject = "Sunbelt Rentals Alert: " + AlertName;

					log(string.Format("Alert SendTo: {0}, Subject: {1}, ScheduleID: {2}", whoTo, Subject, ScheduleId.ToString()));

					Sunbelt.Email.SendEmail(whoTo
											 , ConfigurationManager.AppSettings["SunbeltEmail"].ToString()
											 , ""
											 , ConfigurationManager.AppSettings["SunbeltEmail"].ToString()
											 , Subject
											 , sMessage
											 , true);

					log(string.Format("Alert notification complete.  ScheduleID: {0}, {1}", ScheduleId.ToString(), whoTo));
				}
				Alerts.AlertNotificationComplete(ScheduleId, "Email has been sent to " + email);
			}
			else
			{
				log("Alert Error: No alert id was passed to process");
				throw new Exception("No alert id was passed");
			}
		}
		catch (Exception ex)
		{
			log("Alert :File Access Exception: " + ex.Message);
			throw ex;
		}
	}
    protected void Page_PreRender(object sender, EventArgs e)
    {

    }
	private void log(string details)
	{
		Sunbelt.Logger logfile = new Logger(@"c:\CustomerWebServiceAlertsAndReportsLog.txt", Logger.LogBy.WeeklyFile);
		logfile.Message(details);
		logfile.Update();
		logfile.Dispose();
	}

}

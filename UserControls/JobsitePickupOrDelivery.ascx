﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="JobsitePickupOrDelivery.ascx.cs" EnableTheming="true"  Inherits="UserControls_JobsitePickupOrDelivery" %>


<style type="text/css">
	.style1
	{
		height: 10px;
		font-family: Tahoma;
	}
	.style2
	{
		font-size: 11px;
		font-family: Tahoma;
	}
	.gridview
	{
		Font-Size:"XX-Small";
		font-family: Tahoma;
	}
</style>

<table id="tblJobsite" cellspacing="1" cellpadding="5" style="width: 100%; height: 100%;" runat="server">
	<tr >
		<td style="vertical-align: top;">
			<asp:Repeater ID="repJobsites" runat="server"  onitemdatabound="repJobsites_ItemDataBound">
				<ItemTemplate>
					<table style="background-color: #DCDCDC" runat="server">
						<tr >
							<td class="style2" valign="top"> <b>Job Site: </b></td>
							<td class="style2" ><asp:Label  ID="lblJobsite" runat="server" Text="lblJobsite" /></td>
						</tr>
						<tr id="trJobsiteDelivery" runat="server" >
							<td class="style2" valign="top"><b>Delivery: </b></td>
							<td>
								<asp:GridView CellPadding="2" ID="gvDelivery"  CssClass="gridview" EnableViewState="true" runat="server" AutoGenerateColumns="false">
									<EmptyDataTemplate>No Deliveries have been made</EmptyDataTemplate>
  									<Columns>
										<asp:BoundField HeaderText="Contract #" DataField="ContractNumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="75px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="PO Number" DataField="PONumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Equipment Type" DataField="EquipmentType">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="200px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Equipment #" DataField="EquipmentNumber">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Make" DataField="Make">
											<ItemStyle HorizontalAlign="Left" Width="85px" wrap="false" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Model" DataField="Model">
											<ItemStyle HorizontalAlign="Left" Width="120px"  wrap="false" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Ordered By" DataField="OrderedBy">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="Date Rented">
											<ItemTemplate>
												<%# Convert.ToDateTime(Eval("DateRented")).ToShortDateString() %>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
								<br />
							</td>
						</tr>
						<tr  id="trPickupID" runat="server">
							<td class="style2" valign="top"><b>Pickup: </b></td>
							<td>
								<asp:GridView CellPadding="2" ID="gvPickup"  CssClass="gridview" EnableViewState="true" runat="server" AutoGenerateColumns="false">
									<EmptyDataTemplate>No Pickup's have been requested</EmptyDataTemplate>
  									<Columns>
										<asp:BoundField HeaderText="Contract #" DataField="ContractNumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="75px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="PO Number" DataField="PONumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Equipment Type" DataField="EquipmentType">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="200px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
  										<asp:BoundField HeaderText="Equipment #" DataField="EquipmentNumber">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Make" DataField="Make">
											<ItemStyle HorizontalAlign="Left" Width="85px" wrap="false" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Model" DataField="Model">
											<ItemStyle HorizontalAlign="Left" Width="120px" />
										</asp:BoundField>
										<asp:TemplateField HeaderText="Date Rented">
											<ItemTemplate>
												<%# Convert.ToDateTime(Eval("DateRented")).ToShortDateString() %>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Pickup Ticket Number">
											<ItemTemplate>
												<%# Eval("PickupTicketNumber").ToString()%>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Pickup Ordered By">
											<ItemTemplate>
												<%# Eval("PickupOrderedBy").ToString()%>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Pickup Date">
											<ItemTemplate>
												<%# Eval("PickupDate").ToString() != "" ? Convert.ToDateTime(Eval("PickupDate")).ToShortDateString() : "Unknown"%>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</td>
						</tr>
					</table>
					<br />
				</ItemTemplate>
			</asp:Repeater>
		</td>
	</tr>
</table>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using SBNet.Extensions;
using System.Xml.Linq;
using System.IO;


public partial class UserControls_AnyNewReservation : System.Web.UI.UserControl
{
    private string TestXMLEquipment = "<Alert AlertProcedure=\"[Alerts].[sb_CheckNewReservations]\" RunTime=\"2010-08-19T13:31:03.303\" TriggeredTime=\"2010-08-19T13:31:03.303\" ItemCount=\"4\" TotalQuantity=\"4\" TotalQuantityReturned=\"0\">"
  + "<JobSite AccountNumber=\"411105\" JobNumber=\"EVANKO #2\" JobName=\"EVANKO #2\" JobAddress1=\"AT SUNBELT\" JobAddress2=\"\" JobCity=\"MORGANTON\" JobState=\"NC\" JobZIP=\"28655\">"
    + "<Equipment AccountNumber=\"411105\" ContractNumber=\"24958345\" CustomerPONumber=\"\" OrderedBy=\"EVANKO, MIKE\" ContractDateOut=\"2010-08-21T00:00:00\" DateOut=\"2010-08-21T00:00:00\" DaysOnRent=\"0\" ContractCreatedTime=\"2010-08-19T10:57:25\" DetailCreatedTime=\"2010-08-19T10:57:25\" JobName=\"EVANKO #2\" ContractLine=\"1\" EquipmentNumber=\"0480020\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"48\" Class=\"20\" ClassDescription=\"DINGO TRACK SKIDSTEER LOADER\" CatClassDisplayName=\"048-0020 DINGO TRACK SKIDSTEER LOADER\" />"
    + "<Equipment AccountNumber=\"411105\" ContractNumber=\"24958342\" CustomerPONumber=\"\" OrderedBy=\"EVANKO, MIKE\" ContractDateOut=\"2010-08-20T00:00:00\" DateOut=\"2010-08-20T00:00:00\" DaysOnRent=\"0\" ContractCreatedTime=\"2010-08-19T09:41:22\" DetailCreatedTime=\"2010-08-19T09:41:22\" JobName=\"EVANKO #2\" ContractLine=\"1\" EquipmentNumber=\"0700021\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"70\" Class=\"21\" ClassDescription=\"5 HP TILLER\" CatClassDisplayName=\"070-0021 5 HP TILLER\" />"
    + "<Equipment AccountNumber=\"411105\" ContractNumber=\"24958343\" CustomerPONumber=\"\" OrderedBy=\"EVANKO, MIKE\" ContractDateOut=\"2010-08-20T00:00:00\" DateOut=\"2010-08-20T00:00:00\" DaysOnRent=\"0\" ContractCreatedTime=\"2010-08-19T09:51:34\" DetailCreatedTime=\"2010-08-19T09:51:34\" JobName=\"EVANKO #2\" ContractLine=\"1\" EquipmentNumber=\"0700080\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"70\" Class=\"80\" ClassDescription=\"13HP HYD REAR TINE TILLER\" CatClassDisplayName=\"070-0080 13HP HYD REAR TINE TILLER\" />"
  + "  <Equipment AccountNumber=\"411105\" ContractNumber=\"24958344\" CustomerPONumber=\"\" OrderedBy=\"WEBTEMP\" ContractDateOut=\"2010-08-20T00:00:00\" DateOut=\"2010-08-20T00:00:00\" DaysOnRent=\"0\" ContractCreatedTime=\"2010-08-19T10:56:45\" DetailCreatedTime=\"2010-08-19T10:56:45\" JobName=\"EVANKO #2\" ContractLine=\"1\" EquipmentNumber=\"0480020\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"48\" Class=\"20\" ClassDescription=\"DINGO TRACK SKIDSTEER LOADER\" CatClassDisplayName=\"048-0020 DINGO TRACK SKIDSTEER LOADER\" />"
  + "</JobSite>"
  + "</Alert>";
		
	public int ContractNumber { set; get; }

	protected void Page_Load(object sender, EventArgs e)
    {
    }

	public void LoadAlert( int AlertID, int SchedulePendingID )
	{
		string AlertDetailsXML = string.Empty;

		try
		{
			if (SchedulePendingID == 0)
			{
				AlertDetailsXML = TestXMLEquipment;
			}
			else
			{
				DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

				if (dt.Rows.Count == 0)
					throw new Exception("No alert information found for scheduled ID item.");

				if (dt.Rows.Count > 0)
					AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();
			}

			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);
			var items = from item in xDoc.Descendants("JobSite")
						select new
						{
							JobName = item.Attribute("JobName") != null ? item.Attribute("JobName").Value : "",
							JobAddress1 = item.Attribute("JobAddress1") != null ? item.Attribute("JobAddress1").Value : "",
							JobCity = item.Attribute("JobCity") != null ? item.Attribute("JobCity").Value : "",
							JobState = item.Attribute("JobState") != null ? item.Attribute("JobState").Value : "",
							JobZIP = item.Attribute("JobZIP") != null ? item.Attribute("JobZIP").Value : "",
							Equipment = item.Elements("Equipment").ToArray()
						};

			//mike.Text = items.DumpCollection(true, 10);
			repJobsites.DataSource = items;
			repJobsites.DataBind();

			if (items.Distinct().Count() == 0)
			{
				//gvJobsiteEquipment.EmptyDataText = "No equipment movement found for jobsite.";
				//gvJobsiteEquipment.GridLines = GridLines.None;  // Turn off gridlines if nothing is returned.  Empty gridview message appears 3d raised.
				//gvJobsiteEquipment.DataSource = null;
			}
			else
			{
				DataTable dtAlertInfo = Alerts.GetAlertById(AlertID);
				int AccountNumber = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["AccountNumber"].ToString(), 0);
				bool IsCorpLink = ConvertTools.ToBoolean(dtAlertInfo.Rows[0]["IsCorpLinkAccount"].ToString(), false);
				int AlertTypeID = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["TypeID"].ToString(), 0);
				int CompanyID = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["CompanyID"].ToString(), 0);
				//lblJobsiteName.Text = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();
				//lblJobsiteAddr1.Text = cj.Address.Address1;
				//lblReferenceDetailValue.Text += "</td></tr><tr><td style='font-family: Arial;'>" + cj.Address.City + ", " + cj.Address.State + " " + cj.Address.Zip;
				string EquipmentNumber = string.Empty;
				int PC;
				int salesRepID;
				if (AlertTypeID == (int)Alerts.TypeID.ContractEquipmentOnRent)
				{
					ContractNumber = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["ReferenceValue"]);
					EquipmentList el = CustomerData.GetCustomerEquipmentByContract(AccountNumber, ContractNumber, IsCorpLink, CompanyID);
					//lblContract.Text = "&nbsp;for contract:&nbsp;" + ContractNumber.ToString();
					PC = el.First().PC;
					salesRepID = ((SalesRep)CustomerData.GetCustomerSalesRep(el.First().SalesRepID, CompanyID)).SalesRepID;
				}
				else
				{
					EquipmentNumber = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();
					EquipmentOnRent eor = CustomerData.GetCustomerEquipment(AccountNumber, EquipmentNumber, IsCorpLink, CompanyID);
					PC = eor.PC;
					salesRepID = eor.SalesRepID;
					//lblContract.Text = "";
				}
				ProfitCenter pc = CustomerData.GetProfitCenter(PC, CompanyID);
				SalesRep sr = CustomerData.GetCustomerSalesRep(salesRepID, CompanyID);
			}
		}
		catch (Exception ex)
		{
			Response.Write(ex.Message);
			//divStepsToTake.Visible = false;
			//gvJobsiteEquipment.DataSource = null;
			//gvJobsiteEquipment.GridLines = GridLines.None;
			//gvJobsiteEquipment.EmptyDataText = "Unexpected error encountered. " + ex.Message;
			//SBNet.Error.LogErrToSQL(ex, "JobsitePickupOrDelivery User Control");
			//gvJobsiteEquipment.EmptyDataRowStyle.ForeColor = System.Drawing.Color.Red;
			//gvJobsiteEquipment.DataBind();
		}
	}
	protected void repJobsites_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			Label lblJobsite = e.Item.FindControl("lblJobsite") as Label;
			lblJobsite.Text = DataBinder.Eval(e.Item.DataItem, "JobName").ToString();
			lblJobsite.Text += "<br/>" + DataBinder.Eval(e.Item.DataItem, "JobAddress1").ToString();
			lblJobsite.Text += "<br/>" + DataBinder.Eval(e.Item.DataItem, "JobCity").ToString()
							+ ", " + DataBinder.Eval(e.Item.DataItem, "JobState").ToString()
							+ " " + DataBinder.Eval(e.Item.DataItem, "JobZip").ToString();

			XElement[] vEL = (XElement[])DataBinder.Eval(e.Item.DataItem, "Equipment");

            //GridView gvPickup = e.Item.FindControl("gvPickup") as GridView;
            //var Pickup = from prd in vEL
            //             where prd.Attribute("PickupTicketNumber") != null
            //             select new
            //             {
            //                ContractNumber = (string)prd.Attribute("ContractNumber"),
            //                PONumber = prd.Attribute("CustomerPONumber") != null ? prd.Attribute("CustomerPONumber").Value : "",
            //                EquipmentType = prd.Attribute("CatClassDisplayName") != null ? prd.Attribute("CatClassDisplayName").Value : "",
            //                EquipmentNumber = (string)prd.Attribute("EquipmentNumber"),
            //                Make = prd.Attribute("EquipmentMake") != null ? prd.Attribute("EquipmentMake").Value : "",
            //                Model = prd.Attribute("EquipmentModel") != null ? prd.Attribute("EquipmentModel").Value.Trim() : "",
            //                Job = prd.Attribute("JobName") != null ? prd.Attribute("JobName").Value : "",
            //                DateRented = prd.Attribute("DateOut") != null ? prd.Attribute("DateOut").Value : "",
            //                PickupDate = prd.Attribute("PickupDate") != null ? prd.Attribute("PickupDate").Value : "",
            //                PickupTicketNumber = prd.Attribute("PickupTicketNumber") != null ? prd.Attribute("PickupTicketNumber").Value : "",
            //                PickupOrderedBy = prd.Attribute("PickupOrderedBy") != null ? prd.Attribute("PickupOrderedBy").Value : "" };

            //gvPickup.DataSource = Pickup;
			//gvPickup.DataBind();



			GridView gvDelivery = e.Item.FindControl("gvDelivery") as GridView;
			var JobsiteDeliveries = from prd in vEL
									 where prd.Attribute("PickupTicketNumber") == null
                                    orderby prd.Attribute("DetailCreatedTime").ToString() ascending
									 select new
									 {
										 ContractNumber = (string)prd.Attribute("ContractNumber"),
										 PONumber = prd.Attribute("CustomerPONumber") != null ? prd.Attribute("CustomerPONumber").Value : "",
										 EquipmentType = prd.Attribute("CatClassDisplayName") != null ? prd.Attribute("CatClassDisplayName").Value : "",
										 EquipmentNumber = (string)prd.Attribute("EquipmentNumber"),
										 Make = prd.Attribute("EquipmentMake") != null ? prd.Attribute("EquipmentMake").Value : "",
										 Model = prd.Attribute("EquipmentModel") != null ? prd.Attribute("EquipmentModel").Value.Trim() : "",
										 Job = prd.Attribute("JobName") != null ? prd.Attribute("JobName").Value : "",
										 OrderedBy = prd.Attribute("OrderedBy") != null ? prd.Attribute("OrderedBy").Value : "",
										 DateRented = prd.Attribute("DateOut") != null ? prd.Attribute("DateOut").Value : "",
                                         DetailCreatedTime = prd.Attribute("DetailCreatedTime") != null ? prd.Attribute("DetailCreatedTime").Value : ""
										 //PickupDate = prd.Attribute("PickupDate") != null ? prd.Attribute("PickupDate").Value : "",
										 //PickupTicketNumber = prd.Attribute("PickupTicketNumber") != null ? prd.Attribute("PickupTicketNumber").Value : "",
										 //PickupOrderedBy = prd.Attribute("PickupOrderedBy") != null ? prd.Attribute("PickupOrderedBy").Value : ""
									 };
			if(JobsiteDeliveries.Count() == 0)
			{
				//TableRow trJobsiteDelivery = e.Item.FindControl("repJobsites_ctl00_trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = Page.FindControl("repJobsites_ctl00_trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = e.Item.FindControl("trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = Page.FindControl("trJobsiteDelivery") as TableRow;
			}
			else
			{
               // gvDelivery.Sort("DetailCreatedTime", SortDirection.Descending);
                gvDelivery.DataSource = JobsiteDeliveries;
				gvDelivery.DataBind();
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;
using Sunbelt.BusinessObjects;
using Sunbelt.Tools;
using Sunbelt.Security;

public partial class EquipmentOnRentPastDate : System.Web.UI.UserControl
{
	private string TestXMLEquipment = "<Contract ItemCount=\"1\" TotalQuantity=\"1\" TotalQuantityReturned=\"0\"> <Equipment AccountNumber=\"119119\" ContractNumber=\"20369713\" DateOut=\"2009-05-04T00:00:00\" JobName=\"LONGWOOD HEATING PLANT\" ContractLine=\"1\" EquipmentNumber=\"50412\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"56\" Class=\"320\" ClassDescription=\"6000LB 36' SHOOTING BOOM FORKLIFT\" CatClassDisplayName=\"056-0320 6000LB 36' SHOOTING BOOM FORKLIFT\" EquipmentMake=\"JCB\" EquipmentModel=\"506C\" /></Contract>";

	public int ContractNumber{ set; get; }

	protected void Page_Load(object sender, EventArgs e)
    {
    }

	public void LoadAlert( int AlertID, int SchedulePendingID )
	{
		string AlertDetailsXML = string.Empty;

		try
		{

			if (SchedulePendingID == 0)
			{
				AlertDetailsXML = TestXMLEquipment;
			}
			else
			{
				DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

				if (dt.Rows.Count == 0)
					throw new Exception("No alert information found for scheduled ID item.");

				if (dt.Rows.Count > 0)
					AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();
			}


			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);
			var items = from item in xDoc.Descendants("Equipment")
						select new
						{
							EquipmentNumber = item.Attribute("EquipmentNumber") != null ? item.Attribute("EquipmentNumber").Value : "",
							EquipmentType = item.Attribute("CatClassDisplayName") != null ? item.Attribute("CatClassDisplayName").Value : "",
							Make = item.Attribute("EquipmentMake") != null ? item.Attribute("EquipmentMake").Value : "",
							Model = item.Attribute("EquipmentModel") != null ? item.Attribute("EquipmentModel").Value : "",
							Job = item.Attribute("JobName") != null ? item.Attribute("JobName").Value : "",
							DateRented = item.Attribute("DateOut") != null ? item.Attribute("DateOut").Value : "",
							ContractNumber = item.Attribute("ContractNumber") != null ? item.Attribute("ContractNumber").Value : "",
							PONumber = item.Attribute("CustomerPONumber") != null ? item.Attribute("CustomerPONumber").Value : "",
							DaysOnRent = item.Attribute("DaysOnRent") != null ? item.Attribute("DaysOnRent").Value : ""
						};

			gvEquipmentOnRent.DataSource = items;
			gvEquipmentOnRent.DataBind();

			if (items.Distinct().Count() == 0)
			{
				gvEquipmentOnRent.EmptyDataText = "No equipment found for contract.";
				gvEquipmentOnRent.GridLines = GridLines.None;  // Turn off gridlines if nothing is returned.  Empty gridview message appears 3d raised.
			}
			else
			{
				DataTable dtAlertInfo = Alerts.GetAlertById(AlertID);
				int AccountNumber = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["AccountNumber"].ToString(), 0);
                bool IsCorpLink = Sunbelt.Tools.ConvertTools.ToBoolean(dtAlertInfo.Rows[0]["IsCorpLinkAccount"].ToString(), false);
				int AlertTypeID = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["TypeID"].ToString(), 0);
				int CompanyID = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["CompanyID"].ToString(), 0);

				string EquipmentNumber = string.Empty;
				int PC = 0;
				int salesRepID =0;
				if (AlertTypeID == (int)Alerts.TypeID.ContractEquipmentOnRent)
				{
					lblOnRentPast.Text = "Alert details for Equipment on Rent on or past date " + dtAlertInfo.Rows[0]["ParameterValue"].ToString();
					ContractNumber = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["ReferenceValue"]);
					EquipmentList el = CustomerData.GetCustomerEquipmentByContract(AccountNumber, ContractNumber, IsCorpLink, CompanyID);
					lblOnRentPast.Text += "&nbsp;for contract:&nbsp;" + ContractNumber.ToString();
					PC = el.First().PC;
					salesRepID = ((SalesRep)CustomerData.GetCustomerSalesRep(el.First().SalesRepID, CompanyID)).SalesRepID;
				}
				else if( AlertTypeID == (int)Alerts.TypeID.EquipmentOnRent )
				{
					EquipmentNumber = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();
					EquipmentOnRent eor = CustomerData.GetCustomerEquipment(AccountNumber, EquipmentNumber, IsCorpLink, CompanyID);
					PC = eor.PC;
					salesRepID = eor.SalesRepID;
					lblOnRentPast.Text = "Alert details for Equipment on Rent on or past date " + dtAlertInfo.Rows[0]["ParameterValue"].ToString();
					trSaleRepAndLocationDetails.Visible = false;
					trSaleRepAndLocationDetailsHeading.Visible = false;
					lblOrCall.Text = ".";	
				}
				else if (AlertTypeID == (int)Alerts.TypeID.EquipmentOnRentPastDays)
				{
					string numberOfDays = dtAlertInfo.Rows[0]["ParameterValue"].ToString();
					lblOnRentPast.Text = "Alert details for Equipment on Rent on or past " + numberOfDays + (Convert.ToInt32(numberOfDays) > 1 ? " days" : " day");
					trSaleRepAndLocationDetails.Visible = false;
					trSaleRepAndLocationDetailsHeading.Visible = false;
					lblOrCall.Text = ".";	
				}
                else if (AlertTypeID == (int)Alerts.TypeID.ReservationEquipmentOnRent)
                {
                    string numberOfDays = dtAlertInfo.Rows[0]["ParameterValue"].ToString();
                    lblOnRentPast.Text = "Alert details for Equipment on rent within 2 Days of Estimate Return Date";
                    trSaleRepAndLocationDetails.Visible = false;
                    trSaleRepAndLocationDetailsHeading.Visible = false;
                    lblOrCall.Text = ".";
                }



				ProfitCenter pc = CustomerData.GetProfitCenter(PC, CompanyID);
				SalesRep sr = CustomerData.GetCustomerSalesRep(salesRepID, CompanyID);

				if (pc != null && sr != null )
				{

					lblSalesRepName.Text = sr.FirstName + " " + sr.LastName;
					lblSalesRepPhone.Text = sr.Phone;
					lblSalesRepEmail.Text = sr.Email;
					PCName.Text = pc.PCName;
					PCBranch.Text = "Branch #" + pc.PC.ToString();
					PCAddr.Text = pc.Address.Address1;
					PCCityStateZIP.Text = pc.Address.City.Trim() + ", " + pc.Address.State + " " + pc.Address.Zip;
					if( pc.PhoneNumber != null && pc.PhoneNumber != "" )
						PCCityStateZIP.Text += "<br/>" + pc.PhoneNumber;
				}
				else
				{
					lblSalesRepName.Text = "Information currently";
					lblSalesRepPhone.Text = "unavailable";

					PCName.Text = "Information currently ";
					PCBranch.Text = "unavailable";
				}
			}
		}
		catch (Exception ex)
		{
			divStepsToTake.Visible = false;
			gvEquipmentOnRent.DataSource = null;
			gvEquipmentOnRent.GridLines = GridLines.None;
			gvEquipmentOnRent.EmptyDataText = "Unexpected error encountered. " + ex.Message;

            Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control");
			gvEquipmentOnRent.EmptyDataRowStyle.ForeColor = System.Drawing.Color.Red;
			gvEquipmentOnRent.DataBind();
		}
	 
	}
	
}

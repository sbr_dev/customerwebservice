﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReservationRequestEmail.ascx.cs" Inherits="ReservationRequestEmail" %>
<%
    string CustomerServiceEmail = string.Empty;
    if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true")
        CustomerServiceEmail = "commandcenter@sunbeltrentals.com";
    else
        CustomerServiceEmail = "qanoreply@sunbeltrentals.com";
%>
<head>
    <title>Reservation Confirmation</title>
	<style type="text/css">
		
		BODY { font-family: Tahoma, Verdana, Narrow, Sans-Serif; font-size: 8pt; }

		.style3
		{
			height: 40px;
		}

		table.ResConformation 
		{
			font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
			background-color:#F2F8F1;
		}

		td.Label 
		{	
			font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
						            width: 188px;

		}
		td.data 
		{	
			width: 72px;
			font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
		}
		td.infoHeader 
		{	
			border-left: 1px solid White;
			padding-left:1px;
			color:White;
			font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
			background-color: #4A843D;

		}
		.info_Left 
{	
	color:Black;
	font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
}		
.info_Right
{	
	color:Black;
	font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
}	
	</style>
</head>

<div style="font-family: Tahoma, Verdana, Narrow, Sans-Serif; padding-left: 10px; width: 630px;">


<table width="800px" class="ResConformation" border="0" cellpadding="0" cellspacing="0" style="background-color:#F2F8F1;" >
<tr>
	<td style="width:800px">
<table width="800px" border="0">
		<tr>
			<td style="width:800px"><span style="font-size: 10pt; font-weight: bold; height:16px;" valign="bottom">Sunbelt Rentals Inc.</span>
				 <br />										   
				 <span style="font-size: 14pt; color:#666666;">Reservation Confirmation</span>
			</td>
			<td style="width:100px; vertical-align:top; text-align:right">
				<span><img id="SunbeltLogo" alt="Sunbelt Rentals"  src="http://customer.sunbeltrentals.com/Images/vert_sunbelt_logo.png" /></span>
			</td>
		</tr>
<tr valign="top">
	<td colspan="2"><hr size="2"  noshade="true"  color="Black"  width="100%"/></td>
</tr>
		</table>
	</td>
</tr>
<tr>
	<td style="height:30px;">
		<p><asp:Label ID="lblRequestorName" runat="server" Visible="false" /> Thank you for submitting your Reservation to Sunbelt Rentals.</p>
	</td>
</tr>
<tr>
	<td style="height:30px; padding-bottom:5px; width:730px">
		<p>Your reservation has been received by Sunbelt Rentals and is currently being processed. A member of our team will contact you within <b>2 business hours</b> with confirmation of your reservation.</p>
	</td>
</tr>
<tr>
    <td>
        <table style=" font-family: Tahoma, Verdana, Narrow, Sans-Serif; width: 700px;" 
            border="0" cellpadding="1px" cellspacing="1px">
            <tr>
                <td  class="infoHeader" colspan="2" style="width:340px;background-color:#4A843D; color:#ffffff; font-weight:bold; height: 20px; padding-left: 1px; ">Reservation Information</td>
		        <td class="infoHeader" style="width:399px; background-color:#4A843D; color:White; font-weight:bold" >Customer Information</td>
            </tr>
            <tr>
                <td style="width: 130px;"><b>Reservation&nbsp;Number:</b>&nbsp;</td>
                <td class="data" align="left" style="font-weight:bold; width: 160px;">[RESERVATION_NUMBER]</td>
                <td class="data"><b>[CUSTOMER_ACCOUNT_NAME]</b></td>
            </tr>
            <tr>
                <td style="width: 110px;"><b>Rental&nbsp;Start&nbsp;Date:</b></td>
                <td class="data"align="left" style="width: 160px">[RENTAL_START_DATE]</td>
                <td class="data">[CUSTOMER_ADDRESS1-2]</td>
            </tr>
            <tr>
                <td style="width: 110px;"><b>Rental&nbsp;Return&nbsp;Date:</b></td>
                <td class="data" align="left" style="width: 160px">[RENTAL_RETURN_DATE]</td>
                <td class="data">[CUSTOMER_CITY_STATE_ZIP]</td>
            </tr>
            <tr>
                <td style="width: 110px;"><b>Order&nbsp;Date:</b></td>
                <td class="data"align="left" style="width: 160px">[RENTAL_ORDER_DATE]</td>
                <td class="data">[CUSTOMER_PHONE]</td>
            </tr>
            <tr>
                <td colspan="3" align="left">
                <br />                    
					<asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" 
                        style="margin-right: 124px; " Width="584px">
                        <HeaderStyle CssClass="infoHeader" BackColor="#4A843D" ForeColor="White" BorderColor="White" 
                            Font-Size="9pt" Font-Names="Tahoma, Verdana, Narrow, Sans-Serif" 
                            HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="1px" />
                        <RowStyle Font-Names="Tahoma, Verdana, Narrow, Sans-Serif" Font-Size="9pt" />
                        <Columns>
							<asp:TemplateField>
                                <HeaderTemplate>&nbsp;Qty&nbsp;</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblQty" runat="server" Font-Names="Arial" Text='<%#Eval("Quantity")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                                <ItemStyle Width="18%" HorizontalAlign="Center"  BorderColor="#F2F8F1"/>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>Rental Item</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblTitle" runat="server" Font-Names="Arial" Text='<%#Eval("Title")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                                <ItemStyle Width="82%" HorizontalAlign="Left"  BorderColor="#F2F8F1"/>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
				</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td style=" padding-top: 15px; width:730px" >
        Quoted delivery and pick up fees are estimates and provided for convenience only. Actual delivery charges will be quoted when the order is confirmed based on the rental location.
        
    </td>
</tr>
<tr>
    <td style="padding-top:15px; width:630px" class="style3">
		<p class='Text'  style="color:Gray; font-size:smaller">
			If you have any questions or you have received this message in error, please contact Sunbelt Rentals Customer Service
		 		 toll free at <a href="tel:1-866-786-2358">1-866-SUNBELT</a> or by email <%=@CustomerServiceEmail %>.&nbsp;&nbsp;&nbsp;Please refer to your reservation, <b><asp:Label ID="lblReservationNumber2" runat="server" /></b>, when contacting us.
</p>
    </td>
</tr>
<tr>
    <td style="height: 30px;"></td>
</tr>
<tr>
    <td style="color:#FF0000; text-align: center; font: Tahoma, Verdana, Narrow, Sans-Serif 12px; vertical-align:bottom; ">
        ** Note: This is an automated email. Do no reply to this email **
    </td>
</tr>
<tr>
    <td style="text-align:right;"><a href="http://www.adobe.com/products/acrobat/readstep2.html" ><img src="http://customer.sunbeltrentals.com/Images/get_adobe_reader.gif" border="0" /></a></td>
</tr>
</table>
</div>
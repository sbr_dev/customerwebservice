﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnyNewReservation.ascx.cs" EnableTheming="true"  Inherits="UserControls_AnyNewReservation" %>


<style type="text/css">
	.style1
	{
		height: 10px;
		font-family: Tahoma;
	}
	.style2
	{
		font-size: 11px;
		font-family: Tahoma;
	}
	.gridview
	{
		Font-Size:"XX-Small";
		font-family: Tahoma;
	}
</style>

<table id="tblJobsite" cellspacing="1" cellpadding="5" style="width: 100%; height: 100%;" runat="server">
	<tr >
		<td style="vertical-align: top;">
			<asp:Repeater ID="repJobsites" runat="server"  onitemdatabound="repJobsites_ItemDataBound">
				<ItemTemplate>
					<table style="background-color: #DCDCDC" runat="server" border="0">
						<tr >
							<td colspan="2" >
                            <table><tr>
							<td class="style2" valign="top"> <b>Job Site: </b></td>
							<td class="style2" ><asp:Label  ID="lblJobsite" runat="server" Text="lblJobsite" /></td>
							</tr></table>						    
							</td>
						</tr>
						<tr id="trJobsiteDelivery" runat="server" >
							<td colspan="2" >
								<asp:GridView CellPadding="2" AllowSorting="true" ID="gvDelivery"  CssClass="gridview" EnableViewState="true" runat="server" AutoGenerateColumns="false">
									<EmptyDataTemplate>No reservations have been made</EmptyDataTemplate>
  									<Columns>
                                        <asp:TemplateField HeaderText="Date&nbsp;Ordered">
											<ItemTemplate>
												<%# 
				                                    String.Format("{0:ddd}", Convert.ToDateTime(Eval("DetailCreatedTime"))) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  hh:mm tt}", Convert.ToDateTime(Eval("DetailCreatedTime"))).ToLower()
				
												    %>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="left" Width="130px" />
										</asp:TemplateField>

										<asp:BoundField HeaderText="Equipment&nbsp;Type" DataField="EquipmentType">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="200px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
										<asp:BoundField HeaderText="Ordered&nbsp;By" DataField="OrderedBy">
											<ItemStyle HorizontalAlign="Left"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
 										<asp:TemplateField HeaderText="Date Rented">
											<ItemTemplate>
												<%# Convert.ToDateTime(Eval("DateRented")).ToShortDateString() %>
											</ItemTemplate>
											<ItemStyle HorizontalAlign="Center" Width="100px" />
										</asp:TemplateField>
                                        <asp:BoundField HeaderText="PO&nbsp;#" DataField="PONumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="100px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>

										<asp:BoundField HeaderText="Reservation&nbsp;#" DataField="ContractNumber">
											<ItemStyle HorizontalAlign="Center"  Wrap="false" Width="75px"  />
											<HeaderStyle HorizontalAlign="Center" />
										</asp:BoundField>
									</Columns>
								</asp:GridView>
								<br />
							</td>
						</tr>
					</table>
					<br />
				</ItemTemplate>
			</asp:Repeater>
		</td>
	</tr>
</table>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using SBNet.Extensions;
using System.Xml.Linq;
using System.IO;


public partial class UserControls_JobsitePickupOrDelivery : System.Web.UI.UserControl
{
	private string TestXMLEquipment = "<Contract AlertProcedure=\"[Alerts].[sb_CheckJobsiteActivity]\" TriggeredTime=\"2009-11-20T11:51:06.880\" ItemCount=\"5\" TotalQuantity=\"5\" TotalQuantityReturned=\"0\">" +
  "<JobSite AccountNumber=\"4\" JobNumber=\"1 - AMECO ** DUKE PO\" JobName=\"AMECO ** DUKE POWER\" JobAddress1=\"3195 PINE HALL RD\" JobAddress2=\"DUKE POWER\" JobCity=\"BELEWS CREEK\" JobState=\"NC\" JobZIP=\"27009\">" +
    "<Equipment AccountNumber=\"4\" ContractNumber=\"20013581\" OrderedBy=\"\" DateOut=\"2008-04-09T00:00:00\" JobName=\"AMECO ** DUKE POWER\" ContractLine=\"1\" EquipmentNumber=\"S210601\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"22\" Class=\"250\" ClassDescription=\"54&quot; PADFOOT RIDE-ON ROLLER DIESEL\" CatClassDisplayName=\"022-0250 54&quot; PADFOOT RIDE-ON ROLLER DIESEL\" EquipmentMake=\"HAMM\" EquipmentModel=\"2220D\" PickupTicketNumber=\"1154880\" PickupDate=\"2009-11-20T00:00:00\" PickupOrderedBy=\"EVANKO, MIKE\" />" +
    "<Equipment AccountNumber=\"4\" ContractNumber=\"20013894\" OrderedBy=\"\" DateOut=\"2008-05-07T00:00:00\" JobName=\"AMECO ** DUKE POWER\" ContractLine=\"1\" EquipmentNumber=\"145319\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"107\" Class=\"110\" ClassDescription=\"1 TON AIR CONDITIONER SPOT COOLER 110V\" CatClassDisplayName=\"107-0110 1 TON AIR CONDITIONER SPOT COOLER 110V\" EquipmentMake=\"MOBILE AIR\" EquipmentModel=\"OFFICE PRO-18\" PickupTicketNumber=\"1154879\" PickupDate=\"2009-11-19T08:00:00\" PickupOrderedBy=\"G, A\" />" +
 "</JobSite>" +
  "<JobSite AccountNumber=\"4\" JobNumber=\"1 - AMECO ** DUKE PO\" JobName=\"Evanko's ** DUKE POWER\" JobAddress1=\"3195 PINE HALL RD\" JobAddress2=\"DUKE POWER\" JobCity=\"BELEWS CREEK\" JobState=\"NC\" JobZIP=\"27009\">" +
    "<Equipment AccountNumber=\"4\" ContractNumber=\"12579599\" OrderedBy=\"AMECO, ROBIN\" DateOut=\"2007-10-11T00:00:00\" JobName=\"AMECO ** DUKE POWER\" ContractLine=\"1\" EquipmentNumber=\"463629\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"56\" Class=\"620\" ClassDescription=\"10,000LB 55' SHOOTING BOOM FORKLIFT\" CatClassDisplayName=\"056-0620 10,000LB 55' SHOOTING BOOM FORKLIFT\" EquipmentMake=\"LULL\" EquipmentModel=\"1044C-54 PKG4\" PickupTicketNumber=\"1154875\" PickupDate=\"2009-11-19T00:30:00\" PickupOrderedBy=\"G, A\" />" +
    "<Equipment AccountNumber=\"4\" ContractNumber=\"20013884\" OrderedBy=\"\" DateOut=\"2008-05-07T00:00:00\" JobName=\"AMECO ** DUKE POWER\" ContractLine=\"1\" EquipmentNumber=\"145317\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"107\" Class=\"110\" ClassDescription=\"1 TON AIR CONDITIONER SPOT COOLER 110V\" CatClassDisplayName=\"107-0110 1 TON AIR CONDITIONER SPOT COOLER 110V\" EquipmentMake=\"MOBILE AIR\" EquipmentModel=\"OFFICE PRO-18\" PickupTicketNumber=\"1154878\" PickupDate=\"2009-11-19T00:30:00\" PickupOrderedBy=\"G, A\" />" +
    "<Equipment AccountNumber=\"4\" ContractNumber=\"22389481\" OrderedBy=\"HANNATH, NEAL\" DateOut=\"2009-11-19T00:00:00\" JobName=\"AMECO ** DUKE POWER\" ContractLine=\"1\" EquipmentNumber=\"S34752\" Quantity=\"1\" QuantityReturned=\"0\" Cat=\"1\" Class=\"170\" ClassDescription=\"1300CFM 150PSI DIESEL AIR COMPRESSOR\" CatClassDisplayName=\"001-0170 1300CFM 150PSI DIESEL AIR COMPRESSOR\" EquipmentMake=\"SULLAIR\" EquipmentModel=\"1300H DTQ\" />" +
  "</JobSite>" +
"</Contract>";
		
	public int ContractNumber { set; get; }

	protected void Page_Load(object sender, EventArgs e)
    {
    }

	public void LoadAlert( int AlertID, int SchedulePendingID )
	{
		string AlertDetailsXML = string.Empty;

		try
		{
			if (SchedulePendingID == 0)
			{
				AlertDetailsXML = TestXMLEquipment;
			}
			else
			{
				DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

				if (dt.Rows.Count == 0)
					throw new Exception("No alert information found for scheduled ID item.");

				if (dt.Rows.Count > 0)
					AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();
			}

			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);
			var items = from item in xDoc.Descendants("JobSite")
						select new
						{
							JobName = item.Attribute("JobName") != null ? item.Attribute("JobName").Value : "",
							JobAddress1 = item.Attribute("JobAddress1") != null ? item.Attribute("JobAddress1").Value : "",
							JobCity = item.Attribute("JobCity") != null ? item.Attribute("JobCity").Value : "",
							JobState = item.Attribute("JobState") != null ? item.Attribute("JobState").Value : "",
							JobZIP = item.Attribute("JobZIP") != null ? item.Attribute("JobZIP").Value : "",
							Equipment = item.Elements("Equipment").ToArray()
						};

			//mike.Text = items.DumpCollection(true, 10);
			repJobsites.DataSource = items;
			repJobsites.DataBind();

			if (items.Distinct().Count() == 0)
			{
				//gvJobsiteEquipment.EmptyDataText = "No equipment movement found for jobsite.";
				//gvJobsiteEquipment.GridLines = GridLines.None;  // Turn off gridlines if nothing is returned.  Empty gridview message appears 3d raised.
				//gvJobsiteEquipment.DataSource = null;
			}
			else
			{
				DataTable dtAlertInfo = Alerts.GetAlertById(AlertID);
				int AccountNumber = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["AccountNumber"].ToString(), 0);
				bool IsCorpLink = ConvertTools.ToBoolean(dtAlertInfo.Rows[0]["IsCorpLinkAccount"].ToString(), false);
				int AlertTypeID = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["TypeID"].ToString(), 0);
				int CompanyID = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["CompanyID"].ToString(), 0);
				//lblJobsiteName.Text = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();
				//lblJobsiteAddr1.Text = cj.Address.Address1;
				//lblReferenceDetailValue.Text += "</td></tr><tr><td style='font-family: Arial;'>" + cj.Address.City + ", " + cj.Address.State + " " + cj.Address.Zip;
				string EquipmentNumber = string.Empty;
				int PC;
				int salesRepID;
				if (AlertTypeID == (int)Alerts.TypeID.ContractEquipmentOnRent)
				{
					ContractNumber = Sunbelt.Tools.ConvertTools.ToInt32(dtAlertInfo.Rows[0]["ReferenceValue"]);
					EquipmentList el = CustomerData.GetCustomerEquipmentByContract(AccountNumber, ContractNumber, IsCorpLink, CompanyID);
					//lblContract.Text = "&nbsp;for contract:&nbsp;" + ContractNumber.ToString();
					PC = el.First().PC;
					salesRepID = ((SalesRep)CustomerData.GetCustomerSalesRep(el.First().SalesRepID, CompanyID)).SalesRepID;
				}
				else
				{
					EquipmentNumber = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();
					EquipmentOnRent eor = CustomerData.GetCustomerEquipment(AccountNumber, EquipmentNumber, IsCorpLink, CompanyID);
					PC = eor.PC;
					salesRepID = eor.SalesRepID;
					//lblContract.Text = "";
				}
				ProfitCenter pc = CustomerData.GetProfitCenter(PC, CompanyID);
				SalesRep sr = CustomerData.GetCustomerSalesRep(salesRepID, CompanyID);
			}
		}
		catch (Exception ex)
		{
			Response.Write(ex.Message);
			//divStepsToTake.Visible = false;
			//gvJobsiteEquipment.DataSource = null;
			//gvJobsiteEquipment.GridLines = GridLines.None;
			//gvJobsiteEquipment.EmptyDataText = "Unexpected error encountered. " + ex.Message;
			//SBNet.Error.LogErrToSQL(ex, "JobsitePickupOrDelivery User Control");
			//gvJobsiteEquipment.EmptyDataRowStyle.ForeColor = System.Drawing.Color.Red;
			//gvJobsiteEquipment.DataBind();
		}
	}
	protected void repJobsites_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
		{
			Label lblJobsite = e.Item.FindControl("lblJobsite") as Label;
			lblJobsite.Text = DataBinder.Eval(e.Item.DataItem, "JobName").ToString();
			lblJobsite.Text += "<br/>" + DataBinder.Eval(e.Item.DataItem, "JobAddress1").ToString();
			lblJobsite.Text += "<br/>" + DataBinder.Eval(e.Item.DataItem, "JobCity").ToString()
							+ ", " + DataBinder.Eval(e.Item.DataItem, "JobState").ToString()
							+ " " + DataBinder.Eval(e.Item.DataItem, "JobZip").ToString();

			XElement[] vEL = (XElement[])DataBinder.Eval(e.Item.DataItem, "Equipment");

			GridView gvPickup = e.Item.FindControl("gvPickup") as GridView;
			var Pickup = from prd in vEL
                         where prd.Attribute("PickupTicketNumber") != null
						 select new
						 {
							ContractNumber = (string)prd.Attribute("ContractNumber"),
							PONumber = prd.Attribute("CustomerPONumber") != null ? prd.Attribute("CustomerPONumber").Value : "",
							EquipmentType = prd.Attribute("CatClassDisplayName") != null ? prd.Attribute("CatClassDisplayName").Value : "",
							EquipmentNumber = (string)prd.Attribute("EquipmentNumber"),
							Make = prd.Attribute("EquipmentMake") != null ? prd.Attribute("EquipmentMake").Value : "",
							Model = prd.Attribute("EquipmentModel") != null ? prd.Attribute("EquipmentModel").Value.Trim() : "",
							Job = prd.Attribute("JobName") != null ? prd.Attribute("JobName").Value : "",
							DateRented = prd.Attribute("DateOut") != null ? prd.Attribute("DateOut").Value : "",
							PickupDate = prd.Attribute("PickupDate") != null ? prd.Attribute("PickupDate").Value : "",
							PickupTicketNumber = prd.Attribute("PickupTicketNumber") != null ? prd.Attribute("PickupTicketNumber").Value : "",
							PickupOrderedBy = prd.Attribute("PickupOrderedBy") != null ? prd.Attribute("PickupOrderedBy").Value : "" };

			gvPickup.DataSource = Pickup;
			gvPickup.DataBind();



			GridView gvDelivery = e.Item.FindControl("gvDelivery") as GridView;
			var JobsiteDeliveries = from prd in vEL
									 where prd.Attribute("PickupTicketNumber") == null
									 select new
									 {
										 ContractNumber = (string)prd.Attribute("ContractNumber"),
										 PONumber = prd.Attribute("CustomerPONumber") != null ? prd.Attribute("CustomerPONumber").Value : "",
										 EquipmentType = prd.Attribute("CatClassDisplayName") != null ? prd.Attribute("CatClassDisplayName").Value : "",
										 EquipmentNumber = (string)prd.Attribute("EquipmentNumber"),
										 Make = prd.Attribute("EquipmentMake") != null ? prd.Attribute("EquipmentMake").Value : "",
										 Model = prd.Attribute("EquipmentModel") != null ? prd.Attribute("EquipmentModel").Value.Trim() : "",
										 Job = prd.Attribute("JobName") != null ? prd.Attribute("JobName").Value : "",
										 OrderedBy = prd.Attribute("OrderedBy") != null ? prd.Attribute("OrderedBy").Value : "",
										 DateRented = prd.Attribute("DateOut") != null ? prd.Attribute("DateOut").Value : ""
										 //PickupDate = prd.Attribute("PickupDate") != null ? prd.Attribute("PickupDate").Value : "",
										 //PickupTicketNumber = prd.Attribute("PickupTicketNumber") != null ? prd.Attribute("PickupTicketNumber").Value : "",
										 //PickupOrderedBy = prd.Attribute("PickupOrderedBy") != null ? prd.Attribute("PickupOrderedBy").Value : ""
									 };
			if(JobsiteDeliveries.Count() == 0)
			{
				//TableRow trJobsiteDelivery = e.Item.FindControl("repJobsites_ctl00_trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = Page.FindControl("repJobsites_ctl00_trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = e.Item.FindControl("trJobsiteDelivery") as TableRow;
				//trJobsiteDelivery = Page.FindControl("trJobsiteDelivery") as TableRow;
			}
			else
			{
				gvDelivery.DataSource = JobsiteDeliveries;
				gvDelivery.DataBind();
			}
		}
	}
}

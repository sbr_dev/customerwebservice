﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class UserControls_AnyInvoicePastDue : System.Web.UI.UserControl
{
	private string InvoiceXMLTestData = "<Invoices InvoiceCount=\"1\" TotalBilled=\"550.2200\" TotalBalance=\"550.2200\"><Invoice AccountNumber=\"119119\" InvoiceNumber=\"23033605-001\" InvoiceDate=\"11/16/2009\" OriginalAmount=\"550.2200\" CurrentBalance=\"550.2200\" /></Invoices>";

	protected void Page_Load(object sender, EventArgs e)
	{

	}

	public void LoadAlert(int AlertID, int SchedulePendingID)
	{
		string AlertDetailsXML = string.Empty;

		try
		{

			if (SchedulePendingID == 0)
			{
				AlertDetailsXML = InvoiceXMLTestData;
			}
			else
			{
				DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

				if (dt.Rows.Count == 0)
					throw new Exception("No alert information found for scheduled ID item.");

				if (dt.Rows.Count > 0)
					AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();
			}


			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);
			var items = from item in xDoc.Descendants("Invoice")
						select new
						{
							AccountNumber = item.Attribute("AccountNumber") != null ? item.Attribute("AccountNumber").Value : "",
							InvoiceNumber = item.Attribute("InvoiceNumber") != null ? item.Attribute("InvoiceNumber").Value : "",
							InvoiceDate = item.Attribute("InvoiceDate") != null ? item.Attribute("InvoiceDate").Value : "",
							OriginalAmount = item.Attribute("OriginalAmount") != null ? item.Attribute("OriginalAmount").Value : "",
							CurrentBalance = item.Attribute("CurrentBalance") != null ? item.Attribute("CurrentBalance").Value : "",
						};

			gvInvoices.DataSource = items;
			gvInvoices.DataBind();

			if (items.Distinct().Count() == 0)
			{
				gvInvoices.EmptyDataText = "No invoices found for Any New Invoice alert.";
				gvInvoices.GridLines = GridLines.None;  // Turn off gridlines if nothing is returned.  Empty gridview message appears 3d raised.
			}
		}
		catch (Exception ex)
		{
			gvInvoices.DataSource = null;
			gvInvoices.GridLines = GridLines.None;
			gvInvoices.EmptyDataText = "Unexpected error encountered. " + ex.Message;
            Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control");
			gvInvoices.EmptyDataRowStyle.ForeColor = System.Drawing.Color.Red;
			gvInvoices.DataBind();
		}
	}
}

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EquipmentOnRentPastDate.ascx.cs" Inherits="EquipmentOnRentPastDate" %>


<style type="text/css">
	.style1
	{
		height: 38px;
	}
	.style2
	{
		height: 21px;
	}
	.gridview
	{
		Font-Size:"XX-Small";
		font-family: Tahoma;
	}
</style>

<table id="tblInvoicePayment"  cellspacing="0" style="width: 100%; height: 100%;" runat="server">
	<tr style="font-family:Tahoma; font-size:smaller">
		<th align="left" class="style2" valign="bottom"><asp:Label ID="lblOnRentPast" runat="server"/></th>
	</tr>
	<tr >
		<td style="vertical-align: top;">
			<asp:GridView CellPadding="2" CssClass="gridview" ID="gvEquipmentOnRent" EnableViewState="true" runat="server"
				AllowSorting="true" ShowFooter="false" AutoGenerateColumns="false" GridLines="Both">
  				<Columns>
					<asp:BoundField HeaderText="Contract #" DataField="ContractNumber">
                        <ItemStyle HorizontalAlign="Center"  Wrap="false" Width="75px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
					<asp:BoundField HeaderText="Job Location" DataField="Job">
                        <ItemStyle HorizontalAlign="Left" Width="200px" />
                    </asp:BoundField>
					<asp:BoundField HeaderText="PO Number" DataField="PONumber">
                        <ItemStyle HorizontalAlign="Center"  Wrap="false" Width="100px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
					<asp:BoundField HeaderText="Equipment Type" DataField="EquipmentType">
                        <ItemStyle HorizontalAlign="Left"  Wrap="false" Width="200px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
					<asp:BoundField HeaderText="Equipment #" DataField="EquipmentNumber">
                        <ItemStyle HorizontalAlign="Left"  Wrap="false" Width="100px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
					<asp:BoundField HeaderText="Make" DataField="Make">
                        <ItemStyle HorizontalAlign="Left" Width="85px" wrap="false" />
                    </asp:BoundField>
                    
					<asp:BoundField HeaderText="Model" DataField="Model">
                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                    </asp:BoundField>
					<asp:TemplateField HeaderText="Date Rented">
						<ItemTemplate>
							<%# Convert.ToDateTime(Eval("DateRented")).ToShortDateString() %>
						</ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
					</asp:TemplateField>
					<asp:BoundField HeaderText="Days On Rent" DataField="DaysOnRent">
                        <ItemStyle HorizontalAlign="Center" Width="120px" />
                    </asp:BoundField>
				</Columns>
			</asp:GridView>
		</td>
	</tr>
</table>
<div id="divStepsToTake" visible="true" runat="server">
<table style="font-family:Tahoma, Arial; font-size:smaller">
<tr>
	<td colspan="4" class="style1" valign="bottom">1. If you still need the equipment, no action is required.</td>
</tr>
<tr>
	<td colspan="4">2. If you want to return the equipment or have it picked up, submit an <a href='https://www.sunbeltrentals.com/myaccount/equipment/'>equipment-pickup request online</a><asp:Label ID="lblOrCall" runat="server"> or call:</asp:Label></td>
</tr>
<tr id="trSaleRepAndLocationDetailsHeading" valign="top" style="color:Gray;" runat="server">
	<td colspan="1" style="font-size:small;vertical-align: bottom" class="style2;" >&nbsp;&nbsp;&nbsp;</td>
	<td colspan="1" class="style2" style="vertical-align: bottom;" >
		<u style="color: #000000; vertical-align: bottom;"><asp:Label ID="Label2" runat="server" Text="Sales Representative:"/></u></td>
	<td colspan="1" class="style2" style="vertical-align: bottom; font-size:small" ></td>
	<td colspan="1" class="style2" style="vertical-align: bottom;">
		<u style="color: #000000"><asp:Label ID="Label1" runat="server" Text="Sunbelt Location:"/></u></td>
</tr>
<tr id="trSaleRepAndLocationDetails" valign="top" align="left" style="color:Gray;" runat="server">
	<td colspan="1">&nbsp;&nbsp;&nbsp;</td>
	<td colspan="1" >
		<asp:Label ID="lblSalesRepName" runat="server"/><br /><asp:Label ID="lblSalesRepEmail" runat="server" /><br /><asp:Label ID="lblSalesRepPhone" runat="server"/>	</td>
	<td colspan="1"></td>
	<td colspan="1">
		<asp:Label ID="PCName" runat="server"/><br /><asp:Label ID="PCBranch" runat="server"/><br /><asp:Label ID="PCAddr" runat="server"/><br /><asp:Label ID="PCCityStateZIP" runat="server"/></td>
</tr>
</table>
</div>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GearBoxTransactionEmail.ascx.cs" Inherits="GearBoxTransactionEmail" %>

<head>
    <title>Reservation Confirmation</title>
	<style type="text/css">
		
		BODY { font-family: Tahoma, Verdana, Narrow, Sans-Serif; font-size: 8pt; }

		.style3
		{
			height: 40px;
		}

		table.ResConformation 
		{
			font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
			background-color:#F2F8F1;
		}

		td.Label 
		{	
			font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
						            width: 188px;

		}
		td.data 
		{	
			width: 72px;
			font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
		}
		td.infoHeader 
		{	
			border-left: 1px solid White;
			padding-left:1px;
			color:White;
			font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
			background-color: #4A843D;

		}
		.info_Left 
{	
	color:Black;
	font: bold 12px Tahoma, Verdana, Narrow, Sans-Serif;
}		
.info_Right
{	
	color:Black;
	font: 12px Tahoma, Verdana, Narrow, Sans-Serif;
}	
	    .style4
        {
            width: 457px;
        }
	</style>
</head>

<div style="font-family: Tahoma, Verdana, Narrow, Sans-Serif; padding-left: 10px; width: 630px;">


<table class="ResConformation" border="0" cellpadding="1px" cellspacing="1px"style="background-color:#F2F8F1; width: 706px;"  >
    <tr>
	    <td style="width:700px">
            <table width="700px" border="0">
		        <tr>
			        <td class="style4" style="width:100%"><span style="font-size: 10pt; font-weight: bold; height:16px;" >Sunbelt Rentals Inc.</span>
				    <br /><span style="font-size: 14pt; color:#666666;"><asp:Label ID="lblTransactionType1" runat="server" /></span></td>
			        <td vertical-align:top; text-align:right">&nbsp;<span><img id="SunbeltLogo" width="150" height="75"  alt="Sunbelt Rentals"  src="http://customer.sunbeltrentals.com/Images/mast_gearbox.png" /></span></td>
		        </tr>
                <tr valign="top">
	                <td colspan="2"><hr size="2"  noshade="true"  color="Black"  width="100%"/></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
	    <td style="height:30px;">
		    <p>&nbsp;Thank You for doing business with Sunbelt Rentals.</p>
	    </td>
    </tr>
    <tr>
	    <td style="height:30px; padding-bottom:5px; width:730px">
		    <p>&nbsp;<asp:Label ID="lblTransactionType2" runat="server" /> the Sunbelt Rentals ‘Gear Box <asp:Label ID="lblPCNumber" runat="server" />’ located on your job site.</p>
	    </td>
    </tr>
<tr>
    <td>
        <table style=" font-family: Tahoma, Verdana, Narrow, Sans-Serif; width: 700px;" 
            border="0" cellpadding="0px" cellspacing="0px">
            <tr>
                <td  class="infoHeader" colspan="2" style="width:340px;background-color:#4A843D; color:#ffffff; font-weight:bold; height: 20px; padding-left: 1px; "><asp:Label ID="lblTransactionType4" runat="server" /> Information</td>
		        <td class="infoHeader" style="width:399px; background-color:#4A843D; color:White; font-weight:bold" >Customer Information</td>
            </tr>
            <tr>
                <td style="width: 130px;"><b><asp:Label ID="lblTransactionType3" runat="server" />:</b>&nbsp;</td>
                <td class="data" align="left" style="font-weight:bold; width: 160px;"><asp:Label ID="lblTransactionNumber" runat="server" /></td>
                <td class="data"><b><asp:Label ID="lblCustomerAccountName" runat="server" /></b></td>
            </tr>
            <tr>
                <td style="width: 110px;"><b><asp:Label ID="lblPlaceHolderName1" runat="server" /></b></td>
                <td class="data" align="left" style="width: 160px"><b><asp:Label ID="lblPlaceHolderValue1" runat="server" /></b></td>
                <td class="data"><asp:Label ID="lblCustomerAddress12" runat="server" /></td>
            </tr>
            <tr>
                <td style="width: 110px;"><b><asp:Label ID="lblPlaceHolderName2" runat="server" /></b></td>
                <td class="data" align="left" style="width: 160px"><b><asp:Label ID="lblPlaceHolderValue2" runat="server" /></b></td>
                <td class="data"><asp:Label ID="lblCustomerCityStateZip" runat="server" /></td>
            </tr>
            <tr>
                <td style="width: 110px;"><b><asp:Label ID="lblPlaceHolderName3" runat="server" /></b></td>
                <td class="data" align="left" style="width: 160px"><b><asp:Label ID="lblPlaceHolderValue3" runat="server" /></b></td>
                <td class="data"><asp:Label ID="lblCustomerPhone" runat="server" /></td>
            </tr>
            <tr id="trPlaceHolder3"  visible="false">
                <td style="width: 110px;"><b><asp:Label ID="lblPlaceHolderName4" runat="server" /></b></td>
                <td class="data" align="left" style="width: 160px"><b><asp:Label ID="lblPlaceHolderValue4" runat="server" /></b></td>
                <td class="data">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="left">
                <br />                    
					<asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" 
                        style="margin-right: 124px; " Width="100%">
                        <HeaderStyle CssClass="infoHeader" BackColor="#4A843D" ForeColor="White" BorderColor="White" 
                            Font-Size="9pt" Font-Names="Tahoma, Verdana, Narrow, Sans-Serif" 
                            HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="1px" />
                        <RowStyle Font-Names="Tahoma, Verdana, Narrow, Sans-Serif" Font-Size="9pt" />
                        <Columns>
							<asp:TemplateField>
                                <HeaderTemplate>&nbsp;Qty&nbsp;</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblQty" runat="server" Font-Names="Arial" Text='<%#Eval("Quantity")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                                <ItemStyle Width="18%" HorizontalAlign="Center"  BorderColor="#F2F8F1"/>
                            </asp:TemplateField>
							<asp:TemplateField >
                                <HeaderTemplate>&nbsp;Equipment&nbsp;#&nbsp;</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblEquipmentNumber" runat="server" Font-Names="Arial" Text='<%#Eval("EquipNo")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                                <ItemStyle Width="18%" HorizontalAlign="Center"  BorderColor="#F2F8F1"/>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>Rental Item</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblDescription" runat="server" Font-Names="Arial" Text='<%#Eval("Description")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                                <ItemStyle Width="82%" HorizontalAlign="Left"  BorderColor="#F2F8F1"/>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
				</td>
            </tr>
        </table>
    </td>
</tr>
<tr id="trPriceEstimates" runat="server" visible="true">
    <td style=" padding-top: 15px; width:730px" >
        Price estimates are provided for convenience only, actual charges will be calculated during invoicing process.
    </td>
</tr>
<tr>
    <td style="padding-top:15px; width:630px" class="style3">
		<p class='Text'  style="color:Gray; font-size:smaller">
		<asp:Label ID="lblFooterMessage" runat="server" />
<%--			If you have any questions or you have received this message in error, please contact the Sunbelt Rentals store at 
			<asp:Label ID="PCPhoneNumber" runat="server" /> or by email <asp:Label ID="lblPCGearBoxEmail" runat="server" />.   Please refer to your contract when contacting us. 
--%>			
        </p>
    </td>
</tr>
<tr>
    <td style="height: 30px;"></td>
</tr>
<tr>
    <td style="color:#FF0000; text-align: center; font: Tahoma, Verdana, Narrow, Sans-Serif 12px; vertical-align:bottom; ">
        ** Note: This is an automated email. Do no reply to this email **
    </td>
</tr>
<tr id="trGetPDFLink" runat="server" visible="true">
    <td style="text-align:right;"><a href="http://www.adobe.com/products/acrobat/readstep2.html" ><img src="http://customer.sunbeltrentals.com/Images/get_adobe_reader.gif" border="0" /></a></td>
</tr>
</table>
</div>
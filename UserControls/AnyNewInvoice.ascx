﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnyNewInvoice.ascx.cs" Inherits="UserControls_AnyNewInvoice" %>
<style type="text/css">
	.style1
	{
		height: 38px;
	}
	.style2
	{
		height: 21px;
	}
</style>


<table id="tblInvoicePayment"  cellspacing="0" style="width: 100%; height: 100%;" runat="server">
	<tr style="font-family:Tahoma; font-size:smaller">
		<th align="left" class="style2" valign="bottom">Alert details for Any New Invoice</th>
	</tr>
	<tr >
		<td style="vertical-align: top;">
			<asp:GridView CellPadding="2"  Font-Size="Smaller" ID="gvInvoices" EnableViewState="true" runat="server"  Font-Names="Tahoma"
				AllowSorting="true" ShowFooter="false" AutoGenerateColumns="false" SkinID="Gray" GridLines="Both">
  				<Columns>
					<asp:BoundField HeaderText="Account Number" DataField="AccountNumber">
                        <ItemStyle HorizontalAlign="Center"  Wrap="false" Width="75px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>

					<asp:BoundField HeaderText="Invoice Number" DataField="InvoiceNumber">
                        <ItemStyle HorizontalAlign="Right"  Wrap="false" Width="100px"  />
                        <HeaderStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    
					<asp:TemplateField HeaderText="Invoice Date">
						<ItemTemplate>
							<%# Eval("InvoiceDate")%>
						</ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" />
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Original Amount">
						<ItemTemplate>
							<%# string.Format("{0:$#,##0.00;($#,##0.00)}", Convert.ToDecimal( Eval("OriginalAmount")) )%>
						</ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="90px" />
					</asp:TemplateField>

					<asp:TemplateField HeaderText="Current Balance">
						<ItemTemplate>
							<%# string.Format("{0:$#,##0.00;($#,##0.00)}", Convert.ToDecimal(Eval("CurrentBalance")))%>
						</ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" Width="90px" />
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</td>
	</tr>
</table>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;
using Sunbelt;
using Sunbelt.Queue;
using Sunbelt.BusinessObjects;
using Sunbelt.Common.DAL;
using SBNet;

public partial class Reservations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //   http://localhost/CustomerExtranetWebService/reservations.aspx?email=michael.evanko@sunbeltrentals.com&StrAcct=false&FilePath=\\\\sunwebappdev\\InvoiceReprint\\Sunbelt_WebReservation_24958349000.PDF&Orderid=1000769&RatePC=88
            //   http://localhost/CustomerExtranetWebService/reservations.aspx?email=michael.evanko@sunbeltrentals.com&StrAcct=false&FilePath=\\\\sunwebappdev\\InvoiceReprint\\111.PDF&Orderid=1000787&RatePC=61
            if (Request.QueryString["Email"] != null && Request.QueryString["StrAcct"] != null && Request.QueryString["FilePath"] != null && Request.QueryString["OrderId"] != null)
            {
                string EmailAddress = Request.QueryString["Email"].ToString();
                bool StrAcct = Convert.ToBoolean(Request.QueryString["StrAcct"].ToString());
                string filePath = Request.QueryString["FilePath"].ToString();
                Int64 OrderId = Convert.ToInt64(Request.QueryString["OrderId"].ToString());
                int PC = Convert.ToInt32(Request.QueryString["RatePC"].ToString());
				int CompanyID = Convert.ToInt32(Request.QueryString["CompanyID"].ToString());
		        string QASiteText = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QATEST - " : "";
				string subject = QASiteText + "Sunbelt Rentals Reservation";


                Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Reservations.ToString(),
                    string.Format("Send Reservations: Email,{0}|Stratgic Account,{1}|FilePath,{2}|OrderID,{3}|RatePC,{4}",
                                        EmailAddress,StrAcct,filePath,OrderId,PC));
				string sPC = PC.ToString();
                if (sPC.Length == 2)
                    sPC = "0" + sPC;
                else if (sPC.Length == 1)
                    sPC = "00" + sPC;

                ReservationRequest1.SetOrderItemsGridview(OrderId);

                string sMessage = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);

                ReservationRequest1.RenderControl(htw);
                sMessage += sw.ToString();


				string DM_EmailAddress = Reporting.GetDM_EmailAddress(PC);
				if (DM_EmailAddress != string.Empty)
				{
					DM_EmailAddress = (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QA_" : "") + DM_EmailAddress;
				}
				else
                    
					Sunbelt.Log.LogError(new Exception("Problem getting DM email address for PC: " + PC.ToString() + " for the reservation.aspx page on webservice"), Request.Url.AbsoluteUri.ToString());


				//******************** New email **************************/

				WebService webService = new WebService();
				QueueMailMessage queueMailMessage = webService.GetReservationEmailRecipients(OrderId, false, "");
				string sEmailMessageBody = "";
                queueMailMessage.BCC +=  ";michael.evanko@sunbeltrentals.com";

                StringWriter swNew = new StringWriter();
				HtmlTextWriter htwNew = new HtmlTextWriter(swNew);
				ReservationRequestNew1.SetOrderItemsGridview(OrderId, CompanyID);
				ReservationRequestNew1.RenderControl(htwNew);
				sEmailMessageBody = swNew.ToString();



                int AccountNumber = 0;
                DataTable dt = new DataTable();
                using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData)))
                {
                    sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
                    dt = sp.ExecuteDataTable();
                    AccountNumber = Convert.ToInt32(dt.Rows[0]["AccountNumber"]);
                }

//                string ExtraCCs = Reporting.GetCCEmailAddresses(PC, AccountNumber);
                

                if (AccountNumber == 0) // if Cash Customer
                {
                    try
                    {
                        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;

                        SBNet.CashCustomer cc = new SBNet.CashCustomer();
                        cc = p.GetCashCustomer(EmailAddress);
                        Customer customer = CustomerData.GetCustomer(cc.DriverLicenseNumber, cc.DriverLicenseState);

                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", cc.FirstName + " " + cc.LastName);
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", customer.Address.Address1.Trim());
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", customer.Address.City.Trim() + ", " + customer.Address.State.Trim() + " " + customer.Address.Zip.Trim());
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", customer.Address.City.Trim() + ", " + customer.Address.State.Trim() + " " + customer.Address.Zip.Trim());
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", customer.PrimaryPhone.Replace(")", ") "));
                    }
                    catch( Exception ex )
                    {
                        string x = ex.Message;
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", "");
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", "");
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", "");
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", "");
                        sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", "");
                    }
                }
                else
                {
                    //MembershipUser mu = Membership.GetUser(EmailAddress, false);
                    Customer cust = CustomerData.GetCustomer(AccountNumber);
                    sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", cust.CompanyName + " (Acct. #" + cust.AccountNumber.ToString() + ")");
                    sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", cust.Address.Address1.Trim());
                    sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", cust.Address.City.Trim() + ", " + cust.Address.State.Trim() + " " + cust.Address.Zip.Trim());
                    sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", cust.PrimaryPhone.Replace(")", ") "));

                }
				queueMailMessage.Subject = subject;
				queueMailMessage.Body = sEmailMessageBody;
				queueMailMessage.Attachment = filePath;
				queueMailMessage.AsHTML = true;

                // PROCDUCTION NOTE: Will need to change to AttachmentsOnly for production.
                queueMailMessage.CleanupAttachments = QueueMailMessage.CleanupType.AttachmentsOnly;
                queueMailMessage.Date = DateTime.Now;
				Sunbelt.Email.SendEmail(queueMailMessage);

				//******************** End of New Email **************************/
            }
            else
            {
                Sunbelt.Log.LogError(new Exception("Problem sending reservation from reservation.aspx page on webservice"), Request.Url.AbsoluteUri.ToString());
                throw new Exception("false");
            }
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogError(ex, "Problem sending reservation from reservation.aspx page on webservice");
            throw ex;
        }
    }

    // resolved exception of missing form tag when rendering user control
    public override void VerifyRenderingInServerForm(Control control)
    {
        return;
    }

}

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using Sunbelt.Security;
using System.Xml.Serialization;
using System.IO;


public partial class AlertInfo : System.Web.UI.UserControl
{
    int AccountNumber = 0;
    bool IsCorpLink = false;
	int CompanyID = Sunbelt.CommonValues.USCompanyID;

    int scheduleID { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

	public void SetAlertInfo(int AlertId, string RequestorEmail, int ScheduleID )
    {
		scheduleID = ScheduleID;
        DataTable dtAlertInfo = Alerts.GetAlertById(AlertId);

        AccountNumber = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["AccountNumber"].ToString(), 0);
        IsCorpLink = ConvertTools.ToBoolean(dtAlertInfo.Rows[0]["IsCorpLinkAccount"].ToString(), false);
		CompanyID = ConvertTools.ToInt32(dtAlertInfo.Rows[0]["CompanyID"].ToString(), 0);

        string strPhoneNumber = ConvertTools.ToString(dtAlertInfo.Rows[0]["NotifyPhoneNumber"]);
        string strFaxNumber = ConvertTools.ToString(dtAlertInfo.Rows[0]["NotifyFaxNumber"], "");


        lblReference.Text = dtAlertInfo.Rows[0]["ReferenceLabel"].ToString();
        lblReferenceValue.Text = dtAlertInfo.Rows[0]["ReferenceValue"].ToString();

		SetReference(AlertId, ConvertTools.ToInt32(dtAlertInfo.Rows[0]["TypeID"], 0), ConvertTools.ToString(dtAlertInfo.Rows[0]["RequestorFirstName"], ""),
            ConvertTools.ToString(dtAlertInfo.Rows[0]["RequestorLastName"], ""), ConvertTools.ToString(dtAlertInfo.Rows[0]["ParameterValue"], ""), RequestorEmail);
        
        lblParameterValue.Text += ConvertTools.ToString(dtAlertInfo.Rows[0]["ParameterValue"], "");
        lblCreatedBy.Text = ConvertTools.ToString(dtAlertInfo.Rows[0]["CreatedBy"], "");
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {

    }

    public void SetReference(int AlertId, int TypeId, string AlertSetupByFirstName, string AlertSetupByLastName, string ParameterValue, string RequestorEmail)
    {
        DataTable dtReferenceInfo = Alerts.GetReferenceByTypeId(TypeId);
        lblParameter.Text = ConvertTools.ToString(dtReferenceInfo.Rows[0]["ParameterLabel"], "");
		string wcan = string.Empty;
		string serverURL = string.Empty;
		CustomerJob cj = null;

		if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true")
			serverURL = "http://www.sunbeltrentals.com";
		else if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "false")
			serverURL = "http://qawww.sunbeltrentals.com";
		//serverURL = "http://qacustomer.sunbeltrentals.com";
		Sunbelt.Security.SecureQueryString sqs = new Sunbelt.Security.SecureQueryString();

		StringWriter sw = new StringWriter();
		HtmlTextWriter htw = new HtmlTextWriter(sw);

		switch( TypeId )
		{
			case (int)Alerts.TypeID.EquipmentOnRent:
			case (int)Alerts.TypeID.ContractEquipmentOnRent:
			case (int)Alerts.TypeID.EquipmentOnRentPastDays:
            case (int)Alerts.TypeID.ReservationEquipmentOnRent:

                EquipmentOnRent eor = CustomerData.GetCustomerEquipment(AccountNumber, lblReferenceValue.Text, IsCorpLink, CompanyID);
				cj = CustomerData.GetCustomerJob(AccountNumber, eor.JobNumber, IsCorpLink, CompanyID);

				ProfitCenter pc = CustomerData.GetProfitCenter(eor.PC, CompanyID);
				SalesRep sr = CustomerData.GetCustomerSalesRep(eor.SalesRepID, CompanyID);

				lblParameter.Text = "";
				lblParameterValue.Visible = false;

				//sqs = new SecureQueryString();
				sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
				sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
				wcan = "wcan=" + sqs.ToString();

				EquipmentOnRentPastDate ucEquipmentOnRentByContract = (EquipmentOnRentPastDate)LoadControl(@"~\usercontrols\EquipmentOnRentPastDate.ascx");
				ucEquipmentOnRentByContract.LoadAlert(AlertId, scheduleID);

				litMsg.Text = "You are receiving this email because you were signed up for this alert by Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + ".  ";
				litMsg.Text += "<br/>This alert is generated when equipment becomes past due based on your alert setting.";

				//if (TypeId == (int)Alerts.TypeID.ContractEquipmentOnRent)
				//    litMsg.Text += " on contract " + ucEquipmentOnRentByContract.ContractNumber.ToString();

				//litMsg.Text += " is on rent on or after " + ParameterValue;

				//if (TypeId == (int)Alerts.TypeID.EquipmentOnRentPastDays)
				//    litMsg.Text += " day(s)";
				litMsg.Text += "<br/><br/>";

				ucEquipmentOnRentByContract.RenderControl(htw);
				lblReferenceDetailValue.Text = sw.ToString();

				litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";
				//litLowerMsg.Text += "<a href='" + serverURL + "/support/unsubscribe.aspx?" + wcan + "'>unsubscribe</a>.</div>";

				litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
				litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";


				trCreatedBy.Visible = false;
				trReference.Visible = false;
				lblReference.Text = "";
				trParameter.Visible = false;
				break;

			case (int)Alerts.TypeID.InvoicePastDue:
				tblAlertsDetail.Visible = false;

				litMsg.Text = "You are receiving this email because you were signed up for this alert by Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + ".  ";
				litMsg.Text += "<br/>This alert is generated when invoice(s) become past due.&nbsp;&nbsp;You may view your invoices online at " + " <a href='https://www.sunbeltrentals.com/myaccount/invoices/'> Open Invoices</a>.";

				UserControls_AnyInvoicePastDue AnyInvoicePastDue = (UserControls_AnyInvoicePastDue)LoadControl(@"~\usercontrols\AnyInvoicePastDue.ascx");
				AnyInvoicePastDue.LoadAlert(AlertId, scheduleID);

				AnyInvoicePastDue.RenderControl(htw);
				lblReferenceDetailValue.Text = sw.ToString();

				sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
				sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
				wcan = "wcan=" + sqs.ToString();

				litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";
				//litLowerMsg.Text += "<a href='" + serverURL + "/support/unsubscribe.aspx?" + wcan + "'>unsubscribe</a>.</div>";

				litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
				litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";

				tblAlertsDetail.Visible = true;
				trCreatedBy.Visible = false;
				trReference.Visible = false;
				trParameter.Visible = false;
				lblReference.Text = "";
				break;

			case (int) Alerts.TypeID.BillingMoreThan:
				tblAlertsDetail.Visible = false;

				litMsg.Text = "You are receiving this email because you were signed up for this alert by Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + ".  ";
				litMsg.Text += "<br/>This alert is generated when billings with your PO# <b>" + lblReferenceValue.Text + "</b> are more than " + string.Format("{0:$#,##0.00;($#,##0.00)}", Convert.ToDecimal(ParameterValue));
				litMsg.Text += "<br/>You may view your account details online at <a href='https://www.sunbeltrentals.com/myaccount/'>WWW.SunbeltRentals.com</a>";

				UserControls_BillingPOMoreThan ucBillingPOMoreThan = (UserControls_BillingPOMoreThan)LoadControl(@"~\usercontrols\BillingPOMoreThan.ascx");
				ucBillingPOMoreThan.LoadAlert(AlertId, scheduleID);

				ucBillingPOMoreThan.RenderControl(htw);
				lblReferenceDetailValue.Text = sw.ToString();

				sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
				sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
				wcan = "wcan=" + sqs.ToString();

				litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";
				//litLowerMsg.Text += "<a href='" + serverURL + "/support/unsubscribe.aspx?" + wcan + "'>unsubscribe</a>.</div>";

				litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
				litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";

				tblAlertsDetail.Visible = true;
				trCreatedBy.Visible = false;
				trReference.Visible = false;
				trParameter.Visible = false;
				lblReference.Text = "";

				break;

			case (int) Alerts.TypeID.JobsiteHappening:
				litMsg.Text = "Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + " has requested that you be notified when activity occurs on jobsite." +
							  "<br/>This alert is generated when job site activity has occurred at job site: <b>" + lblReferenceValue.Text + "</b>." +
							  "<br/>You may view your account details online at <a href='https://www.sunbeltrentals.com/myaccount/'>www.SunbeltRentals.com</a>";

							  UserControls_JobsitePickupOrDelivery ucJobsitePickupOrDelivery = (UserControls_JobsitePickupOrDelivery)LoadControl(@"~\usercontrols\JobsitePickupOrDelivery.ascx");
				ucJobsitePickupOrDelivery.LoadAlert(AlertId, scheduleID);

				ucJobsitePickupOrDelivery.RenderControl(htw);
				lblReferenceDetailValue.Text = sw.ToString();

				sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
				sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
				wcan = "wcan=" + sqs.ToString();

				litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";
				//litLowerMsg.Text += "<a href='" + serverURL + "/support/unsubscribe.aspx?" + wcan + "'>unsubscribe</a>.</div>";

				litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
				litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";

				tblAlertsDetail.Visible = true;
				trCreatedBy.Visible = false;
				trReference.Visible = false;
				trParameter.Visible = false;
				lblReference.Text = "";

				trReference.Visible = false;
				trCreatedBy.Visible = false;
				break;

			case (int) Alerts.TypeID.InvoiceNew:
				litMsg.Text = "You are receiving this email because you were signed up for this alert by Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + ".  ";
				litMsg.Text += "<br/>This alert is generated when new invoice(s) become available.&nbsp;&nbsp;You may view your new invoices online at " + " <a href='https://www.sunbeltrentals.com/myaccount/invoices/'>www.SunbeltRentals.com</a>.";

				//StringWriter sw = new StringWriter();
				//HtmlTextWriter htw = new HtmlTextWriter(sw);

				UserControls_AnyNewInvoice ucAnyNewInvoice = (UserControls_AnyNewInvoice)LoadControl(@"~\usercontrols\AnyNewInvoice.ascx");
				ucAnyNewInvoice.LoadAlert(AlertId, scheduleID);

				ucAnyNewInvoice.RenderControl(htw);
				lblReferenceDetailValue.Text = sw.ToString();

				sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
				sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
				wcan = "wcan=" + sqs.ToString();

				litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";

				litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
				litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";

				tblAlertsDetail.Visible = true;
				trCreatedBy.Visible = false;
				trReference.Visible = false;
				trParameter.Visible = false;
				lblReference.Text = "";
				break;
            case (int)Alerts.TypeID.AnyNewReservation:
                litMsg.Text = "You are receiving this email because you were signed up for this alert by Sunbelt Rentals online user " + AlertSetupByFirstName + " " + AlertSetupByLastName + ".  ";
                litMsg.Text += "<br/>This alert is generated when new reservation(s) becomes available.&nbsp;&nbsp;You may view your new reservations online at " + " <a href='https://www.sunbeltrentals.com/myaccount/reservations/'>www.SunbeltRentals.com</a>.";
                UserControls_AnyNewReservation ucAnyNewReservation = (UserControls_AnyNewReservation)LoadControl(@"~\usercontrols\AnyNewReservation.ascx");
                ucAnyNewReservation.LoadAlert(AlertId, scheduleID);

                ucAnyNewReservation.RenderControl(htw);
                lblReferenceDetailValue.Text = sw.ToString();

                sqs.Add("email", HttpUtility.UrlDecode(RequestorEmail));
                sqs.Add("reportID", HttpUtility.UrlDecode(AlertId.ToString()));
                wcan = "wcan=" + sqs.ToString();

                litLowerMsg.Text = "<div style='font-family: Tahoma, Arial Narrow, Sans-Serif; font-size: 8pt;  color: #666666;'>To discontinue this Sunbelt Rentals Alert, click ";
				litLowerMsg.Text += "<a href='" + serverURL + "/customerservice/unsubscribe/?" + wcan + "'>unsubscribe</a>.</div>";

                litLowerMsg.Text += "<br /><br /><div style='width:100%; text-align:center; font-family: Tahoma, Arial;'>";
                litLowerMsg.Text += ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString() + "</div>";

                tblAlertsDetail.Visible = true;
                trCreatedBy.Visible = false;
                trReference.Visible = false;
                trParameter.Visible = false;
                lblReference.Text = "";
                break;
                
			
			
		}
	}
}

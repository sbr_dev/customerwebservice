﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Common.DAL;

public partial class ReservationRequestNew : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        gvItems.BorderColor = System.Drawing.Color.Tomato ;
    }

	public static string ProperCase(string Input)
	{
		return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Input.ToLower()); 
	}

    public void SetOrderItemsGridview(Int64 OrderId, int CompanyID)
    {
		string ReservationID = "";
		string RentalStartDate = "";
		string RentalEndDate = "";
		string ReservationDate = "";
        DateTime dtRentalStartDate;
        DateTime dtRentalEndDate;
        DateTime dtReservationDate;

        DataTable dt = new DataTable();
        try
        {
            using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData)))
            {
                sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
                dt = sp.ExecuteDataTable();
            }

            if (dt.Rows.Count == 0)
            {
                DataTable dtAS400 = new DataTable();
                using (SPData sp = new SPData("BusinessObjects.sb_GetCustomerQuoteDetails", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData)))
                {
                    sp.AddParameterWithValue("@AccountNumber", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
                    sp.AddParameterWithValue("@ContractNumber", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
					sp.AddParameterWithValue("@includereservations", SqlDbType.Bit, 1, ParameterDirection.Input, 1);
					sp.AddParameterWithValue("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, CompanyID);
                    dtAS400 = sp.ExecuteDataTable();
                }
                if (dtAS400.Rows.Count > 0)
                {
                    ReservationID = dtAS400.Rows[0]["ContractNumber"].ToString();
                    dtRentalStartDate = Convert.ToDateTime(dtAS400.Rows[0]["DateOut"]);
                    dtRentalEndDate = Convert.ToDateTime(dtAS400.Rows[0]["EstimatedReturnDate"]);
                    dtReservationDate = Convert.ToDateTime(dtAS400.Rows[0]["DateCreated"]);


                    RentalStartDate = String.Format("{0:ddd}", dtRentalStartDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalStartDate);
                    RentalEndDate = String.Format("{0:ddd}", dtRentalEndDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalEndDate);
                    ReservationDate = String.Format("{0:ddd}", dtReservationDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtReservationDate);
                }
            }
            else
            {

                ReservationID = dt.Rows[0]["ContractNumber"].ToString();
                dtRentalStartDate = Convert.ToDateTime(dt.Rows[0]["StartDate"]);
                dtRentalEndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"]);
                dtReservationDate = Convert.ToDateTime(dt.Rows[0]["OrderCreationDate"]);


                RentalStartDate = String.Format("{0:ddd}", dtRentalStartDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalStartDate);
                RentalEndDate = String.Format("{0:ddd}", dtRentalEndDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalEndDate);
                ReservationDate = String.Format("{0:ddd}", dtReservationDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtReservationDate);
            }
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogError(ex, "Retrieving reservation details failed");
        }
		lblReservationNumber1.Text = ReservationID;
		lblReservationNumber2.Text = ReservationID;
		lblRentalStartDate.Text = RentalStartDate;
		lblRentalEndDate.Text = RentalEndDate;
        lblReservationDate.Text = ReservationDate;

        gvItems.DataSource = dt;
        gvItems.DataBind();
    }
}

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ReportInfo : System.Web.UI.UserControl
{
    int AccountNumber = 0;
    bool IsCorpLink = false;

    public int RecordCount
    {
        set { lblNumberOfRecords.Text = value.ToString(); }
        get { return Convert.ToInt32(lblNumberOfRecords.Text); }
    }
	//public int ReportID
	//{
	//    set { lblReportID.Text = value.ToString(); }
	//    get { return Convert.ToInt32(lblReportID.Text); }
	//}

    protected void Page_Load(object sender, EventArgs e)
    {

    }

	public string SetReportInfo(int ReportId)
    {
        DataTable dtUserReport = Reporting.GetUserReportByID(ReportId);
        if (dtUserReport.Rows.Count > 0)
        {
			Guid UserId = new Guid(dtUserReport.Rows[0]["UserID"].ToString());
            AccountNumber = Convert.ToInt32(dtUserReport.Rows[0]["CustomerNumber"].ToString());
            IsCorpLink = Convert.ToBoolean(dtUserReport.Rows[0]["IsCorpLink"].ToString());
            lblReportName.Text = Convert.ToString(dtUserReport.Rows[0]["Title"]);
            //lblDescription.Text = Convert.ToString(dtUserReport.Rows[0]["Description"]);

			bool isStanleyReport = isStanleySlidingDateWeeklyReport(ReportId);

			string serverURL = string.Empty;
			if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true")
				serverURL = "http://www.sunbeltrentals.com";
			else if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "false")
				serverURL = "http://qawww.sunbeltrentals.com";

			hlUnsubscribe.NavigateUrl = hlUnsubscribe.NavigateUrl.Replace("[SERVERURL]", serverURL);
			hlUnsubscribe.NavigateUrl = hlUnsubscribe.NavigateUrl.Replace("[REPORTID]", ReportId.ToString());
 
			DataTable dtUserReportColumns = Reporting.GetUserReportColumnsByUserReportID(ReportId);

            DataTable dtUserReportFilters = Reporting.GetUserReportFiltersByUserReportID(ReportId);
            foreach (DataRow dr in dtUserReportFilters.Rows)
            {
                if (Sunbelt.Tools.ConvertTools.ToString(dr["Filter_List"], "") != "")
                    lblFilters.Text += dr["Header"].ToString() + " Is Equal To " + dr["Filter_List"].ToString() + "; ";
                else if (Sunbelt.Tools.ConvertTools.ToString(dr["Filter_Like"], "") != "")
                    lblFilters.Text += dr["Header"].ToString() + " Is Like " + dr["Filter_Like"].ToString() + "; ";
		else if (dr["Filter"].ToString() == "GreaterThan" )
			lblFilters.Text += dr["Header"].ToString() + " Greater Than " + dr["Filter_Range_Begin"].ToString() + "; ";  //need to change where filter gets in right place
                else if (Sunbelt.Tools.ConvertTools.ToString(dr["Filter_Range_Begin"], "") != "")
                {
					lblFilters.Text += dr["ColumnName"].ToString() + " = " + dr["RunTimeDateFilterRangeDisplayDetailed"].ToString().Trim() + "; ";
                }
            }
            if( lblFilters.Text.Trim() == string.Empty)
                lblFilters.Text = "None";
            lblFilters.Text += "<br/>Reference ID: " + ReportId;
        }
		return lblFilters.Text;
   }
	public bool isStanleySlidingDateWeeklyReport(int ReportId)
	{
		int[] StanleyReportIds = { 497, 10811,10828,10829,10830,10831,10832,10833,10835,10836,10837,10838,10839,10840,10841,10842,
									10843,10844,10845,10846,10847,10848,10853,10854,10855,10856,10857,10858,10859,10860,10861,
									10862,10863,10864,10865,10866,10867,10868,10869,10871,10872,10873,10874,10875,10876,10877,
									10878,10879,10880,10881,10882,10883,10884,10885,10886,10887,10888,10889,10890,10891,10892,
									10893,10894,10895,10896,10897,10898,10944 };
		return StanleyReportIds.Contains(ReportId);
	}
}

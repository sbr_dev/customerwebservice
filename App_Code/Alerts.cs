using System;
using System.Data;
using System.Data.SqlClient;
using Sunbelt.Common.DAL;
using System.Configuration;

/// <summary>
/// Summary description for Alerts
/// </summary>
public class Alerts
{
	public enum TypeID { EquipmentOnRent = 1, InvoicePastDue, BillingMoreThan, JobsiteHappening = 6, InvoiceNew, ContractEquipmentOnRent, EquipmentOnRentPastDays, AnyNewReservation, ReservationEquipmentOnRent };

    private static string csBaseData
	{
		get { return ConfigurationManager.ConnectionStrings["BaseData"].ConnectionString; }
	}
	
	public static DataTable GetAlertById(int ID)
    {
		using(SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("Alerts.sb_SelAlertById", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("AlertId", ID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
    }

    public static int AlertNotificationComplete(int ScheduleId, string Action)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using( SqlCommand command = new SqlCommand("Alerts.sb_SetScheduleCompleted", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("SchedulePendingID", ScheduleId);
				command.Parameters.AddWithValue("ActionResults", Action);
			return command.ExecuteNonQuery();
			}
		}
    }

    public static DataTable GetReferenceByTypeId(int TypeId)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using( SqlCommand command = new SqlCommand("Alerts.sb_SelReferenceInfoByTypeId", conn) )
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("TypeId", TypeId);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}

    public static DataTable GetFrequencies()
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("Alerts.sb_SelAlertFrequencies", conn))
			{
				command.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}

    public static DataTable GetFrequencyParameters(int FrequencyId)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
            using (SqlCommand command = new SqlCommand("Alerts.sb_SelAlertFrequencyParameters", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("FrequencyID", FrequencyId);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
            }
		}
	}

    public static DataTable GetAlertFrequenciesByAlertID(int AlertId)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("Alerts.sb_SelAlertFrequenciesByAlertID", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("AlertId", AlertId);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}

    public static int DeleteAlertById(int AlertId)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("Alerts.sb_DeleteAlertById", conn) )
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("AlertId", AlertId);
	            int retval = command.ExecuteNonQuery();
	            return retval;
			}
		}
	}

	public static DataTable GetScheduleAlertPendingByID(int SchedulePendingID)
	{
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("Alerts.sb_SelSchedulePendingByScheduleID", conn) )
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("SchedulePendingID", SchedulePendingID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}

	public static DataTable GetSMSCarriers()
	{
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("sms.sb_SelCarriers", conn) )
			{
				command.CommandType = CommandType.StoredProcedure;

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for PdfService
/// </summary>
[WebService(Namespace = "http://cpws.sunbeltrentals.com")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Request : System.Web.Services.WebService
{
    public const string ONSTART = "ON_START";
    public const string ONEND   = "ON_END";
    public const string WAITING = "WAITING";


    public Request()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }


    [WebMethod]
    public void OnPdfStart(string docNumber)
    {
        Sunbelt.Tools.WebTools.SetCache(docNumber, ONSTART, DateTime.Now.AddMinutes(5));
    }

    [WebMethod]
    public void OnPdfEnd(string docNumber)
    {
        Sunbelt.Tools.WebTools.SetCache(docNumber, ONEND, DateTime.Now.AddMinutes(5));
    }

    [WebMethod]
    public string GetPdfStatus(string docNumber)
    {
        return Sunbelt.Tools.WebTools.GetCache(docNumber, "") as String;
    }

    [WebMethod]
    public void InitPdf(string docNumber)
    {
        Sunbelt.Tools.WebTools.SetCache(docNumber, WAITING, DateTime.Now.AddMinutes(5));
    }

    [WebMethod]
    public bool IsPdfEnd( string docNumber)
    {
        string value = Sunbelt.Tools.WebTools.GetCache(docNumber, "") as String;
        return (value == ONEND );
    }

    [WebMethod]
    public void FinishPdf(string docNumber)
    {
        Sunbelt.Tools.WebTools.RemoveCache(docNumber);
    }
}



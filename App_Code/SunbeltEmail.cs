using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using Sunbelt.Queue;
using System.Collections.Generic;

namespace Sunbelt
{
	/// <summary>
	/// Summary description for SunbeltEmail
	/// </summary>
	public class Email
	{
		public enum CleanupType { None, AttachmentsOnly, AttachmentsAndFolder }

		/// <summary>
		/// Returns the contents of a text file.
		/// </summary>
		/// <param name="sFileName">The text file name.</param>
		/// <returns></returns>
		static public string ReadTextFile(string sFileName)
		{
			// Create an instance of StreamReader to read from a file.
            using (StreamReader sr = new StreamReader(sFileName))
            {
                return sr.ReadToEnd();
            }
		}

		/// <summary> 
		/// Remove the <script> tags from HTML.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		static public string RemoveScriptTagesFromHTML(string text)
		{
			Regex r = new Regex("<script[^>]*>(.*?)</script>", RegexOptions.Singleline | RegexOptions.IgnoreCase);
			return r.Replace(text, "");
		}

		/// <summary>
		/// Creates an HTML message with optional styles added from 'styleSheets' files. Perform a
		/// string.Replace("[CONTENT_HERE]", 'your message') to insert your message between
		/// the <BODY> tags of the HTML.
		/// </summary>
		/// <param name="styleSheets"></param>
		/// <returns></returns>
		static public string CreateHTMLMessageTemplate(params string[] styleSheets)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<html>");
			sb.Append("<head>");
			sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=us-ascii\">");
			//sBuilder.Append("<title>Page title goes here</title>");

			sb.Append("<style type=\"text/css\">");
			foreach (string fn in styleSheets)
			{
				sb.Append(ReadTextFile(fn));
			}
			sb.Append("</style>");

			sb.Append("</head>");
			sb.Append("<body>[CONTENT_HERE]</body>");
			sb.Append("</html>");
			return sb.ToString();
		}

		/// <summary>
		/// Creates an HTML message with optional styles added from 'styleSheets' files. Perform a
		/// string.Replace("[CONTENT_HERE]", 'your message') to insert your message between
		/// the <BODY> tags of the HTML.
		/// </summary>
		/// <param name="styleSheets"></param>
		/// <returns></returns>
		static public string CreateHTMLMessageTemplateWithStyles(params string[] styles)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append("<html>");
			sb.Append("<head>");
			sb.Append("<meta http-equiv=Content-Type content=\"text/html; charset=us-ascii\">");
			//sBuilder.Append("<title>Page title goes here</title>");

			sb.Append("<style type=\"text/css\">");
			foreach (string s in styles)
			{
				sb.Append(s);
			}
			sb.Append("</style>");

			sb.Append("</head>");
			sb.Append("<body>[CONTENT_HERE]</body>");
			sb.Append("</html>");
			return sb.ToString();
		}

		/// <summary>
		/// Creates an HTML message with optional styles added from 'styleSheets'. Inserts 'message'
		/// between the <BODY> tags.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="styleSheets"></param>
		/// <returns></returns>
		static public string CreateHTMLMessage(string message, bool removeScriptTags, params string[] styleSheets)
		{
			StringBuilder sb = new StringBuilder(CreateHTMLMessageTemplate(styleSheets));
			sb.Replace("[CONTENT_HERE]", (removeScriptTags) ? RemoveScriptTagesFromHTML(message) : message);
			return sb.ToString();
		}

		static public void SendEmail(string[] sTo, string sFrom, string[] sCC, string[] sBCC,
			string sSubject, string sBody, bool bAsHTML)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, null, CleanupType.None, false);
		}

		static public void SendEmail(string[] sTo, string sFrom, string[] sCC, string[] sBCC,
			string sSubject, string sBody, bool bAsHTML, bool returnToSenderOnErr)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, null, CleanupType.None, returnToSenderOnErr);
		}

		static public void SendEmail(string[] sTo, string sFrom, string[] sCC, string[] sBCC,
			string sSubject, string sBody, bool bAsHTML,
			string[] sAttachment, CleanupType cleanupAttachments)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, sAttachment, cleanupAttachments, false);
		}

		static public void SendEmail(string[] sTo, string sFrom, string[] sCC, string[] sBCC,
			string sSubject, string sBody, bool bAsHTML,
			string[] sAttachment, CleanupType cleanupAttachments,
			bool returnToSenderOnErr)
		{
			SendIt(CommaSeparateArray(sTo),
				sFrom,
				CommaSeparateArray(sCC),
				CommaSeparateArray(sBCC),
				sSubject, sBody, bAsHTML,
				CommaSeparateArray(sAttachment),
				cleanupAttachments,
				returnToSenderOnErr);
		}

		static public void SendEmail(string sTo, string sFrom, string sCC, string sBCC,
			string sSubject, string sBody, bool bAsHTML)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, null, CleanupType.None, false);
		}

		static public void SendEmail(string sTo, string sFrom, string sCC, string sBCC,
			string sSubject, string sBody, bool bAsHTML, bool returnToSenderOnErr)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, null, CleanupType.None, returnToSenderOnErr);
		}

		static public void SendEmail(string sTo, string sFrom, string sCC, string sBCC,
			string sSubject, string sBody, bool bAsHTML,
			string sAttachment, CleanupType cleaupAttachments)
		{
			SendEmail(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, sAttachment, cleaupAttachments, false);
		}

		static public void SendEmail(string sTo, string sFrom, string sCC, string sBCC,
			string sSubject, string sBody, bool bAsHTML,
			string sAttachment, CleanupType cleaupAttachments,
			bool returnToSenderOnErr)
		{
			//Make sure these are comma separated.
			sTo = CommaSeparateString(sTo);
			sCC = CommaSeparateString(sCC);
			sBCC = CommaSeparateString(sBCC);

			SendIt(sTo, sFrom, sCC, sBCC, sSubject, sBody, bAsHTML, sAttachment, cleaupAttachments, returnToSenderOnErr);
		}

		static private string CommaSeparateArray(string[] array)
		{
			StringBuilder sb = new StringBuilder();
			if (array != null)
			{
				foreach (string s in array)
				{
					if(!string.IsNullOrEmpty(s))
						sb.Append(s.Trim() + ",");
				}
			}
			//Remove last comma.
			if ((sb.Length > 0) && (sb[sb.Length - 1] == ','))
				sb.Remove(sb.Length - 1, 1);
			//Comma separate 'sb' in case one of the array elements
			// has two or more email address.
			return CommaSeparateString(sb.ToString());
		}
		
		static private string CommaSeparateString(string str)
		{
			StringBuilder sb = new StringBuilder();
			if(!string.IsNullOrEmpty(str))
			{
				string[] parts = str.Split(new char[] { ',', ':', ';' });
				for (int i = 0; i < parts.Length; i++)
				{
					string s = parts[i].Trim();
					if(!string.IsNullOrEmpty(s))
						sb.Append(s.Trim() + ",");
				}
				//Remove last comma.
				if ((sb.Length > 0) && (sb[sb.Length - 1] == ','))
					sb.Remove(sb.Length - 1, 1);
			}
			return sb.ToString();
		}

		/// <summary>
		/// Takes a ',', ':', or ';' seperated string and returns a string array.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		static public string[] StringToStringArray(string str)
		{
			List<string> list = new List<string>(); 
			if (!string.IsNullOrEmpty(str))
			{
				string[] parts = str.Split(new char[] { ',', ':', ';' });
				for (int i = 0; i < parts.Length; i++)
				{
					string s = parts[i].Trim();
					if (!string.IsNullOrEmpty(s))
						list.Add(s);
				}
			}
			return list.ToArray();
		}

		static private void SendIt(string sTo, string sFrom, string sCC, string sBCC,
			string sSubject, string sBody, bool bAsHTML,
			string sAttachment, CleanupType cleaupAttachments,
			bool returnToSenderOnErr)
		{
			if (string.IsNullOrEmpty(sTo))
				throw new Exception("SendEmail 'sTo' is required.");
			if (string.IsNullOrEmpty(sFrom))
				throw new Exception("SendEmail 'sFrom' is required.");

			bool bSendToQueue = false;
			bool.TryParse(ConfigurationSettings.AppSettings["UseEmailQueue"], out bSendToQueue);
			if (bSendToQueue)
			{
				//No \r\n in the subject.
				string subject = sSubject.Replace("\r\n", " ");
				QueueMailMessage qmm = new QueueMailMessage(subject, //Use subject as label
					sTo, sFrom, sCC, sBCC,
					subject, sBody, bAsHTML,
					sAttachment, (QueueMailMessage.CleanupType)cleaupAttachments);
				qmm.ReturnToSenderOnErr = returnToSenderOnErr;
				EmailQueue.SendEmail(qmm, ConfigurationSettings.AppSettings["EmailQueuePath"]);	
			}
			else
			{
				using (MailMessage mailMsg = new MailMessage())
				{
					//Set the properties.
					mailMsg.To.Add(sTo);
					mailMsg.From = new MailAddress(sFrom);
					//CC this if needed.
					if (!string.IsNullOrEmpty(sCC))
						mailMsg.CC.Add(sCC);
					//BCC so dont let the other users see who it was sent to.
					if (!string.IsNullOrEmpty(sBCC))
						mailMsg.Bcc.Add(sBCC);
					//Set the mail format.
					mailMsg.IsBodyHtml = bAsHTML;
					//mailMsg.BodyEncoding = System.Text.Encoding.UTF8;
					//Set the priority - options are High, Low, and Normal
					mailMsg.Priority = MailPriority.Normal;
					//Set the subject. No \r\n in the subject.
					mailMsg.Subject = sSubject.Replace("\r\n", " ");
					//Set the body
					mailMsg.Body = sBody;
					//Add attachments. Attachments have to be added one at a time.
					// Split the list by the commas.
					if (!string.IsNullOrEmpty(sAttachment))
					{
						string[] attachments = sAttachment.Split(new char[] { ',' });
						foreach (string s in attachments)
						{
							if (!string.IsNullOrEmpty(s))
								mailMsg.Attachments.Add(new Attachment(s));
						}
					}

					//Specify  Smtp Server.
#if false
					SmtpClient smtp = new SmtpClient("sbmail.sunbeltrentals.com");
#else
					//To user the default contstructor you must setup the smtp settings
					// in the app.config or web.config like this:
					//<system.net>
					//	<mailSettings>
					//		<smtp>
					//			<network host="sbmail.sunbeltrentals.com" port="25" userName="????" password="?????" />			 
					//		</smtp>		  
					//	</mailSettings>	   
					//</system.net>
					SmtpClient smtp = new SmtpClient();
#endif

					//Now, to send the message, use the Send method of the SmtpMail class.
					smtp.Send(mailMsg);
				}
			}
		}
		static public void SendEmail( QueueMailMessage qmm )
		{
			//Make sure these are comma separated.
			qmm.To = CommaSeparateString(qmm.To);
			qmm.CC = CommaSeparateString(qmm.CC);
			qmm.BCC = CommaSeparateString(qmm.BCC);

			if (string.IsNullOrEmpty(qmm.To))
				throw new Exception("SendEmail 'sTo' is required.");
			if (string.IsNullOrEmpty(qmm.From))
				throw new Exception("SendEmail 'sFrom' is required.");

			bool bSendToQueue = false;
			bool.TryParse(ConfigurationSettings.AppSettings["UseEmailQueue"], out bSendToQueue);

			EmailQueue.SendEmail(qmm, ConfigurationSettings.AppSettings["EmailQueuePath"]);
		}
	}
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunbelt
{
	public static class CommonStrings
	{
		public const string CompanyID = "CompanyID";
		public const string SqlParameterCompanyID = "@" + CompanyID;
	}
	public static class CommonValues
	{
		public const int WildCardCompanyID = -1;
		public const int USCompanyID = 1;
		public const int CACompanyID = 2;
	}
}

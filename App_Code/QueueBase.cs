﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;

namespace Sunbelt.Queue
{
	public class QueueBase<T>
	{
		protected MessageQueue Queue { set; get; }
		protected XmlMessageFormatter Formatter { set; get; }

		//public QueueBase() { }
		public QueueBase(string path)
		{
			Queue = new System.Messaging.MessageQueue(path);
			Formatter = new XmlMessageFormatter(new Type[] { typeof(T) });
		}

		public T Peek(out string id)
		{
			return (T)Peek(new TimeSpan(0), out id);
		}

		public T Peek(TimeSpan ts, out string id)
		{
			System.Messaging.Message msg;
			id = string.Empty;

			if (!IsMessageInQueue())
				return default(T);
			msg = Queue.Peek(ts);
			msg.Formatter = Formatter;
			id = msg.Id;
			return (T)msg.Body;
		}

		public T PeekById(string id)
		{
			System.Messaging.Message msg;
			msg = Queue.PeekById(id);
			msg.Formatter = Formatter;
			id = msg.Id;
			return (T)msg.Body;
		}

		public void Send(T item, string id)
		{
			Queue.Send(item, id);
		}
	
		public T Receive(out string id)
		{
			return (T)Receive(new TimeSpan(0), out id);
		}

		public T Receive(TimeSpan ts, out string id)
		{
			System.Messaging.Message msg;
			id = string.Empty;

			if (!IsMessageInQueue())
				return default(T);
			msg = Queue.Receive(ts);
			msg.Formatter = Formatter;
			id = msg.Id;
			return (T)msg.Body;
		}

		public T ReceiveById(string id)
		{
			System.Messaging.Message msg;
			msg = Queue.ReceiveById(id);
			msg.Formatter = Formatter;
			id = msg.Id;
			return (T)msg.Body;
		}

		public MessageEnumerator GetMessageEnumerator()
		{
			return Queue.GetMessageEnumerator2();
		}

		public List<T> GetMessageList(out List<string> idList)
		{
			MessageEnumerator e = Queue.GetMessageEnumerator2();
		    idList = new List<string>();
		    List<T> list = new List<T>();
			while (e.MoveNext())
			{
				System.Messaging.Message msg = e.Current;
				msg.Formatter = Formatter;
				list.Add((T)msg.Body);
				idList.Add(msg.Id);
			}
		    return list;
		}

		public bool IsMessageInQueue()
		{
			try
			{
				//Set Peek to return immediately.
				Queue.Peek(new TimeSpan(0));
				//If no exception, a message is in the queue.
				return true;
			}
			catch (MessageQueueException ex)
			{
				if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
				{
					//If an IOTimeout exception was thrown, there are no messages.
					return false;
				}
				else if (ex.MessageQueueErrorCode == MessageQueueErrorCode.ServiceNotAvailable)
				{
					//Look for ServiceNotAvailable exception. This gets thrown if the
					// MSQueue service is not running yet.  This can happen at boot
					// time if this code executes before the MSMQ service has started.
					return false;
				}
				else
				{
					//Handle other sources of MessageQueueException.
					throw ex;
				}
			}
		}

		public bool IsQueueReady()
		{
			try
			{
				//Set Peek to return immediately.
				Queue.Peek(new TimeSpan(0));
				//If no exception a message is in the queue.
				//Queue is ready.
				return true;
			}
			catch (MessageQueueException ex)
			{
				if (ex.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
				{
					//If an IOTimeout was thrown, there is no messages.
					//Queue is ready.
					return true;
				}
				else
				{
					//Handle other sources of MessageQueueException.
					return false;
				}
			}
		}

		public IAsyncResult BeginPeek()
		{
			return Queue.BeginPeek();
		}

		public IAsyncResult BeginPeek(TimeSpan timeout)
		{
			return Queue.BeginPeek(timeout);
		}

		public void SetPeekCompleted(PeekCompletedEventHandler handler)
		{
			Queue.PeekCompleted += handler;
		}

		public T EndPeek(IAsyncResult asyncResult)
		{
			return (T)Queue.EndPeek(asyncResult).Body;
		}

	}
}

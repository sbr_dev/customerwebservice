using System;
using System.IO;
using System.Net;
using System.Web;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web.Services;
using System.Collections.Generic;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Sunbelt.Common.DAL;
using Sunbelt.Queue;
using System.Linq;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Reflection;
using System.Diagnostics;


/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://cews.sunbeltrentals.com")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebService : System.Web.Services.WebService
{
	// used when rendering a page by hand from a non-aspx page
	public class FormlessPage : Page
	{
		public override void VerifyRenderingInServerForm(Control control) { }
		public UserControl LoadControl(string UserControlPath, params object[] constructorParameters)
		{
			//List constParamTypes = new List() ;
			List<Type> constParamTypes = new List<Type>();
			foreach (object constParam in constructorParameters)
			{
				constParamTypes.Add(constParam.GetType());
			}

            UserControl ctl = Page.LoadControl(UserControlPath) as UserControl;

            // Find the relevant constructor
            ConstructorInfo constructor = ctl.GetType().BaseType.GetConstructor(constParamTypes.ToArray()) ;

            //And then call the relevant constructor
            if (constructor == null)
            {
                throw new MemberAccessException("The requested constructor was not found on : " + ctl.GetType().BaseType.ToString()) ;
            }
            else
            {
                constructor.Invoke(ctl,constructorParameters) ;
            }

            // Finally return the fully initialized UC
            return ctl;
        }
}
    public WebService() 
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent();
    }

    public enum PDFType { Reservations, Invoice, Contract }

    [WebMethod]
	public string EmailUserReport(int UserReportId, int ScheduleId)
	{
		return EmailUserReport2(UserReportId, ScheduleId, null);
	}

    [WebMethod]
	public string EmailUserReport2(int UserReportId, int ScheduleId, Guid? referenceId)
    {
		DataTable dtUserReport = Reporting.GetUserReportByID(UserReportId);
		
		if (dtUserReport.Rows.Count > 0)
        {
            Guid UserId = new Guid(dtUserReport.Rows[0]["UserID"].ToString());
            int AccountNumber = Convert.ToInt32(dtUserReport.Rows[0]["CustomerNumber"].ToString());
            bool IsCorpLink = Convert.ToBoolean(dtUserReport.Rows[0]["IsCorpLink"].ToString());
            int CompanyID = Convert.ToInt32(dtUserReport.Rows[0]["CompanyID"].ToString());

			//Verify that the user is still part of that account
			bool blnVerifiedUserAccount = Reporting.VerifyAccount(UserId, AccountNumber, IsCorpLink, CompanyID);
            if (blnVerifiedUserAccount)
            {
				string sEmail = ConvertTools.ToString(dtUserReport.Rows[0]["EmailRecipient"], "");
				string url = ToWebAddress("~/Report.aspx?ReportId=" + UserReportId + "&Email=" + sEmail + "&ScheduleId=" + ScheduleId);
				if(referenceId.HasValue)
					url += "&referenceId=" + referenceId.ToString();
				try
				{
					Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Report.aspx call" + url);
					StringBuilder strRequest = MakeRequest(url);
				}
				catch(Exception ex)
				{
					Sunbelt.Log.LogError(ex, "Error making web request: " + url, referenceId);
					throw;	
				}
            }
            else
            {
                Reporting.DeleteReport(UserReportId);
                Exception ex = new Exception("User " + UserId + " no longer belongs to account " + AccountNumber.ToString() + ". Report " + UserReportId.ToString() + " has been deleted." + "\r\nReference Id: " + referenceId);
				if(referenceId.HasValue)
					Sunbelt.Log.LogError(ex, null, referenceId);
				throw ex;
            }
        }
        else
        {
            //Delete The Report
            Reporting.DeleteReport(UserReportId);
            throw new Exception("User report count = " + dtUserReport.Rows.Count + ".  " + UserReportId + " user report no longer exists.  SP called = ReportSpec.sb_SelReport with parameter UserReportId = " + UserReportId + "\r\nReference Id: " + referenceId);
        }
		return "true";
	}

    [WebMethod]
	public string SendAlert(int AlertId, int ScheduleId, string Value)
    {
		return SendAlert2(AlertId, ScheduleId, Value, null);
	}

    [WebMethod]
	public string SendAlert2(int AlertId, int ScheduleId, string Value, Guid? referenceId)
    {
        if(!referenceId.HasValue)
			referenceId = new Guid();
        bool AlertErrorEncountered = false;                     // flag is used whether to set alert complete or not.
		string NotificationErrorMessage = string.Empty;
		
		DataTable dtAlert = Alerts.GetAlertById(AlertId);
		if (dtAlert.Rows.Count > 0)
        {
			string LogToSQLText = string.Empty;
			bool notifyByEmailSuccess = false;
			bool notifyByPhoneSuccess = false;

            int TypeId = Convert.ToInt32(dtAlert.Rows[0]["TypeID"].ToString());
            string TypeName = dtAlert.Rows[0]["TypeName"].ToString();
            string AlertName = dtAlert.Rows[0]["AlertName"].ToString();
            string ParameterValue = dtAlert.Rows[0]["ParameterValue"].ToString();
            string ReferenceValue = dtAlert.Rows[0]["ReferenceValue"].ToString();
            int AccountNumber = ConvertTools.ToInt32(dtAlert.Rows[0]["AccountNumber"].ToString(), 0);
			bool IsCorpLink = ConvertTools.ToBoolean(dtAlert.Rows[0]["IsCorpLinkAccount"].ToString(), false);
			int CompanyID = ConvertTools.ToInt32(dtAlert.Rows[0]["CompanyID"].ToString());

			bool notifyByEmail = false;
			bool notifyByPhone = false;
			//bool notifyByFax = false;  // Currently not supported

            DataTable dt = Alerts.GetScheduleAlertPendingByID(ScheduleId);
            
            // Load XML Data if there
            string AlertDetailsXML = string.Empty;
            if (dt.Rows.Count == 0)
				throw new Exception("No alert information found for scheduled ID item.");
			if (dt.Rows.Count > 0)
				AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();

			if (dt.Rows.Count == 0)
				throw new Exception("No alert information found for scheduled ID item.");
			string Action = dt.Rows[0]["Action"].ToString().ToUpper();
			switch( Action )
			{
				case "E":
					notifyByEmail = true;
					break;
				case "P":
					notifyByPhone = true;
					break;
				//case "F":    Currently not supported
				//    notifyByFax = true;
				//    break;
			}

			//StringBuilder sb = new StringBuilder();

            if (notifyByPhone)
            {
				string RequestorPhoneNumber = string.Empty;
				string RequestorCarrierEmailDomain = string.Empty;
				string RequestorFirstName = string.Empty;
				string RequestorLastName = string.Empty;

				try
				{
					RequestorPhoneNumber = dtAlert.Rows[0]["NotifyPhoneNumber"].ToString();
					RequestorCarrierEmailDomain = dtAlert.Rows[0]["CarrierEmailDomain"].ToString();

					RequestorFirstName = ConvertTools.ToString(dtAlert.Rows[0]["RequestorFirstName"], "");
					RequestorLastName = ConvertTools.ToString(dtAlert.Rows[0]["RequestorLastName"], "");

					int deliveryType = 0;
					QueueMailMessage qmm = new QueueMailMessage();
					qmm.To = RequestorPhoneNumber + RequestorCarrierEmailDomain;
					qmm.From = System.Configuration.ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
					qmm.BCC = qmm.From;
					//qmm.BCC += ";michael.evanko@sunbeltrentals.com";
					qmm.Label = "AlertTextSend";

                    string AlertQAInfo = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? " A:" + AlertId.ToString() + " S:" + ScheduleId.ToString() : "";
                    string AlertOwnerSignature = string.Format(" Created by {0}{1}",
												ConvertTools.ToString(dtAlert.Rows[0]["RequestorFirstName"], "") + " " + ConvertTools.ToString(dtAlert.Rows[0]["RequestorLastName"], ""),
                                                AlertQAInfo );

					switch (TypeId)
					{
						/**************************************************/
						case (int)Alerts.TypeID.EquipmentOnRent:		
						case (int)Alerts.TypeID.ContractEquipmentOnRent:
						case (int)Alerts.TypeID.EquipmentOnRentPastDays:
                        case (int) Alerts.TypeID.ReservationEquipmentOnRent:
							EquipmentOnRent eor = CustomerData.GetCustomerEquipment(AccountNumber, ReferenceValue, IsCorpLink, CompanyID );
							CustomerJob cj = CustomerData.GetCustomerJob(AccountNumber, eor.JobNumber, IsCorpLink, CompanyID);
							
							int EquipmentCount = 0;
							if (TypeId == (int)Alerts.TypeID.EquipmentOnRent || TypeId == (int)Alerts.TypeID.ContractEquipmentOnRent)
							{
                                EquipmentCount = GetScheduledAlertEquipmentCount(AlertDetailsXML);
								qmm.Body = string.Format("Contract {0}(Acct# {1}) has {2} piece{3} of equipment on rent after {4}.",
															ReferenceValue,
															AccountNumber,
															EquipmentCount,
															EquipmentCount == 1 ? "" : "s",
															ParameterValue );
								if( cj.JobName.Trim() != "" )
									qmm.Body += string.Format(" Job site: {0}.",
										                 CultureInfo.CurrentCulture.TextInfo.ToTitleCase(cj.JobName.ToLower().Trim() ));
							}
							else if (TypeId == (int)Alerts.TypeID.EquipmentOnRentPastDays)	
							{
								if (ReferenceValue == "[ANY]")
								{
                                    int ScheduledNumberOfEquipmentPastRent = GetScheduledAlertEquipmentCount(AlertDetailsXML);
									qmm.Body = string.Format("{0} piece{1} of equipment {2} past rent {3} day{4} for Acct# {5}",
													ScheduledNumberOfEquipmentPastRent,
													ScheduledNumberOfEquipmentPastRent < 2 ? "" : "s",
													ScheduledNumberOfEquipmentPastRent < 2 ? "is" : "are",
													ParameterValue,
													Convert.ToInt32(ParameterValue) < 2 ? "" : "s",
													AccountNumber);


								}
								else
								{
									qmm.Body = string.Format("Equipment {0} is past rent {1} day{2} for Acct# {3}.",
																  ReferenceValue,
																  ParameterValue,
																  Convert.ToInt32(ParameterValue) < 2 ? "" : "s",
																  AccountNumber);
								}
							}
                            else if (TypeId == (int)Alerts.TypeID.ReservationEquipmentOnRent)
                            {
                                EquipmentCount = GetScheduledAlertEquipmentCount(AlertDetailsXML);
                                qmm.Body = string.Format("Reservation {0}(Acct# {1}) has {2} piece{3} of equipment on rent after {4}.",
                                                            ReferenceValue,
                                                            AccountNumber,
                                                            EquipmentCount,
                                                            EquipmentCount == 1 ? "" : "s",
                                                            ParameterValue);
                            }

							LogToSQLText = String.Format("Successfully sent equipment alert to: {0}{1} at {2}", RequestorFirstName, RequestorLastName, RequestorPhoneNumber);
							break;

						/**************************************************/
						case (int)Alerts.TypeID.InvoicePastDue: // 2

							int InvoiceCount = 0;
                            InvoiceCount = GetScheduledAlertInvoiceCount(AlertDetailsXML);
							qmm.Body = string.Format("{0} invoice{1} {2} past due for Acct# {3}",
														InvoiceCount,
														InvoiceCount < 2 ? "" : "s",
														InvoiceCount < 2 ? "is" : "are",
														AccountNumber);
							LogToSQLText = String.Format("Successfully sent invoice past due to: {0}{1} at {2}", RequestorFirstName, RequestorLastName, RequestorPhoneNumber);
							break;

						/**************************************************/
						case (int)Alerts.TypeID.BillingMoreThan: //  3 PO Limit Monitor

							decimal scheduleAlertValue = Convert.ToDecimal(dt.Rows[0]["AlertValue"]);

							qmm.Body = string.Format("Billing for PO# {0} is now {1:$#,##0.00;($#,##0.00)}, exceeding {2:$#,##0.00;($#,##0.00)}", 
														ReferenceValue,  
														Convert.ToDecimal(scheduleAlertValue),
														Convert.ToDecimal(ParameterValue));
							LogToSQLText = String.Format("Successfully sent Billing more than to: {0}{1} at {2}", RequestorFirstName, RequestorLastName, RequestorPhoneNumber);
							break;

						/**************************************************/
						case (int)Alerts.TypeID.JobsiteHappening:  // 6
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 0", AccountNumber, IsCorpLink);
							CustomerJob cj2 = CustomerData.GetCustomerJob(AccountNumber, ParameterValue, IsCorpLink, CompanyID);
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 1", AccountNumber, IsCorpLink);
                            List<JobsiteActivityCount> Counts = GetJobsiteCounts(AlertDetailsXML);
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 2", AccountNumber, IsCorpLink);
                            int DeliveryCount = Counts.Find(delegate(JobsiteActivityCount jac) { return (jac.Activity == "Delivery"); }).Count;
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 3", AccountNumber, IsCorpLink);
                            int PickupCount = Counts.Find(delegate(JobsiteActivityCount jac) { return (jac.Activity == "Pickup"); }).Count;
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 4", AccountNumber, IsCorpLink);
                            qmm.Body = string.Format("{0}{1}{2}{3}{4} at job site: {5}.",
														DeliveryCount == 0 ? "" : DeliveryCount.ToString() + " ",
														DeliveryCount == 0 ? "" : (DeliveryCount == 1 ? "delivery" : "deliveries"),
														(DeliveryCount > 0 && PickupCount > 0) ? " and " : "",
														PickupCount == 0 ? "" : PickupCount.ToString() + " ",
														PickupCount == 0 ? "" : (PickupCount == 1 ? "pickup" : "pickups"),
														CultureInfo.CurrentCulture.TextInfo.ToTitleCase(ReferenceValue.ToLower()));
//Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Alert JobsiteHappening 5", AccountNumber, IsCorpLink);
                            LogToSQLText = String.Format("Successfully sent job site happening to: {0}{1} at {2}", RequestorFirstName, RequestorLastName, RequestorPhoneNumber);
							break;

						/**************************************************/
						case (int)Alerts.TypeID.InvoiceNew:   //7
							CustomerJob cj4 = CustomerData.GetCustomerJob(AccountNumber, ParameterValue, IsCorpLink, CompanyID);
                            decimal totaledBilled = GetScheduledNewInvoiceTotaledBilled(AlertDetailsXML);
							int scheduleAlertValueNewInvoices = Convert.ToInt32(dt.Rows[0]["AlertValue"]);

							qmm.Body = string.Format("{0} new invoice{1} generated, totaling {2:$#,##0.00;($#,##0.00)} for Acct# {3}",
														scheduleAlertValueNewInvoices,
														scheduleAlertValueNewInvoices > 1 ? "s" : "",
														totaledBilled,
														AccountNumber);
							LogToSQLText = String.Format("Successfully sent new invoice to: {0}{1} at {2}", RequestorFirstName, RequestorLastName, RequestorPhoneNumber);
							break;

						// Text format: JobName(equipment count) distinct contracts  Example:  VISTAKON(1)  45250512, PCA(2) 45255391, 45255391 ....
						case (int)Alerts.TypeID.AnyNewReservation:   //10
				
							//string text = GetAnyNewReservationText(617, 0);
							qmm.Body = GetAnyNewReservationText(AlertId, ScheduleId);
							break;

						  default:
							NotificationErrorMessage = "Alert type not found: " + TypeId.ToString();
							Sunbelt.Log.LogError(new Exception(NotificationErrorMessage), "Unknowth Alert ID");
							break;
					}

					if( !string.IsNullOrEmpty(qmm.Body) )
					{
						qmm.Body += AlertOwnerSignature;
						qmm.Subject = "Sunbelt Rentals Alert";
						if (TypeId == (int)Alerts.TypeID.AnyNewReservation)
						{
							qmm.BCC = "michael.evanko@sunbeltrentals.com";
							qmm.Subject = "Sunbelt Rentals Any New Reservations";
						}
						Sunbelt.Email.SendEmail(qmm);
						notifyByPhoneSuccess = true;
                        Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Alerts.ToString(), LogToSQLText, AccountNumber, IsCorpLink);
					}
				}
				catch (Exception ex)
				{
					notifyByPhoneSuccess = false;
					NotificationErrorMessage = ex.Message;
					Sunbelt.Log.LogError(ex, "notifyByPhone", referenceId);
				}

                if (notifyByPhone == true && notifyByPhoneSuccess == false)
                {
                    SendProductionSupportErrorMessage(string.Format("Generating Text messaging for alert ID {0} failed.  Email {1}, Error: {2} \r\nReference Id: {3} ", AlertId.ToString(), RequestorPhoneNumber == null ? "" : RequestorPhoneNumber, NotificationErrorMessage, referenceId));
                    AlertErrorEncountered = true;
                }
			}


            Guid UserId = new Guid(dtAlert.Rows[0]["UserId"].ToString());
            DataTable dtUserInfo = Reporting.GetUserEmailAddress(UserId);
			string sEmail = dtAlert.Rows[0]["NotifyEmail"].ToString();

			if( notifyByEmail )
            {
				try
				{
					string url = ToWebAddress("~/Default.aspx?AlertId=" + AlertId + "&Email=" + sEmail + "&Message=" + string.Empty + "&ScheduleId=" + ScheduleId);
					if (referenceId.HasValue)
						url += "&referenceId=" + referenceId.ToString();
					MakeRequest(url);
                    Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Email.ToString(), "Successfully sent Alert to email address " + sEmail, AccountNumber, IsCorpLink);
					//SBNet.Log.LogToSQL(SBNet.Log.Sections.Email, "Successfully sent Alert to email address " + sEmail, AccountNumber, IsCorpLink);
					notifyByEmailSuccess = true;
				}
				catch (Exception ex)
				{
					NotificationErrorMessage = ex.Message;
					notifyByEmailSuccess = false;
					Sunbelt.Log.LogError(ex, "notifyByEmail", referenceId);
					//throw ex;

				}
            }

            if (notifyByEmail == true && notifyByEmailSuccess == false)
            {
                SendProductionSupportErrorMessage(string.Format("Generating alert email for ID {0} failed.  Email {1}, Exception: {2} \r\nReference Id: {3} ", AlertId.ToString(), sEmail, NotificationErrorMessage, referenceId));
                AlertErrorEncountered = true;
            }

			//  Generate text to tag the alert as completed regardless of success or failure.  
			//  Errors are now emailed to Production Support and logged in the schedule history of the failure.
			string TextToTagNotificationComplete = string.Format("Alert failure occurred for AlertID: {0},  Action: {1} ", AlertId.ToString(), Action);

			if( notifyByEmail && notifyByEmailSuccess == true )
				TextToTagNotificationComplete = "Email sent to " + sEmail;

			if (notifyByPhone && notifyByPhoneSuccess == true)
				TextToTagNotificationComplete = string.Format("Text message info: {0} ", LogToSQLText);

            // Mark the alert completed if without error
            // Note:  Always flag as completed, even with error, for now
            // if( AlertErrorEncountered == false)
    			Alerts.AlertNotificationComplete(ScheduleId, TextToTagNotificationComplete );
        }
        else
        {
            Alerts.DeleteAlertById(AlertId);
            return "Unable to find alert id " + AlertId + ".  This alert has been deleted!";
        }

		if(AlertErrorEncountered || !string.IsNullOrEmpty(NotificationErrorMessage))
		{
			//throw execption here...
			throw new Exception( string.Format(" ** Error **  Alert: {0}, Reference Id: {1}, NotificationErrorMessage {2}", AlertId, referenceId, NotificationErrorMessage));
		}

		return "true";
    }

    [WebMethod]
	public string EmailInvoiceOrContract(string EmailAddress, string FilePathWithName, PDFType pdfType, bool IsStrategicAccount, int AccountNumber, Int64 OrderId, string CatClassDesc, DateTime StartDate, int ContractNumber, int PC, Guid referenceId)
    {
        try
        {
            if (pdfType == PDFType.Reservations)
            {
                PdfReader reader = new PdfReader(FilePathWithName);
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                string pdfWithTC = FilePathWithName.Remove(FilePathWithName.LastIndexOf("\\"));
                pdfWithTC += "\\Sunbelt_WebReservation_";
                pdfWithTC += FilePathWithName.Remove(0, FilePathWithName.LastIndexOf("\\") + 1);

                PdfCopy copy = new PdfCopy(document, new FileStream(pdfWithTC, FileMode.Create));

                document.Open();
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    copy.AddPage(copy.GetImportedPage(reader, i));
                }

                string RentalContractTermsAndConditionsPDF = Server.MapPath("~/" +
					ConfigurationManager.AppSettings["RentalContractTermsAndConditionsPDF"].ToString());

				reader = new PdfReader(RentalContractTermsAndConditionsPDF);
                copy.AddPage(copy.GetImportedPage(reader, 1));
                document.Close();

                FileInfo file = new FileInfo(pdfWithTC);
                if (file.Exists)
                {
                    if (IsStrategicAccount)
                    {
                        string url = ToWebAddress("~/Reservations.aspx?Email=" + EmailAddress + "&StrAcct=true" + "&FilePath=" + file.FullName
                            + "&OrderId=" + OrderId + "&RatePC=" + PC);
						url += "&referenceId=" + referenceId.ToString();
 
						StringBuilder strRequest = MakeRequest(url);
                        if (strRequest.ToString().Remove(0, 4).Contains("Error"))
                            return strRequest.ToString();

                        SentOrder(OrderId);

                        string txtMsg = "Resv " + ContractNumber + " by " + EmailAddress + " acct "
                            + AccountNumber + ", out: " + StartDate.Date.ToShortDateString() + "." + CatClassDesc;

                       //ME SendTextMessage(ConfigurationManager.AppSettings["AlertReservationsPhoneNumber"].ToString(), txtMsg);

                        return "true";
                    }
                    else
                    {
                        string url = ToWebAddress("~/Reservations.aspx?Email=" + EmailAddress + "&StrAcct=false"
                            + "&FilePath=" + file.FullName + "&OrderId=" + OrderId + "&RatePC=" + PC);
						url += "&referenceId=" + referenceId.ToString();

						StringBuilder strRequest = MakeRequest(url);
                        if (strRequest.ToString().Remove(0, 4).Contains("Error"))
                            return strRequest.ToString();

                        SentOrder(OrderId);

                        string txtMsg = "Resv " + ContractNumber + " by " + EmailAddress + " acct "
                            + AccountNumber + ", out: " + StartDate.Date.ToShortDateString() + "." + CatClassDesc;

                        // ME SendTextMessage(ConfigurationManager.AppSettings["AlertReservationsPhoneNumber"].ToString(), txtMsg);
                        return "true";
                    }
                }
                else
                {
                    //Log error message saying file did not exist!  Then return the error id
					Sunbelt.Log.LogError(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName + "\r\nReference Id : " + referenceId);
                    return "";
                }
            }
            else if (pdfType == PDFType.Contract)
            {
                PdfReader reader = new PdfReader(FilePathWithName);
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                string pdfWithTC = FilePathWithName.Remove(FilePathWithName.LastIndexOf("\\"));
                pdfWithTC += "\\Sunbelt_Contract_";
                pdfWithTC += FilePathWithName.Remove(0, FilePathWithName.LastIndexOf("\\") + 1);

                PdfCopy copy = new PdfCopy(document, new FileStream(pdfWithTC, FileMode.Create));

                document.Open();
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    copy.AddPage(copy.GetImportedPage(reader, i));
                }

                //string RentalContractTermsAndConditionsPDF = "Rental Contract Terms and Conditions 10-17-2007 RNT8.pdf";
				string RentalContractTermsAndConditionsPDF = System.Configuration.ConfigurationManager.AppSettings["RentalContractTermsAndConditionsPDF"].ToString();
				 
                reader = new PdfReader(Server.MapPath("~/" + RentalContractTermsAndConditionsPDF));
                copy.AddPage(copy.GetImportedPage(reader, 1));
                document.Close();

                string Message = "You have been sent a contract from beta.sunbeltrentals.com.  If you feel you have received this in "
					+ "error please contact Sunbelt Rentals Customer Service at <a href='tel:1-866-786-2358'>1-866-SUNBELT</a>, or by emailing " + ConfigurationSettings.AppSettings["SunbeltCustomerServiceEmail"];

                FileInfo file = new FileInfo(pdfWithTC);
                if (file.Exists)
                {
					QueueMailMessage queue = new QueueMailMessage(
						"Sunbelt Rentals Contract", 
						EmailAddress, 
						ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						System.Configuration.ConfigurationManager.AppSettings["EService_Email"].ToString(),
						"",
						"Sunbelt Rentals Contract",
						Message,
						true,
						file.FullName,
						QueueMailMessage.CleanupType.AttachmentsOnly);

				//	EmailQueue.SendEmail(queue, ConfigurationSettings.AppSettings["EmailQueuePath"]);

                    return "true";
                }
                else
                {
                    //Log error message saying file did not exist!  Then return the error id
					Sunbelt.Log.LogError(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName);
                    return "";
                }
            }
            else if (pdfType == PDFType.Invoice)
            {
                string Message = "You have been sent an invoice from beta.sunbeltrentals.com.  If you feel you have received this in "
					+ "error please contact Sunbelt Rentals Customer Service at <a href='tel:1-866-786-2358'>1-866-SUNBELT</a>, or by emailing " + ConfigurationSettings.AppSettings["SunbeltCustomerServiceEmail"];

                FileInfo file = new FileInfo(FilePathWithName);
                if (file.Exists)
                {
					QueueMailMessage queue = new QueueMailMessage(
						"Sunbelt Rentals Contract",
						EmailAddress,
						ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						System.Configuration.ConfigurationManager.AppSettings["EService_Email"].ToString(),
						"",
						"Sunbelt Rentals Contract",
						Message,
						true,
						file.FullName, 
						QueueMailMessage.CleanupType.AttachmentsOnly);

			//		EmailQueue.SendEmail(queue, ConfigurationSettings.AppSettings["EmailQueuePath"]);
                    return "true";
                }
                else
                {
                    //Log error message saying file did not exist!  Then return the error id
					Sunbelt.Log.LogError(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName + "Reference Id :" + referenceId);
                    return "";
                }
            }
            else
            {
                return "Unknown Type";
            }
        }
        catch (IOException ioe)
        {
			Sunbelt.Log.LogError(ioe, "File path " + FilePathWithName, referenceId);
            return "false";
        }
        catch (Exception ex)
        {
			Sunbelt.Log.LogError(ex, "File path " + FilePathWithName, referenceId);
            return "false";
        }
    }


    [WebMethod]
    public string EmailReservationConfirmation( WebReservationQueueMessage.DocumentType docType, 
                                                int contractNumber,
                                                string FTPServer,
                                                string PDFShare,
                                                Guid referenceId )
    {
        string FilePathWithName= string.Empty;
        bool IsStrategicAccount = false;
        Int64 OrderId=0; 
        string EmailAddress = string.Empty;
        int PC = 0; 
        try
        {
            if (docType != WebReservationQueueMessage.DocumentType.Reservation)
                throw new Exception("DocType for EmailReservationConfirmation() is not a Reservation");

            StringBuilder sb = new StringBuilder();
          
            DataTable dtOrders = new DataTable();
            try
            {
                dtOrders = ReservationsData.GetPendingOrders(contractNumber);
                IsStrategicAccount = Convert.ToBoolean(dtOrders.Rows[0]["IsStrategicAccount"]);
                OrderId = Convert.ToInt64(dtOrders.Rows[0]["OrderID"]);
                PC = Sunbelt.Tools.ConvertTools.ToInt32(dtOrders.Rows[0]["RatePC"], 0);
                EmailAddress = Sunbelt.Tools.ConvertTools.ToString(dtOrders.Rows[0]["EmailAddress"], "");

            }
            catch (Exception ex)
            { 
                sb.Append("Problem retreiving data. " + ex.ToString() + " \r\n");
                Sunbelt.Log.LogError(ex, "Problem retreiving data. ReferenceID: " + referenceId.ToString());

            }

            string PDF_FileName = string.Format("{0:D8}", contractNumber) + "000.PDF";
            FilePathWithName = @"\\" + FTPServer + @"\" + PDFShare + @"\" + PDF_FileName;

            PdfReader reader = new PdfReader(FilePathWithName);
            Document document = new Document(reader.GetPageSizeWithRotation(1));
            string pdfWithTC = FilePathWithName.Remove(FilePathWithName.LastIndexOf("\\"));
            pdfWithTC += "\\Sunbelt_WebReservation_";
            pdfWithTC += FilePathWithName.Remove(0, FilePathWithName.LastIndexOf("\\") + 1);

            PdfCopy copy = new PdfCopy(document, new FileStream(pdfWithTC, FileMode.Create));

            document.Open();
            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                copy.AddPage(copy.GetImportedPage(reader, i));
            }

            string RentalContractTermsAndConditionsPDF = Server.MapPath("~/" +
                ConfigurationManager.AppSettings["RentalContractTermsAndConditionsPDF"].ToString());

            reader = new PdfReader(RentalContractTermsAndConditionsPDF);
            copy.AddPage(copy.GetImportedPage(reader, 1));
            document.Close();

            FileInfo file = new FileInfo(pdfWithTC);
            if (file.Exists)
            {
                if (IsStrategicAccount)
                {
                    string url = ToWebAddress("~/Reservations.aspx?Email=" + EmailAddress + "&StrAcct=true" + "&FilePath=" + file.FullName
                        + "&OrderId=" + OrderId + "&RatePC=" + PC);
					url += "&referenceId=" + referenceId.ToString();

					StringBuilder strRequest = MakeRequest(url);
                    if (strRequest.ToString().Remove(0, 4).Contains("Error"))
                        return strRequest.ToString();

                    SentOrder(OrderId);

                    return "true";
                }
                else
                {
                    string url = ToWebAddress("~/Reservations.aspx?Email=" + EmailAddress + "&StrAcct=false"
                        + "&FilePath=" + file.FullName + "&OrderId=" + OrderId + "&RatePC=" + PC);
					url += "&referenceId=" + referenceId.ToString();
					
					StringBuilder strRequest = MakeRequest(url);
                    if (strRequest.ToString().Remove(0, 4).Contains("Error"))
                        return strRequest.ToString();

                    SentOrder(OrderId);

                    return "true";
                }
            }
            else
            {
                //Log error message saying file did not exist!  Then return the error id
                Sunbelt.Log.LogError(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName + " ReferenceID: " + referenceId.ToString());
                return "";
            }
        }
#if false
            else if (pdfType == PDFType.Contract)
            {
                PdfReader reader = new PdfReader(FilePathWithName);
                Document document = new Document(reader.GetPageSizeWithRotation(1));
                string pdfWithTC = FilePathWithName.Remove(FilePathWithName.LastIndexOf("\\"));
                pdfWithTC += "\\Sunbelt_Contract_";
                pdfWithTC += FilePathWithName.Remove(0, FilePathWithName.LastIndexOf("\\") + 1);

                PdfCopy copy = new PdfCopy(document, new FileStream(pdfWithTC, FileMode.Create));

                document.Open();
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    copy.AddPage(copy.GetImportedPage(reader, i));
                }

                //string RentalContractTermsAndConditionsPDF = "Rental Contract Terms and Conditions 10-17-2007 RNT8.pdf";
                string RentalContractTermsAndConditionsPDF = System.Configuration.ConfigurationManager.AppSettings["RentalContractTermsAndConditionsPDF"].ToString();

                reader = new PdfReader(Server.MapPath("~/" + RentalContractTermsAndConditionsPDF));
                copy.AddPage(copy.GetImportedPage(reader, 1));
                document.Close();

                string Message = "You have been sent a contract from beta.sunbeltrentals.com.  If you feel you have received this in "
                    + "error please contact Sunbelt Rentals Customer Service at <a href="tel:1-866-786-2358">1-866-SUNBELT</a>, or by emailing " + ConfigurationSettings.AppSettings["SunbeltCustomerServiceEmail"];

                FileInfo file = new FileInfo(pdfWithTC);
                if (file.Exists)
                {
                    QueueMailMessage queue = new QueueMailMessage(
                        "Sunbelt Rentals Contract",
                        EmailAddress,
                        ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
                        System.Configuration.ConfigurationManager.AppSettings["EService_Email"].ToString(),
                        "",
                        "Sunbelt Rentals Contract",
                        Message,
                        true,
                        file.FullName,
                        QueueMailMessage.CleanupType.AttachmentsOnly);

                    EmailQueue.SendEmail(queue, ConfigurationSettings.AppSettings["EmailQueuePath"]);

                    return "true";
                }
                else
                {
                    //Log error message saying file did not exist!  Then return the error id
                    SBNet.Error.LogErrToSQL(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName);
                    return "";
                }
            }
            else if (pdfType == PDFType.Invoice)
            {
                string Message = "You have been sent an invoice from beta.sunbeltrentals.com.  If you feel you have received this in "
                    + "error please contact Sunbelt Rentals Customer Service at <a href="tel:1-866-786-2358">1-866-SUNBELT</a>, or by emailing " + ConfigurationSettings.AppSettings["SunbeltCustomerServiceEmail"];

                FileInfo file = new FileInfo(FilePathWithName);
                if (file.Exists)
                {
                    QueueMailMessage queue = new QueueMailMessage(
                        "Sunbelt Rentals Contract",
                        EmailAddress,
                        ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
                        System.Configuration.ConfigurationManager.AppSettings["EService_Email"].ToString(),
                        "",
                        "Sunbelt Rentals Contract",
                        Message,
                        true,
                        file.FullName,
                        QueueMailMessage.CleanupType.AttachmentsOnly);

                    EmailQueue.SendEmail(queue, ConfigurationSettings.AppSettings["EmailQueuePath"]);
                    return "true";
                }
                else
                {
                    //Log error message saying file did not exist!  Then return the error id
                    SBNet.Error.LogErrToSQL(new Exception("File does not exist"), "Unable to find the file " + FilePathWithName);
                    return "";
                }
            }
            else
            {
                return "Unknown Type";
            }
#endif
        catch (IOException ioe)
        {
            Sunbelt.Log.LogError(ioe, "File path " + FilePathWithName + " ReferenceID: " + referenceId.ToString());
            return "false";
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogError(ex, "File path " + FilePathWithName + " ReferenceID: " + referenceId.ToString());
            return "false";
        }

    }

    
    [WebMethod]
    public string SendTextMessage( string phoneNumbers, string message, string carrier )
    {
		DataTable dtSMSCarriers;
		DataColumn[] MyKey = new DataColumn[1];
		dtSMSCarriers = Alerts.GetSMSCarriers();

		MyKey[0] = dtSMSCarriers.Columns["CarrierID"];
		dtSMSCarriers.PrimaryKey = MyKey;
		DataRow[] dtr = dtSMSCarriers.Select("CarrierName like '%" + carrier + "%'");
        Guid referenceId = new Guid();
        try
        {
			if (!string.IsNullOrEmpty(phoneNumbers) && !string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(carrier) && (dtr.Length > 0 ))
            {
				QueueMailMessage qmm = new QueueMailMessage();

				string CarrierEmailDomain = string.Empty;
				if (dtr.Length > 0)
					CarrierEmailDomain = dtr[0]["CarrierEmailDomain"].ToString();

	
				// switching away from Celltrust.com text messaging.
				string[] numbers = phoneNumbers.Split(';');

				string ProcessedPhoneNumbers = string.Empty;
				string separator = "";
				foreach (string num in numbers)
				{
					if (num.Trim().Length == 10)
					{
						ProcessedPhoneNumbers += separator + num.Trim() + CarrierEmailDomain;
						separator = ";";
					}
				}


				qmm.To = ProcessedPhoneNumbers;
				qmm.From = System.Configuration.ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
				qmm.Label = "AlertTestSend";
				qmm.Subject = "Alert Test Send";

			//	Sunbelt.Email.SendEmail(qmm);

                return "Successfully Sent Text Message";
            }
            else
                return "Phone number or message is null";
        }
        catch (Exception ex)
        {
			Sunbelt.Log.LogError(ex, "Error sending text message.  " + message, referenceId);
            return ex.ToString() + "\r\nReference Id: " + referenceId;
        }
    }
    [WebMethod]
    public QueueMailMessage CreateReservationEmail(Int64 ReservationOrOrderID, bool isReservationID, string emailAddress, string filePath)
    {
        QueueMailMessage queueMailMessage = GetReservationEmailRecipients( ReservationOrOrderID, isReservationID, emailAddress );
        queueMailMessage.Label = "ReservationEmail";

        string sEmailMessageBody = "";
        queueMailMessage.BCC += ";michael.evanko@sunbeltrentals.com";

        int AccountNumber;
        DataTable dt = new DataTable();
        using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
		using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", conn))
        {
            sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, ReservationOrOrderID);
            dt = sp.ExecuteDataTable();
            AccountNumber = Convert.ToInt32(dt.Rows[0]["AccountNumber"]);
        }

        Page page = new FormlessPage();
        UserControl ctrlReservationRequestBody = (UserControl)page.LoadControl("~/UserControls/ReservationRequestEmail.ascx");
        page.Controls.Add(ctrlReservationRequestBody);
        GridView gv = (GridView)ctrlReservationRequestBody.FindControl("gvItems");

        gv.DataSource = dt;
        gv.DataBind();

        StringWriter swNew = new StringWriter();
        HtmlTextWriter htwNew = new HtmlTextWriter(swNew);
        StringWriter HTMLResponse = new StringWriter();
        HttpContext.Current.Server.Execute(page, HTMLResponse, false);
        sEmailMessageBody = HTMLResponse.ToString();

        if (AccountNumber == 0) // if Cash Customer
        {
            try
            {
                SBNet.SBNetSqlMembershipProvider p = (SBNet.SBNetSqlMembershipProvider)System.Web.Security.Membership.Provider;

                SBNet.CashCustomer cc = new SBNet.CashCustomer();
                cc = p.GetCashCustomer(emailAddress);
                Customer customer = CustomerData.GetCustomer(cc.DriverLicenseNumber, cc.DriverLicenseState);

                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", cc.FirstName + " " + cc.LastName);
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", customer.Address.Address1.Trim());
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", customer.Address.City.Trim() + ", " + customer.Address.State.Trim() + " " + customer.Address.Zip.Trim());
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", customer.Address.City.Trim() + ", " + customer.Address.State.Trim() + " " + customer.Address.Zip.Trim());
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", customer.PrimaryPhone.Replace(")", ") "));
            }
            catch (Exception ex)
            {
                string x = ex.Message;
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", "");
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", "");
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", "");
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", "");
                sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", "");
            }
        }
        else
        {
            //MembershipUser mu = Membership.GetUser(EmailAddress, false);
            Customer cust = CustomerData.GetCustomer(AccountNumber);
            sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ACCOUNT_NAME]", cust.CompanyName + " (Acct. #" + cust.AccountNumber.ToString() + ")");
            sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_ADDRESS1-2]", cust.Address.Address1.Trim());
            sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_CITY_STATE_ZIP]", cust.Address.City.Trim() + ", " + cust.Address.State.Trim() + " " + cust.Address.Zip.Trim());
            sEmailMessageBody = sEmailMessageBody.Replace("[CUSTOMER_PHONE]", cust.PrimaryPhone.Replace(")", ") "));

        }

        DateTime dtRentalStartDate = Convert.ToDateTime(dt.Rows[0]["StartDate"]);
        DateTime dtRentalEndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"]);
        DateTime dtReservationDate = Convert.ToDateTime(dt.Rows[0]["OrderCreationDate"]);

        string RentalStartDate = String.Format("{0:ddd}", dtRentalStartDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalStartDate);
        string RentalEndDate = String.Format("{0:ddd}", dtRentalEndDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtRentalEndDate);
        string ReservationDate = String.Format("{0:ddd}", dtReservationDate) + ",&nbsp;" + String.Format("{0:MM/dd/yyyy  h:mm tt}", dtReservationDate);

        sEmailMessageBody = sEmailMessageBody.Replace("[RESERVATION_NUMBER]", dt.Rows[0]["ContractNumber"].ToString());
        sEmailMessageBody = sEmailMessageBody.Replace("[RENTAL_START_DATE]", RentalStartDate);
        sEmailMessageBody = sEmailMessageBody.Replace("[RENTAL_RETURN_DATE]", RentalEndDate);
        sEmailMessageBody = sEmailMessageBody.Replace("[RENTAL_ORDER_DATE]", ReservationDate);


        string subject = (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QATEST - " : "" ) + "Sunbelt Rentals Reservation";
        queueMailMessage.Subject = subject;
        queueMailMessage.Body    = sEmailMessageBody;
        queueMailMessage.Attachment = filePath;
        queueMailMessage.AsHTML = true;

        // PROCDUCTION NOTE: Will need to change to AttachmentsOnly for production.
        queueMailMessage.CleanupAttachments = QueueMailMessage.CleanupType.AttachmentsOnly;
        queueMailMessage.Date = DateTime.Now;

        return queueMailMessage;
    }

    [WebMethod]
    public QueueMailMessage CreateGearBoxEmail( GearBox_TransactionType gearBox_TransactionType, 
                                                Int32 accountNumber, 
                                                Int32 contractNumber,
                                                Int32 batchId,
                                                Int32 location, 
                                                string filePath, 
                                                string user,
                                                bool IsCustomerEquipment )
    {
        Sunbelt.Log.LogActivity("CreateGearBoxEmail", string.Format("GearBox_TransactionType: {0},  accountNumber: {1}, contractNumber: {2}, batchId: {3}, location: {4}, filePath {5}, user: {6}, IsCustomerEquipment: {7} ", gearBox_TransactionType, 
                                                 accountNumber, 
                                                 contractNumber,
                                                 batchId,
                                                 location, 
                                                 filePath,
                                                 user, 
                                                 IsCustomerEquipment));

        QueueMailMessage queueMailMessage = new QueueMailMessage();
        queueMailMessage.Label = string.Format("{0} {1} {2}  {3}", "GearBox", gearBox_TransactionType.ToString(), contractNumber, DateTime.Now.ToString());

        string sEmailMessageBody = string.Empty;
        queueMailMessage.BCC += ";michael.evanko@sunbeltrentals.com";

        FormlessPage page = new FormlessPage();
        UserControl ctrlReservationRequestBody = (UserControl)page.LoadControl("~/UserControls/GearBoxTransactionEmail.ascx", 
                                                                                gearBox_TransactionType,
                                                                                accountNumber,
                                                                                contractNumber,
                                                                                batchId,
                                                                                location,
                                                                                user,
                                                                                IsCustomerEquipment );

        page.Controls.Add( ctrlReservationRequestBody );

        StringWriter swNew = new StringWriter();
        HtmlTextWriter htwNew = new HtmlTextWriter(swNew);
        StringWriter HTMLResponse = new StringWriter();
        HttpContext.Current.Server.Execute(page, HTMLResponse, false);
        sEmailMessageBody = HTMLResponse.ToString();
        string gearBox_subject = string.Empty;
        switch (gearBox_TransactionType)
        {
            case GearBox_TransactionType.RentalContract:
                if( IsCustomerEquipment == false )
                    gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Rental Contract #{1}", location, contractNumber);
                else
					gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Customer Owned Equipment Issue #{1}", location, (contractNumber * -1));
                break;

			case GearBox_TransactionType.RentalReturn:
                if (IsCustomerEquipment == false)
                    gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Rental Return #{1}", location, contractNumber);
                else
                    gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Customer Owned Equipment Returned #{1}", location, (contractNumber * -1));
                break;

			case GearBox_TransactionType.SalesInvoice:
                gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Sales Invoice #{1}", location, contractNumber);
                break;

			default:
                gearBox_subject = string.Format("Sunbelt Rentals Gear Box {0} Unknown Transaction #{1}", location, contractNumber);
                break;

        }
        string subject = (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QATEST - " : "") + gearBox_subject;
        queueMailMessage.Subject = subject;
        queueMailMessage.Body = sEmailMessageBody;
        queueMailMessage.AsHTML = true;

        // PRODUCTION NOTE: Will need to change to AttachmentsOnly for production.
        if (IsCustomerEquipment == false)
        {
            queueMailMessage.Attachment = filePath;
            queueMailMessage.CleanupAttachments = QueueMailMessage.CleanupType.AttachmentsOnly;
        }
        queueMailMessage.Date = DateTime.Now;

        return queueMailMessage;
    }
    [WebMethod]
    public string GetReservationEmailsExtraCCs(int PC, int accountNumber )
    {
        string extraCCs = string.Empty;

        // extraCCs = Reporting.GetCCEmailAddresses(PC, accountNumber);
        extraCCs = GetAdditionalCCEmailAddresses(PC, accountNumber, EmailType.Reservation);
        
        return extraCCs;
    }

	[WebMethod]
	public QueueMailMessage GetReservationEmailRecipients(Int64 ReservationOrOrderID, bool isReservationID, string emailAddress )
	{
        QueueMailMessage Q = new QueueMailMessage();
        Q.Label = "GetReservationEmailRecipients";

        Int64 OrderId;
        string EmailAddress = string.Empty;
        bool StrAcct = false;
        int PC=-1;
        int AccountNumber = -1;
        string AddedCCEmailRecipients = string.Empty;
        bool WebsiteReservation = false, NonWebsiteReservation = false;
		int CompanyID = 0;

        // Assumption the reservation is from the Customer website.
        DataTable dtORDERS_GetOrderItems = new DataTable();
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
		using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", conn))
        {
            if (isReservationID == true)
                sp.AddParameterWithValue("ReservationID", SqlDbType.Int, 5, ParameterDirection.Input, ReservationOrOrderID);
            else
                sp.AddParameterWithValue("OrderID", SqlDbType.Int, 5, ParameterDirection.Input, ReservationOrOrderID);
            dtORDERS_GetOrderItems = sp.ExecuteDataTable();
        }
        if (dtORDERS_GetOrderItems.Rows.Count > 0)
        {
			OrderId       = ReservationOrOrderID;
			EmailAddress  = dtORDERS_GetOrderItems.Rows[0]["UserID"].ToString();
            PC            = Convert.ToInt32(dtORDERS_GetOrderItems.Rows[0]["RatePC"]);
            AccountNumber = Convert.ToInt32(dtORDERS_GetOrderItems.Rows[0]["AccountNumber"]);
            StrAcct       = Convert.ToBoolean(dtORDERS_GetOrderItems.Rows[0]["IsStrategicAccount"]);
			CompanyID     = Convert.ToInt32(dtORDERS_GetOrderItems.Rows[0]["CompanyID"]);
            // This comes from reservations added at the time the reservation was submitted
            AddedCCEmailRecipients = dtORDERS_GetOrderItems.Rows[0]["EmailCCRecipients"].ToString();
            if( string.IsNullOrEmpty(AddedCCEmailRecipients) == false )
            {
                AddedCCEmailRecipients = EmailAddressQAAdjuster(AddedCCEmailRecipients);  // Email Receipients are from  website reservation only
                if (AddedCCEmailRecipients.Length > 0)
                    AddedCCEmailRecipients = "," + AddedCCEmailRecipients;
            }
            WebsiteReservation = true;
        }
        else
        {   // if no records are returned, reservation may of came from phone or AS400
			Quote quote = Sunbelt.BusinessObjects.CustomerData.GetCustomerQuote((int)ReservationOrOrderID, CompanyID);
            if (quote != null)
            {
                OrderId = ReservationOrOrderID;
                EmailAddress = emailAddress;
                AccountNumber = quote.AccountNumber;
                PC = quote.PC;
                StrAcct = quote.IsStrategicAccount;
                NonWebsiteReservation = true;
            }
        }


        if (WebsiteReservation || NonWebsiteReservation)
        {
            string sTo = string.Empty;
            string sFrom = string.Empty;
            string sCC = string.Empty; ;
            string sBCC = string.Empty; ;
            string sSubject = string.Empty; ;

            string BaseSubjectText = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QATEST - " : "";

			string sPC = string.Format("{0:000}",PC);

            LoadEmailAddresses(EmailAddress, AddedCCEmailRecipients, AccountNumber, StrAcct, PC, ref Q);

#if false
                if (StrAcct)
                {
                    if (EmailAddress.EndsWith(".gov") || EmailAddress.EndsWith(".mil"))
                    {
						sTo = EmailAddress;
						sFrom = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
						sCC = ConfigurationManager.AppSettings["EService_Email"].ToString() + ","
							+ ConfigurationManager.AppSettings["StrategicAccount_Email"].ToString() + ","
							+ ConfigurationManager.AppSettings["PCEmail"].ToString() + "M" + sPC + "@sunbeltrentals.com,"
							+ ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "@sunbeltrentals.com,"
							+ ConfigurationManager.AppSettings["GSAEmailGroup"].ToString()
							+ DM_EmailAddress
							+ ExtraCCs
                            + AddedEmailRecipients;
						sBCC = "";
						sSubject = BaseSubjectText;
                    }
                    else
                    {
						sTo = EmailAddress;
						sFrom = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
						sCC = ConfigurationManager.AppSettings["EService_Email"].ToString() + ","
							+ ConfigurationManager.AppSettings["StrategicAccount_Email"].ToString() + ","
							+ ConfigurationManager.AppSettings["PCEmail"].ToString() + "M" + sPC + "@sunbeltrentals.com,"
							+ ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "@sunbeltrentals.com"
							+ DM_EmailAddress
                            + ExtraCCs
                            + AddedEmailRecipients;
                        sBCC = "";
						sSubject = BaseSubjectText;
                    }
                }
                else
                {
					if (EmailAddress.EndsWith(".gov") || EmailAddress.EndsWith(".mil"))
                    {
						sTo   = EmailAddress;
						sFrom = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
						sCC   = ConfigurationManager.AppSettings["EService_Email"].ToString() + ","
								+ ConfigurationManager.AppSettings["PCEmail"].ToString() + "M" + sPC + "@sunbeltrentals.com,"
								+ ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "@sunbeltrentals.com,"
								+ ConfigurationManager.AppSettings["GSAEmailGroup"].ToString()
							    + DM_EmailAddress
                            + ExtraCCs
                            + AddedEmailRecipients;
                        sBCC = "";
						sSubject = BaseSubjectText;
                    }
                    else
                    {
						sTo   = EmailAddress;
						sFrom = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
						sCC   = ConfigurationManager.AppSettings["EService_Email"].ToString() + ","
								+ ConfigurationManager.AppSettings["PCEmail"].ToString() + "M" + sPC + "@sunbeltrentals.com,"
								+ ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "@sunbeltrentals.com"
							    + DM_EmailAddress
                            + ExtraCCs
                            + AddedEmailRecipients;
                        sBCC = "";
						sSubject = BaseSubjectText;
                    }
                }
				sBCC = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
				Q.To = sTo;
				Q.From = sFrom;
				Q.CC = sCC;
				Q.BCC = sBCC;
				Q.Subject = sSubject;
				Q.AsHTML = true;
 #endif            
        }
       else
        {
			Exception ex = new Exception("Problem generating reservation cancellation QueueMailMessage object on webservice. " + String.Format("Reservation orderID {0:d} returned no orders", ReservationOrOrderID));
			Sunbelt.Log.LogError(ex, "WebService.cs");
            Q.ErrMsg = ex.Message;
            return Q;
        }

		return Q;
	}

    // toEmailAddress
    // addedEmailRecipients - this addresses come from the www website when reservation is made.  Become part of the CC addresses.
    // accountNumber        - what it is
    // isStrategicAccount   - what it is
    // q                    - Queue mail object which is updated with all the reciepents that need to know for reservation, equipment, etc... 
    [WebMethod]
    public void LoadEmailAddresses(string toEmailAddress, string addedCCEmailRecipients, int accountNumber, bool isStrategicAccount, int PC, ref QueueMailMessage q)
    {
            q.To   = string.Empty;
            q.From = string.Empty;
            q.CC   = string.Empty;
            q.BCC  = string.Empty;
            q.Subject = string.Empty;

            string BaseSubjectText = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QATEST - " : "";

			string sPC = string.Format("{0:000}",PC);

			string DM_EmailAddress = Reporting.GetDM_EmailAddress(PC);
			if (DM_EmailAddress != string.Empty)
			{
				DM_EmailAddress = "," + (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QA_" : "") + DM_EmailAddress;
			}
			else
                Sunbelt.Log.LogError(new Exception("Problem getting DM email address for PC: " + PC.ToString() + " for the reservation.aspx page on webservice"), String.Format("Reservation orderID {0:d} returned no orders", PC));

			//string ExtraCCs = Reporting.GetCCEmailAddresses(PC, accountNumber );
            //if (accountNumber == 31107)
            //    ExtraCCs += "," + (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QA_" : "") + "grutkowski@sunbeltrentals.com";

            string ExtraCCs = GetAdditionalCCEmailAddresses(PC, accountNumber, EmailType.Reservation);
            if (string.IsNullOrEmpty(ExtraCCs) == false)
                ExtraCCs = "," + ExtraCCs;

            q.To   = toEmailAddress;
			q.From = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
			q.CC   = ConfigurationManager.AppSettings["EService_Email"].ToString() + ","
                    + ConfigurationManager.AppSettings["PCEmail"].ToString() + "M" + sPC + "@sunbeltrentals.com,"
                    + ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "@sunbeltrentals.com," 
					+ ConfigurationManager.AppSettings["PCEmail"].ToString() + sPC + "DSP@sunbeltrentals.com"
                    + DM_EmailAddress
                    + ExtraCCs
                    + addedCCEmailRecipients;
            if (toEmailAddress.EndsWith(".gov") || toEmailAddress.EndsWith(".mil"))
                q.CC = q.CC + "," + ConfigurationManager.AppSettings["GSAEmailGroup"].ToString();

            if( isStrategicAccount )
                q.CC = q.CC + "," + ConfigurationManager.AppSettings["StrategicAccount_Email"].ToString();

        q.BCC = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();
		q.Subject = BaseSubjectText;
		q.AsHTML = true;
	}

    public static StringBuilder MakeRequest(string url)
    {
        StringBuilder sb = new StringBuilder();
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
		
        // Set some reasonable limits on resources used by this request
        //request.MaximumAutomaticRedirections = 4;
        //request.MaximumResponseHeadersLength = 4;
        // Set credentials to use for this request.
        request.Credentials = CredentialCache.DefaultCredentials;

		// The number of milliseconds to wait before the request times out. The default value is 100,000 milliseconds (100 seconds).
		request.Timeout = (int)TimeSpan.FromMinutes(15).TotalMilliseconds;  //900000; // 15 minutes(900,000 micro-seconds) for disney corp account request of 43,000 record
        request.ReadWriteTimeout = (int)TimeSpan.FromMinutes(15).TotalMilliseconds;   //32000;

        Stopwatch stopWatch = new Stopwatch();
		stopWatch.Start();

		try
		{
			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			{
				// Get the stream associated with the response.
				using (Stream receiveStream = response.GetResponseStream())
				{
					// Pipes the stream to a higher level stream reader with the required encoding format. 
					StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

					sb.Append(readStream.ReadToEnd());
				}
			}
		}
		catch (Exception ex)
		{
			stopWatch.Stop();
			//string duration = stopWatch.Elapsed.ToString(@"hh\:mm\:ss.fff");
			Sunbelt.Log.LogError(new Exception("NO ERRROR TEST Exception"), "Report", string.Format("Report:{0}, ElapsedTime:{1}", url, "HEY"));

			string elapsedTimeString = string.Format("{0}:{1}:{2}:{3}",
										  stopWatch.Elapsed.Hours.ToString("00"),
										  stopWatch.Elapsed.Minutes.ToString("00"),
										  stopWatch.Elapsed.Seconds.ToString("00"),
										  stopWatch.Elapsed.Milliseconds.ToString("00"));
			Sunbelt.Log.LogError(ex, "Report", string.Format("Report:{0}, ElapsedTime:{1}", url, elapsedTimeString));
			//Sunbelt.Log.LogError(ex, "Report", $"Successfully processed Report ID {report.UserReportId},  duration({watch.Elapsed.ToString(@"hh\:mm\:ss.fff")})");

			
			throw ex;
		}
		stopWatch.Stop();
		string elapsedTimeString2 = string.Format("{0}:{1}:{2}:{3}",
								  stopWatch.Elapsed.Hours.ToString("00"),
								  stopWatch.Elapsed.Minutes.ToString("00"),
								  stopWatch.Elapsed.Seconds.ToString("00"),
								  stopWatch.Elapsed.Milliseconds.ToString("00"));
		Sunbelt.Log.LogActivity("Report", string.Format("Report:{0}, ElapsedTime:{1}", url, elapsedTimeString2));

        return sb;
    }


    /// <summary>
    /// Adds the left part of the URL (ie. "http//domain-name/app-name/") to virtural path 'sPath'.
    /// </summary>
    /// <param name="sPath"></param>
    /// <returns></returns>
    public static string ToWebAddress(string sPath)
    {
        string webAddress;

        webAddress = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) +
            HttpContext.Current.Request.ApplicationPath;
        if (sPath.Length > 0)
        {
            //Add forward slash to web address.
            if (webAddress[webAddress.Length - 1] != '/')
                webAddress += "/";

            //Strip off "~" if needed.
            if ((sPath.Length > 0) && (sPath[0] == '~') && (sPath[1] == '/'))
                sPath = sPath.Substring(1);
            //Remove the ApplicationPath from sPath if supplied.
            if (HttpContext.Current.Request.ApplicationPath != "/")
                sPath = sPath.Replace(HttpContext.Current.Request.ApplicationPath, "");
            //Strip off '/' if needed.
            if (sPath[0] == '/')
                sPath = sPath.Substring(1);
            webAddress += sPath;
        }
        return webAddress;
    }

    [WebMethod]
    public int ReservationEmailSent( Int64 OrderId )
    {
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
		using (SPData sp = new SPData("dbo.ORDERS_SetEmailSent", conn))
        {
            sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
            return sp.ExecuteNonQuery();
        }
        //return SentOrder(OrderId);
    }

    public static int SentOrder(Int64 OrderId)
    {
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
		using (SPData sp = new SPData("dbo.ORDERS_SetEmailSent", conn))
        {
            sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
            return sp.ExecuteNonQuery();
        }
    }

	public static void SendProductionSupportErrorMessage( string Message)
	{
		string ProductionSite = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) != true ? "QA" : "";
		//QueueMailMessage queue = new QueueMailMessage( "CustomerExtranetWebService"
		//                                                ,ConfigurationManager.AppSettings["ProductionSupportEmail"].ToString() 
		//                                                ,ConfigurationManager.AppSettings["SunbeltEmail"].ToString()
		//                                                ,""
		//                                                ,""
		//                                                , ProductionSite +  "CustomerExtranetWebService Error"
		//                                                ,Message
		//                                                ,true
		//                                                ,""
		//                                                , QueueMailMessage.CleanupType.None );

		//EmailQueue.SendEmail(queue, ConfigurationSettings.AppSettings["EmailQueuePath"]);

		//Sunbelt.Email.SendEmail(ConfigurationManager.AppSettings["ProductionSupportEmail"].ToString()
		//                        , ConfigurationManager.AppSettings["SunbeltEmail"].ToString()
		//                        , ""
		//                        , ""
		//                        , ProductionSite + "CustomerExtranetWebService Error"
		//                        , Message
		//                        , false);

		Sunbelt.Email.SendEmail("michael.evanko@sunbeltrentals.com"
								, ConfigurationManager.AppSettings["SunbeltEmail"].ToString()
								, ""
								, ""
								, ProductionSite + "CustomerExtranetWebService Error"
								, Message
								, false);

	}

    private int GetScheduledAlertEquipmentCount(string alertDetailsXML )
	{
		var TotalQuantity = "";
		try
		{
            System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(alertDetailsXML);

            var TotalQuantityX = from item in xDoc.Descendants("AlertData")
				select (string)item.Attribute("TotalQuantity");

			foreach (string name in TotalQuantityX)
				TotalQuantity = name;

            return Convert.ToInt32(TotalQuantity);

		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control" );
		}
        return Convert.ToInt32(TotalQuantity);
	}

	private int GetScheduledAlertInvoiceCount( string alertDetailsXML )
	{
		var InvoiceCount = "";
		try
		{
			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(alertDetailsXML);

			var InvoiceCountX = from item in xDoc.Descendants("Invoices")
								 select (string)item.Attribute("InvoiceCount");

			foreach (string name in InvoiceCountX)
				InvoiceCount = name;

			return Convert.ToInt32(InvoiceCount);

		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control" );
		}
		return Convert.ToInt32(InvoiceCount);
	}

	public class JobsiteActivityCount
	{
		public string Activity { get; set; }
		public int Count { get; set; }
	}

    private List<JobsiteActivityCount> GetJobsiteCounts(string alertDetailsXML)
	{
		try
		{
			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(alertDetailsXML);

			List<JobsiteActivityCount> counts = (from item in xDoc.Descendants("Equipment")
						  group item by (item.Attribute("PickupTicketNumber") != null) into DorP
						  select new JobsiteActivityCount { Activity = (DorP.Key == true ? "Pickup": "Delivery"), Count = DorP.Count() }).ToList();

			return counts;

		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control");
		}
		return null;
	}

    private decimal GetScheduledNewInvoiceTotaledBilled(string alertDetailsXML)
	{
		decimal newInvoices = 0M;
		try
		{
			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(alertDetailsXML);

			var newInvoicesCount = from item in xDoc.Descendants("Invoices")
								   select (string)item.Attribute("TotalBilled");

			foreach (string count in newInvoicesCount) // should only be one and is the one we want
				newInvoices = Convert.ToDecimal(count);  
		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control");
		}
		return newInvoices;
	}

    //private int GetScheduledNumberOfEquipmentPastRent(int SchedulePendingID)
    //{
    //    string AlertDetailsXML = string.Empty;
    //    int NumberOfEquipment = 0;
    //    try
    //    {
    //        DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

    //        if (dt.Rows.Count == 0)
    //            throw new Exception("No alert information found for scheduled ID item.");
     
    //        if (dt.Rows.Count > 0)
    //            AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();

    //        System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);

    //        var vNumberOfEquipment = from item in xDoc.Descendants("AlertData")
    //                               select (string)item.Attribute("TotalQuantity");

    //        foreach (string count in vNumberOfEquipment) // should only be one and is the one we want
    //            NumberOfEquipment = Convert.ToInt32(count);  
    //    }
    //    catch (Exception ex)
    //    {
	//        Sunbelt.Log.LogError(ex, "EquipmentOnRentByContract User Control");
    //    }
    //    return NumberOfEquipment;
    //}

    public enum EmailType { All = 0, Reservation, ServiceRequest, ReturnEquipment, RentalExtension };

    [WebMethod]
    public string GetAdditionalCCEmailAddresses(int PC, int accountNumber, EmailType emailType)
    {
//        using (SqlConnection conn = new SqlConnection(ConfigManager.ConnectionType.BaseData.ToString()))
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["BaseData"].ConnectionString))
        {
            string ExtraCCs = string.Empty;

            conn.Open();

            using (SqlCommand command = new SqlCommand("config.sb_GetEmailsExtraCCs", conn))
            {
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@PC", PC));
                if (accountNumber > 0)
                    command.Parameters.Add(new SqlParameter("@CustomerNumber", accountNumber));

                try
                {
                    using (SqlDataAdapter mustAdapt = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        mustAdapt.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            switch (emailType)
                            {
                                case EmailType.All:
                                    break;
                                case EmailType.Reservation:
                                    ExtraCCs = dt.Rows[0]["ReservationExtraCCs"].ToString();
                                    break;
                                case EmailType.ServiceRequest:
                                    ExtraCCs = dt.Rows[0]["ServiceExtraCCs"].ToString();
                                    break;
                                case EmailType.ReturnEquipment:
                                    ExtraCCs = dt.Rows[0]["PickupExtraCCs"].ToString();
                                    break;
                                case EmailType.RentalExtension:
                                    ExtraCCs = dt.Rows[0]["ReservationExtraCCs"].ToString();
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            Sunbelt.Log.LogActivity("EVANKO1", string.Format("Email Type: {0},  Emails: {1}, (PC: {2}, account: {3})", emailType.ToString(), ExtraCCs.Trim(), PC,accountNumber));
            return EmailAddressQAAdjuster(ExtraCCs.Trim());
        }
    }

    [WebMethod]
    public string EmailAddressQAAdjuster(string CSVEmailAddress)
    {
        string[] addresses;
        string reConstitutedEmailAddresses = string.Empty;
        string QAMoniker = Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QA_" : "";
        string emailAddressSeperator = "";

        if( CSVEmailAddress.Trim() == string.Empty )
            return string.Empty;

        CSVEmailAddress = CSVEmailAddress.Replace(',', ';');  // Making sure database does not use the combination of ; and , for seperators.
        addresses = CSVEmailAddress.Split(';');
        foreach (string address in addresses)
        {
            reConstitutedEmailAddresses += emailAddressSeperator + QAMoniker + address;
            emailAddressSeperator = ",";
        }
        return reConstitutedEmailAddresses;
    }


	public string GetAnyNewReservationText(int AlertID, int SchedulePendingID)
	{
		string AlertDetailsXML = string.Empty;

		try
		{
			if (SchedulePendingID == 0)
			{
				//AlertDetailsXML = TestXMLEquipment2;
			}
			else
			{
				DataTable dt = Alerts.GetScheduleAlertPendingByID(SchedulePendingID);

				if (dt.Rows.Count == 0)
					throw new Exception("No alert information found for scheduled ID item.");

				if (dt.Rows.Count > 0)
					AlertDetailsXML = dt.Rows[0]["AlertDetails"].ToString();
			}
			System.Xml.Linq.XDocument xDoc = System.Xml.Linq.XDocument.Parse(AlertDetailsXML);
			var items = from item in xDoc.Descendants("JobSite")
						select new
						{
							JobName = item.Attribute("JobName") != null ? item.Attribute("JobName").Value : "",
							JobAddress1 = item.Attribute("JobAddress1") != null ? item.Attribute("JobAddress1").Value : "",
							JobCity = item.Attribute("JobCity") != null ? item.Attribute("JobCity").Value : "",
							JobState = item.Attribute("JobState") != null ? item.Attribute("JobState").Value : "",
							JobZIP = item.Attribute("JobZIP") != null ? item.Attribute("JobZIP").Value : "",
							Equipment = item.Elements("Equipment").ToArray()
						};
			string job = string.Empty;
			string  body = string.Empty;
			string separaterJob = "";
			foreach (var item in items) {
				body = body + separaterJob;
				body = body + (item.JobName + " (" + item.Equipment.Count().ToString() + ") ");
				string separatorContact = "";
				foreach (var e in item.Equipment)
				{
					string c = e.Attribute("ContractNumber").Value;
					if( body.IndexOf(c) < 0 )
						body = body + separatorContact + (e.Attribute("ContractNumber").Value);
					separatorContact = " ";
				}
				separaterJob = ", ";
			}
			return body.ToString();

		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "GetAnyNewReservationText ");
		}
		return "";
	}

    class ReservationsData
    {
        public static DataTable GetPendingOrders()
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
			using (SPData sp = new SPData("dbo.ORDERS_GetPendingEmail", conn))
            {
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetPendingOrders( int ContractNumber )
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
			using (SPData sp = new SPData("dbo.ORDERS_GetPendingEmail", conn))
            {
                sp.AddParameterWithValue("@ContractNumber", SqlDbType.Int, 5, ParameterDirection.Input, ContractNumber);
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetOrderItems(Int64 OrderID)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
			using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", conn))
            {
                sp.AddParameterWithValue("OrderId", SqlDbType.Int, 5, ParameterDirection.Input, OrderID);
                return sp.ExecuteDataTable();
            }
        }

        public static int SentOrder(Int64 OrderId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData))
			using (SPData sp = new SPData("dbo.ORDERS_SetEmailSent", conn))
            {
                sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
                return sp.ExecuteNonQuery();
            }
        }
    }
}


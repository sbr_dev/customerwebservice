using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Common.DAL;
using System.Text;
using System.Diagnostics;

namespace SBNet
{
	/// <summary>
	/// Summary description for Error
	/// </summary>
	public class Error
	{
        public enum Sections { PDF, Alerts, Reports, Login }

		public sealed class ExceptionInfo
		{
			public Exception ExecptionObject = null;
			public int ReferenceNumber = 0;

			public ExceptionInfo(Exception ex, int referenceNumber)
			{
				ExecptionObject = ex;
				ReferenceNumber = referenceNumber;
			}
		}
		
		public Error()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		static public void HandleError(Exception ex, bool redirectToErrorPage)
		{
			HandleError(ex, redirectToErrorPage, "");
		}

		static public void HandleError(Exception ex, bool redirectToErrorPage, string comment)
		{
			string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
			int rc = 0;
			try
			{
				//LOG ERROR HERE...
				rc = LogErrToSQL(ex, comment);  //Write to error log table
			}
			catch
			{
				//Logging failed...
				//Nothing for now.
			}

			if (redirectToErrorPage)
			{
				ExceptionInfo ei = new ExceptionInfo(ex, rc);

				if (HttpContext.Current.Session != null)
					HttpContext.Current.Session["_EXCEPTION_"] = ei;
				else
				if (HttpContext.Current.Application != null)
					HttpContext.Current.Application["_EXCEPTION_"] = ei;
				else
					throw ex;

				HttpContext.Current.Server.Transfer("~/Error/Error.aspx");
			}
		}
		

		public static int LogErrToSQL(Exception ex, string Comments)
		{
			string ErrNumber = "";
			string Routine = "";
			string SQL = "";
			string stackTrace = "";

			if (ex is SqlException)
			{
				stackTrace = BuildExceptionText((SqlException)ex);
			}
			else //All others.
			{
				stackTrace = BuildExceptionText(ex);
			}

            try
            {
                using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("AppLog.sb_InsError", conn))
                {
                    sp.AddParameterWithValue("App", SqlDbType.VarChar, 50, ParameterDirection.Input, ConfigurationManager.AppSettings["ApplicationName"].ToString());
                    sp.AddParameterWithValue("Host", SqlDbType.VarChar, 50, ParameterDirection.Input, HttpContext.Current.Request.UserHostAddress.ToString());
                    sp.AddParameterWithValue("PageURL", SqlDbType.VarChar, 1000, ParameterDirection.Input, HttpContext.Current.Request.Url.ToString());
                    sp.AddParameterWithValue("UserID", SqlDbType.UniqueIdentifier, 1000, ParameterDirection.Input, DBNull.Value );
                    sp.AddParameterWithValue("ErrNumber", SqlDbType.VarChar, 25, ParameterDirection.Input, ErrNumber);
                    sp.AddParameterWithValue("ErrMessage", SqlDbType.VarChar, 500, ParameterDirection.Input, ex.Message);
                    sp.AddParameterWithValue("StackTrace", SqlDbType.VarChar, 3000, ParameterDirection.Input, stackTrace);
                    sp.AddParameterWithValue("RoutineName", SqlDbType.VarChar, 100, ParameterDirection.Input, Routine);
                    sp.AddParameterWithValue("SQL", SqlDbType.VarChar, 1000, ParameterDirection.Input, SQL);
                    sp.AddParameterWithValue("Comments", SqlDbType.VarChar, 1000, ParameterDirection.Input, Comments);
                    sp.AddParameter("rc", SqlDbType.Int, 5, ParameterDirection.ReturnValue);
                    sp.ExecuteNonQuery();

                    Sunbelt.Email.SendEmail( ConfigurationManager.AppSettings["ProductionSupportEmail"]      // To
						                     , ConfigurationManager.AppSettings["SunbeltEmail"].ToString()   // From
											 , null                                                          // CC 
											 , null															 // BCC
 									         , "Sunbelt Error " + Convert.ToInt32(sp.Parameters["returnValue"].Value).ToString()
											 , ex.Message.ToString()
											 , false);

                    return (Convert.ToInt32(sp.Parameters["returnValue"].Value));
                }
            }
            catch (Exception ex2)
            {
                //If there's an error in the errorlogger, let it go! Let it go!
                Debug.WriteLine(ex2.Message);
            }
			return 0;
		}

		private static string BuildExceptionText(SqlException ex)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(GetUserNames());
			if (ex.Errors.Count > 0)
			{
				foreach (SqlError sqle in ex.Errors)
				{
					sb.Append("-- Error --\r\n");
					sb.Append("  Message: " + sqle.Message + "\r\n");
					sb.Append("  Number: " + sqle.Number + "\r\n");
					sb.Append("  Procedure: " + sqle.Procedure + "\r\n");
					sb.Append("  Server: " + sqle.Server + "\r\n");
					sb.Append("  Source: " + sqle.Source + "\r\n");
					sb.Append("  State: " + sqle.State + "\r\n");
					sb.Append("  Severity: " + sqle.Class + "\r\n");
					sb.Append("  LineNumber: " + sqle.LineNumber + "\r\n");
				}
			}
			else
			{
				sb.Append("-- Error --\r\n");
				sb.Append("Message: " + ex.Message + "\r\n");
				sb.Append("Number: " + ex.Number + "\r\n");
			}
			sb.Append("-----------\r\n");
			sb.Append(GetStackTrace(ex) + "\r\n");

			return sb.ToString();
		}

		private static string BuildExceptionText(Exception ex)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(GetUserNames());
			sb.Append("-- Error --\r\n");
			sb.Append("Type: " + ex.GetType().ToString() + "\r\n");
			sb.Append("Message: " + ex.Message + "\r\n");
			sb.Append("Source: " + ex.Source + "\r\n");

			sb.Append("-----------\r\n");
			sb.Append(GetStackTrace(ex) + "\r\n");

			return sb.ToString();
		}

		private static string GetUserNames()
		{
			StringBuilder sb = new StringBuilder();

			try { sb.Append("Sunbelt User: " + HttpContext.Current.Session["UserID"].ToString() + "\r\n"); }
			catch { sb.Append("Sunbelt User: NOT VALID\r\n"); }

			sb.Append("HttpContext User: " + HttpContext.Current.User.Identity.Name + "\r\n");
			sb.Append("Windows Identity: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + "\r\n");
			return sb.ToString();
		}

		private static string GetStackTrace(Exception ex)
		{
			StringBuilder sb = new StringBuilder();

			if (ex.StackTrace != null)
				sb.Append(ex.StackTrace.ToString());
			else
				sb.Append("  No stack trace available.");
			return sb.ToString();
		}

        public static void LogErrToSQL(Exception ex, string ErrNumber, string Routine, string SQL, string Comments)
        {
            try
			{
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("AppLog.sb_InsError", conn))
                {
                    sp.AddParameterWithValue("App", SqlDbType.VarChar, 50, ParameterDirection.Input, ConfigurationManager.AppSettings["ApplicationName"].ToString());
                    sp.AddParameterWithValue("Host", SqlDbType.VarChar, 50, ParameterDirection.Input, HttpContext.Current.Request.UserHostAddress.ToString());
                    sp.AddParameterWithValue("PageURL", SqlDbType.VarChar, 1000, ParameterDirection.Input, HttpContext.Current.Request.Url.ToString());
                    sp.AddParameterWithValue("UserID", SqlDbType.UniqueIdentifier, 1000, ParameterDirection.Input, new Guid(HttpContext.Current.User.Identity.ToString()));
                    sp.AddParameterWithValue("ErrNumber", SqlDbType.VarChar, 25, ParameterDirection.Input, ErrNumber);
                    sp.AddParameterWithValue("ErrMessage", SqlDbType.VarChar, 500, ParameterDirection.Input, ex.Message);
                    sp.AddParameterWithValue("StackTrace", SqlDbType.VarChar, 3000, ParameterDirection.Input, ex.StackTrace);
                    sp.AddParameterWithValue("RoutineName", SqlDbType.VarChar, 100, ParameterDirection.Input, Routine);
                    sp.AddParameterWithValue("SQL", SqlDbType.VarChar, 1000, ParameterDirection.Input, SQL);
                    sp.AddParameterWithValue("Comments", SqlDbType.VarChar, 1000, ParameterDirection.Input, Comments);
                    sp.ExecuteNonQuery();
                }
			}
			catch (Exception ex2)
			{
				//If there's an error in the errorlogger, let it go! Let it go!
				Debug.WriteLine(ex2.Message);
			}
		}
        public static int LogErrToSQL(Exception ex, string Comments, Sections Section, Int32 AccountNumber, bool IsCorpLink)
        {
            string ErrNumber = "";
            string Routine = "";
            string SQL = "";
            string stackTrace = "";

            if (ex is SqlException)
            {
                stackTrace = BuildExceptionText((SqlException)ex);
            }
            else //All others.
            {
                stackTrace = BuildExceptionText(ex);
            }

            MembershipUser mu = Membership.GetUser();
            string userId = "";
            Guid UserId = new Guid();
            if (mu == null)
                userId = null;
            else
                UserId = new Guid(mu.ProviderUserKey.ToString());

            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("AppLog.sb_InsError", conn))
                {
                    sp.AddParameterWithValue("App", SqlDbType.VarChar, 50, ParameterDirection.Input, ConfigurationManager.AppSettings["ApplicationName"].ToString());
                    sp.AddParameterWithValue("Host", SqlDbType.VarChar, 50, ParameterDirection.Input, HttpContext.Current.Request.UserHostAddress.ToString());
                    sp.AddParameterWithValue("PageURL", SqlDbType.VarChar, 1000, ParameterDirection.Input, HttpContext.Current.Request.Url.ToString());
                    if (userId == null)
                        sp.AddParameterWithValue("UserID", SqlDbType.UniqueIdentifier, 1000, ParameterDirection.Input, userId);
                    else
                        sp.AddParameterWithValue("UserID", SqlDbType.UniqueIdentifier, 1000, ParameterDirection.Input, UserId);
                    sp.AddParameterWithValue("AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                    sp.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLink);
                    sp.AddParameterWithValue("Section", SqlDbType.VarChar, 50, ParameterDirection.Input, Section.ToString());

                    sp.AddParameterWithValue("ErrNumber", SqlDbType.VarChar, 25, ParameterDirection.Input, ErrNumber);
                    sp.AddParameterWithValue("ErrMessage", SqlDbType.VarChar, 500, ParameterDirection.Input, ex.Message);
                    sp.AddParameterWithValue("StackTrace", SqlDbType.VarChar, 3000, ParameterDirection.Input, stackTrace);
                    sp.AddParameterWithValue("RoutineName", SqlDbType.VarChar, 100, ParameterDirection.Input, Routine);
                    sp.AddParameterWithValue("SQL", SqlDbType.VarChar, 1000, ParameterDirection.Input, SQL);
                    sp.AddParameterWithValue("Comments", SqlDbType.VarChar, 1000, ParameterDirection.Input, Comments);
                    sp.AddParameter("rc", SqlDbType.Int, 5, ParameterDirection.ReturnValue);
                    sp.ExecuteNonQuery();
                    return (Convert.ToInt32(sp.Parameters["returnValue"].Value));
                }
            }
            catch (Exception ex2)
            {
                //If there's an error in the errorlogger, let it go! Let it go!
                Debug.WriteLine(ex2.Message);
            }
            return 0;
        }

	}
}
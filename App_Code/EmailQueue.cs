﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;

namespace Sunbelt.Queue
{
	/// <summary>
	/// Used to serialize a MailMessage object to MSMQ.
	/// </summary>
	[Serializable]
	public class QueueMailMessage
	{
		public enum CleanupType { None, AttachmentsOnly, AttachmentsAndFolder }
		
		public string Label { get; set; }
		public string To { get; set; }
		public string From { get; set; }
		public string CC { get; set; }
		public string BCC { get; set; }
		public string Subject { get; set; }
		public string Body { get; set; }
		public bool AsHTML { get; set; }
		public string Attachment { get; set; }
		public DateTime Date { get; set; }
		public CleanupType CleanupAttachments { get; set; }
		public string ErrMsg { get; set; }
		public bool ReturnToSenderOnErr { get; set; }

		public QueueMailMessage()
		{
			Date = DateTime.Now;
		}
		
		public QueueMailMessage(string label, string to,
			string from, string cc, string bcc,
			string subject, string body, bool asHTML,
			string attachment, CleanupType cleanupAttachments)
		{
			Label = label;
			To = to;
			From = from;
			CC = cc;
			BCC = bcc;
			Subject = subject;
			Body = body;
			AsHTML = asHTML;
			Attachment = attachment;
			Date = DateTime.Now;
			CleanupAttachments = cleanupAttachments;
		}
	}

	public class EmailQueue : QueueBase<QueueMailMessage>
	{
		public EmailQueue(string path) : base(path)
		{
		}

		public void Send(QueueMailMessage qmm)
		{
			Queue.Send(qmm, qmm.Label);
		}

		static public void SendEmail(QueueMailMessage qmm, string path)
		{
			EmailQueue eq = new EmailQueue(path);
			eq.Send(qmm, qmm.Label);
		}
	}
}

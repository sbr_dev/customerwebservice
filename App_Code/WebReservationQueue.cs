﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Messaging;
using Sunbelt.AS400;

namespace Sunbelt.Queue
{
    public enum GearBox_TransactionType
    { 
        RentalContract = 0,
        RentalReturn = 1,
        SalesInvoice = 2
    }
	public sealed class WebReservationSource
	{
		public const string SunbeltRentalsWebSite = "SunbeltRentalsWebSite";
		public const string MobileSalesProApp = "MobileSalesProApp";
        public const string CustomerWebSite = "CustomerWebSite";
        public const string AccelerateApp = "AccelerateApp";
        public const string GearBox = "GearBoxApp";
    }
	
	/// <summary>
	/// Used to serialize a MailMessage object to MSMQ.
	/// </summary>
	[Serializable]
	public class WebReservationQueueMessage
	{
		public enum DocumentType
		{
			Invoice = 0,
			Contract = 1,
			Quote = 2,
			Reservation = 3,
		}

		public Guid MessageId { get; set; }
		public string Label { get; set; }
		public string Source { get; set; }   //From WebReservationSource class.
		public int ContractNumber { get; set; }
		public int SequenceNumber { get; set; }
		public int ReservationNumber { get; set; }
		public DocumentType DocType { get; set; }
		public string ReferenceNumber { get; set; }
		public string PlacedBy { get; set; }
		public int PC { get; set; }
		public int SaleRep { get; set; }
		public string EmailAddress { get; set; }
		public bool CreatePDFOnly { get; set; }
		public DateTime Date { get; set; }
		public string ErrMsg { get; set; }
		public string FTPServer { get; set; }
		public string PDFShare { get; set; }
        public QueueMailMessage QueueMailMsg { get; set; }
        public string OnStartURLCallBack { get; set; }
        public string OnEndURLCallBack { get; set; }
		
		// --- The following properties are used by the problem queue.
		public PDFReprintResponse ReprintCode { get; set; }
		public int RetryCount { get; set; }

		public WebReservationQueueMessage()
		{
			MessageId = Guid.NewGuid(); 
			Date = DateTime.Now;
			ReprintCode = PDFReprintResponse.NONE;
		}
		
		public WebReservationQueueMessage(string label, string source,
			int contractNumber, int sequenceNumber, int reservationNumber,
			DocumentType docType, string referenceNumber, string placedBy,
			int pc, int salesRep, string emailAddress) : this()
		{
			MessageId = Guid.NewGuid();
			Label = label;
			Source = source;
			ContractNumber = contractNumber;
			SequenceNumber = sequenceNumber;
			ReservationNumber = reservationNumber;
			ReferenceNumber = referenceNumber;
			DocType = docType;
			PlacedBy = placedBy;
			PC = pc;
			SaleRep = salesRep;
			EmailAddress = emailAddress;
			Date = DateTime.Now;
			ReprintCode = PDFReprintResponse.NONE;
            OnStartURLCallBack = string.Empty;
            OnEndURLCallBack = string.Empty;

		}
        public WebReservationQueueMessage(string label, string source,
            int contractNumber, int sequenceNumber, int reservationNumber,
            DocumentType docType, string referenceNumber, string placedBy,
			int pc, int salesRep, string emailAddress, string ftpServer, string pdfShare)
			: this()
        {
            MessageId = Guid.NewGuid();
            Label = label;
            Source = source;
            ContractNumber = contractNumber;
            SequenceNumber = sequenceNumber;
            ReservationNumber = reservationNumber;
            ReferenceNumber = referenceNumber;
            DocType = docType;
            PlacedBy = placedBy;
            PC = pc;
            SaleRep = salesRep;
            EmailAddress = emailAddress;
            Date = DateTime.Now;
            ReprintCode = PDFReprintResponse.NONE;
            FTPServer = ftpServer;
            PDFShare = pdfShare;
            OnStartURLCallBack = string.Empty;
            OnEndURLCallBack = string.Empty;
        }
        public WebReservationQueueMessage(string label, string source,
                                            int contractNumber, int sequenceNumber, int reservationNumber,
                                            DocumentType docType, string referenceNumber, string placedBy,
                                            int pc, int salesRep, string emailAddress, string ftpServer,
											string pdfShare, QueueMailMessage qmm)
			: this()
        {
            MessageId = Guid.NewGuid();
            Label = label;
            Source = source;
            ContractNumber = contractNumber;
            SequenceNumber = sequenceNumber;
            ReservationNumber = reservationNumber;
            ReferenceNumber = referenceNumber;
            DocType = docType;
            PlacedBy = placedBy;
            PC = pc;
            SaleRep = salesRep;
            EmailAddress = emailAddress;
            Date = DateTime.Now;
            ReprintCode = PDFReprintResponse.NONE;
            FTPServer = ftpServer;
            PDFShare = pdfShare;
            QueueMailMsg = qmm;
            OnStartURLCallBack = string.Empty;
            OnEndURLCallBack = string.Empty;
        }
    
    }

	public class WebReservationQueue : QueueBase<WebReservationQueueMessage>
	{
		public WebReservationQueue(string path)
			: base(path)
		{
		}

		public void Send(WebReservationQueueMessage wrqm)
		{
			Queue.Send(wrqm, wrqm.Label);
		}

		static public void SendReservation(WebReservationQueueMessage wrqm, string path)
		{
			WebReservationQueue wrq = new WebReservationQueue(path);
			wrq.Send(wrqm, wrqm.Label);
		}
	}
}

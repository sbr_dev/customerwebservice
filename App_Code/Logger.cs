using System;
using System.IO;
using Sunbelt.Tools;

namespace Sunbelt
{
	/// <summary>
	/// Summary description for Logger.
	/// </summary>
	public class Logger : IDisposable
	{
		public enum LogBy { ReuseFile, DailyFile, WeeklyFile, MonthlyFile };
		public bool AutoCleanup = true;		
		private System.IO.StreamWriter swLog = null;
		private string sLogFileName;
		private string sLogFilePath;
		private LogBy logByType = LogBy.ReuseFile;
		private DateTime dtLast;

		static private object writeLock = new object();

		public Logger(string logFileName, LogBy type)
		{
			Initialize(logFileName, type, true);
		}

		public Logger(string logFileName, LogBy type, bool bAutoCleanup)
		{
			Initialize(logFileName, type, bAutoCleanup);
		}

		~Logger()
		{
			// Simply call Dispose(false).
			Dispose (false);
		}
		
		private void Initialize(string logFileName, LogBy type, bool bAutoCleanup)
		{
			sLogFileName = logFileName;
			sLogFilePath = sLogFileName;
			logByType = type;
			AutoCleanup = bAutoCleanup;
			dtLast = DateTime.MinValue;
			CheckFileName(DateTime.Now);
			if(swLog == null)
				swLog = new StreamWriter(sLogFilePath, true);
		}

		public void Message(string text)
		{
			DateTime dtNow = DateTime.Now;
			lock (writeLock)
			{
				CheckFileName(dtNow);

				//Add to log.
				try
				{
					swLog.Write(DateTimeTools.ToSQLString(dtNow) + " - ");
					swLog.WriteLine(text);
				}
				catch { }
				
			}
		}

		public void Message()
		{
			DateTime dtNow = DateTime.Now;
			lock (writeLock)
			{
				CheckFileName(dtNow);

				//Add to log.
				try
				{
					swLog.WriteLine();
				}
				catch { }
			}
		}

		public void Update()
		{
			lock (writeLock)
			{
				swLog.Flush();
			}
		}

		private void CheckFileName(DateTime dt)
		{
			if(logByType != LogBy.ReuseFile)
			{
				bool bUpdateFile = false;

				//Check if new day.
				if(dtLast == DateTime.MinValue)
					bUpdateFile = true;
				else
				if((logByType == LogBy.DailyFile) && (dtLast.Day != dt.Day))
					bUpdateFile = true;
				else
				if((logByType == LogBy.MonthlyFile) && (dtLast.Month != dt.Month))
					bUpdateFile = true;
				else
				if(logByType == LogBy.WeeklyFile)
				{
					DateTime dt1 = DateTimeTools.ToFirstDayOfWeek(dtLast);
					DateTime dt2 = DateTimeTools.ToLastDayOfWeek(dtLast);
					if(dt < dt1 || dt > dt2)
						bUpdateFile = true;
				}

				if(bUpdateFile)
				{
					//Close current file.
					if(swLog != null)
					{
						swLog.Flush();
						swLog.Close();
					}
					//Cleanup if needed.
					if(AutoCleanup)
						DoAutoCleanup();
					//Get new file.
					sLogFilePath = GenerateDateTimeFileName(dt);
					//Open new file.
					swLog = new StreamWriter(sLogFilePath, true);
					//Remember the dt.
					dtLast = dt;
				}
			}
		}

		private string GenerateDateTimeFileName(DateTime dt)
		{
			string fileSuffix; 
			if(logByType == LogBy.DailyFile)
				fileSuffix = DateTimeTools.ToDateFileString(dt);
			else
			if(logByType == LogBy.WeeklyFile)
				fileSuffix = DateTimeTools.ToDateFileString(
					DateTimeTools.ToFirstDayOfWeek(dt));
			else
			//if(logByType == LogBy.MonthlyFile)
				fileSuffix = DateTimeTools.ToYearMonthFileString(dt);
			
			//Append the date on the file name.
			string file = Path.GetFileNameWithoutExtension(sLogFileName) +
				fileSuffix + Path.GetExtension(sLogFileName);				
			string path = Path.GetDirectoryName(sLogFileName);
			if(path[path.Length-1] != '\\')
				path += '\\';
			path += file;
			return path;
		}

		private void DoAutoCleanup()
		{
			DateTime dtFile = DateTime.MinValue;
			switch(logByType)
			{
				case LogBy.DailyFile:
					//Keep 10 days of files.
					dtFile = DateTime.Now.AddDays(-11);
					break;
				case LogBy.WeeklyFile:
					//Keep 2 months of files.
					dtFile = DateTime.Now.AddMonths(-2);
					break;
				default: //LogBy.MonthlyFile
					//Keep 6 months of files.
					dtFile = DateTime.Now.AddMonths(-7);
					break;
			}
			//Find the files.
			string path = Path.GetDirectoryName(sLogFileName);
			string files = Path.GetFileNameWithoutExtension(sLogFileName) +
				"*" + Path.GetExtension(sLogFileName);				
			// Create a reference to directory.
			DirectoryInfo di = new DirectoryInfo(path);
			// Create an array of the files.
			FileInfo[] fileList = di.GetFiles(files);

			if(fileList.Length > 0)
			{
				//foreach(FileInfo fi in fileList)
				for(int i = 0; i < fileList.Length; i++)
				{
					FileInfo fi = fileList[i];

					//Delete it if older than dtFile.
					if(fi.CreationTime < dtFile)
					{
						//Delete the file.
						fi.Delete();
					}
				}
			}
		}
		
		#region IDisposable Members

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing) 
		{
			if (disposing) 
			{
				// Free other state (managed objects).
				if(swLog != null)
				{
					swLog.Flush();
					swLog.Close();
				}
				swLog = null;
			}
			// Free your own state (unmanaged objects).
			// Set large fields to null.
		}

		#endregion
	}
}

using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Collections.Generic;
using Sunbelt.Tools;
using System.IO;
using System.Xml;

namespace SBNet
{
	/// <summary>
	/// Summary description for SBNetTools
	/// </summary>
	public sealed class Tools
	{
		/// <summary>
		/// Includes 'EntryControl.js' in the page.
		/// </summary>
		/// <param name="myPage">My page.</param>
		public static void IncludeEntryControlsClientScript(Page myPage)
		{
			IncludeSBNetClientScript(myPage);
			//Register the EntryControl.js file.
			if (!myPage.ClientScript.IsClientScriptIncludeRegistered("EntryControl.js"))
				myPage.ClientScript.RegisterClientScriptInclude("EntryControl.js", myPage.ResolveUrl("~/ClientScript/EntryControl.js"));
		}

		/// <summary>
		/// Includes 'SildingDIV.js' in the page.
		/// </summary>
		/// <param name="myPage">My page.</param>
		public static void IncludeSlidingDivClientScript(Page myPage)
		{
			IncludeSBNetClientScript(myPage);
			//Register the SlidingDiv.js file.
			if (!myPage.ClientScript.IsClientScriptIncludeRegistered("SlidingDiv.js"))
				myPage.ClientScript.RegisterClientScriptInclude("SlidingDiv.js", myPage.ResolveUrl("~/ClientScript/SlidingDIV.js"));
		}

		/// <summary>
		/// Includes 'Browser.js' in the page.
		/// </summary>
		/// <param name="myPage">My page.</param>
		public static void IncludeBrowserClientScript(Page myPage)
		{
			IncludeSBNetClientScript(myPage);
			//Register the Browser.js file.
			if (!myPage.ClientScript.IsClientScriptIncludeRegistered("Browser.js"))
				myPage.ClientScript.RegisterClientScriptInclude("Browser.js", myPage.ResolveUrl("~/ClientScript/Browser.js"));
		}

		/// <summary>
		/// Includes 'XML.js' in the page.
		/// </summary>
		/// <param name="myPage">My page.</param>
		public static void IncludeXMLClientScript(Page myPage)
		{
			IncludeSBNetClientScript(myPage);
			//Register the Browser.js file.
			if (!myPage.ClientScript.IsClientScriptIncludeRegistered("XML.js"))
				myPage.ClientScript.RegisterClientScriptInclude("XML.js", myPage.ResolveUrl("~/ClientScript/XML.js"));
		}

		/// <summary>
		/// Includes 'SBNet.js' in the page.
		/// </summary>
		/// <param name="myPage">My page.</param>
		public static void IncludeSBNetClientScript(Page myPage)
		{
			//Register the SBNet.js file.
			if (!myPage.ClientScript.IsClientScriptIncludeRegistered("SBNet.js"))
			{
				StringBuilder sb = new StringBuilder();

				//Add startup code that will make the document.onmousedown call the SBNet.Util.HideDivList() method.
				sb.Append(" SBNet.Util.AttachEvent(document, \"onmousedown\", SBNet.Util.HideDivList);\r\n");
				
				//Add startup code to set the webAddress.
				sb.Append("SBNet.Util.webAddress = \"" + WebTools.ToWebAddress("/") + "\";\r\n");

				myPage.ClientScript.RegisterStartupScript(myPage.GetType(), "SBNet.js", sb.ToString(), true);
				
				myPage.ClientScript.RegisterClientScriptInclude("SBNet.js", myPage.ResolveUrl("~/ClientScript/SBNet.js"));
			}
		}
	}

	public sealed class Data
	{
		/// <summary>
		/// Checks 'text' for unsafe ASP.NET characters.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static bool IsSafe(string text)
		{
			#region RegexBuddy
			// [\w\s'".,~`!@#$%\&\*\(\)\-\+=\{\}\[\]:;\?\\/\\|]*
			// 
			// Match a single character present in the list below �[\w\s'".,~`!@#$%\&\*\(\)\-\+=\{\}\[\]:;\?\\/\\|]*�
			//    Between zero and unlimited times, as many times as possible, giving back as needed (greedy) �*�
			//    Match a single character that is a "word character" (letters, digits, etc.) �\w�
			//    Match a single character that is a "whitespace character" (spaces, tabs, line breaks, etc.) �\s�
			//    One of the characters "'".,~`!@#$%" �'".,~`!@#$%�
			//    A & character �\&�
			//    A * character �\*�
			//    A ( character �\(�
			//    A ) character �\)�
			//    A - character �\-�
			//    A + character �\+�
			//    The character "=" �=�
			//    A { character �\{�
			//    A } character �\}�
			//    A [ character �\[�
			//    A ] character �\]�
			//    One of the characters ":;" �:;�
			//    A ? character �\?�
			//    A \ character �\\�
			//    The character "/" �/�
			//    A \ character �\\�
			//    The character "|" �|�
			#endregion
			return Regex.IsMatch(text,
				"\\A(?:[\\w\\s'\".,~`!@#$%\\&\\*\\(\\)\\-\\+=\\{\\}\\[\\]:;\\?\\\\/\\\\|]*)\\z",
				RegexOptions.Multiline);
		}

		/// <summary>
		/// Returns an ASP.NET safe string from 'text'. Truncates to 'length' if needed.
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string MakeSafe(string text, int length)
		{
			string s = MakeSafe(text);
			if (s.Length > length)
				return s.Substring(0, length);
			return s;
		}

		/// <summary>
		/// Returns an ASP.NET safe string from 'text'.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string MakeSafe(string text)
		{
			return Regex.Replace(text,
				"[^\\w\\s'\".,~`!@#$%\\&\\*\\(\\)\\-\\+=\\{\\}\\[\\]:;\\?\\\\/\\\\|]*",
				"",
				RegexOptions.Multiline);
		}

		/// <summary>
		/// Makes SQL safe text. Removes all Regex non-word characters except space, single quote, and double quote.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string MakeSqlSafe(string text)
		{
			#region RegexBuddy
			// [^\w '"]*
			// 
			// Match a single character NOT present in the list below �[^\w '"]*�
			//    Between zero and unlimited times, as many times as possible, giving back as needed (greedy) �*�
			//    Match a single character that is a "word character" (letters, digits, etc.) �\w�
			//    One of the characters " '"" � '"�
			#endregion
			return Regex.Replace(text, "[^\\w \'\"\\-,]*", "", RegexOptions.Multiline);
		}

		/// <summary>
		/// Makes SQL parameter safe text. Removes all Regex non-word characters except space, single quote, and double quote.
		/// Turns single quote into double single quote.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string MakeSqlParameterSafe(string text)
		{
			return MakeSqlSafe(text).Replace("'", "''");
		}

		/// <summary>
		/// Creates a DataTable from a Generic.List<T> object.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="list">The list.</param>
		/// <returns></returns>
		public static DataTable ListToDataTable<T>(List<T> list)
		{
			DataTable dt = new DataTable(list.GetType().Name);

			//Create table columns.
			Type type = typeof(T);
			PropertyInfo[] piList = type.GetProperties();
			foreach (PropertyInfo pi in piList)
			{
				DataColumn column = new DataColumn();
				column.DataType = pi.PropertyType;
				column.ColumnName = pi.Name;
				//column.ReadOnly = true;
				//column.Unique = true;
				// Add the Column to the DataColumnCollection.
				dt.Columns.Add(column);
			}
			
			if (list.Count > 0)
			{
				//Fill the table.
				foreach(object obj in list)
				{
					DataRow dr = dt.NewRow();
					foreach (PropertyInfo pi in piList)
					{
						dr[pi.Name] = pi.GetValue(obj, null);
					}
					dt.Rows.Add(dr);
				}
			}
			return dt;
		}

		/// <summary>
		/// This method will write all table colums to CSV.
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		static public System.Text.StringBuilder DataTableToCSV(DataTable dt)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			for (int i = 0; i < dt.Columns.Count; i++)
			{
				sb.Append(FormatText(dt.Columns[i].ColumnName));
				if (i < dt.Columns.Count - 1)
					sb.Append(",");
			}
			sb.Append(Environment.NewLine);

            if (dt.Rows.Count == 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append("There were no records returned!");
                    if (i < dt.Columns.Count - 1)
                        sb.Append(",");
                }
                return sb;
            }

			for (int j = 0; j < dt.Rows.Count; j++)
			{
				for (int i = 0; i < dt.Columns.Count; i++)
				{
					sb.Append(FormatText(dt.Rows[j][i].ToString().Trim()));
					if (i < dt.Columns.Count - 1)
						sb.Append(",");
				}
				sb.Append(Environment.NewLine);
			}
			return sb;
		}

        static public System.Text.StringBuilder DataTableToXML(DataTable dt, string DataTableName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (dt.Rows.Count == 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    sb.Append("<" + dt.Columns[i].ColumnName + ">No Records Found</" + dt.Columns[i].ColumnName + ">");
                }
                return sb;
            }

            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                dt.TableName = DataTableName;
                dt.WriteXml(sw);
                sb.Append(sw.ToString());
            }
            return sb;
        }

		static private string FormatText(string text)
		{
			return (char)34 + text.Replace("\"", "\"\"") + (char)34;
		}

		/// <summary>
		/// Creates a shallow copy of object using MemberwiseClone.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static object ShallowClone(object obj)
		{
			return typeof(object).GetMethod("MemberwiseClone", System.Reflection.BindingFlags.NonPublic
			 | System.Reflection.BindingFlags.Instance).Invoke(obj, new object[0]);
		}

		/// <summary>
		/// Creates a deep copy of object using serialization.
		/// </summary>
		/// <param name="obj">The obj.</param>
		/// <returns></returns>
		public static object DeepClone(object obj)
		{
			System.IO.MemoryStream ms = new System.IO.MemoryStream();
			System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf =
				new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			bf.Serialize(ms, obj);
			ms.Flush();
			ms.Seek(0, System.IO.SeekOrigin.Begin);
			return bf.Deserialize(ms);
		}
	}

	/// <summary> 
	/// Cleans paths of invalid characters. 
	/// </summary> 
	public static class PathSanitizer
	{
		/// <summary> 
		/// The set of invalid filename characters, kept sorted for fast binary search 
		/// </summary> 
		private readonly static char[] invalidFilenameChars;
		/// <summary> 
		/// The set of invalid path characters, kept sorted for fast binary search 
		/// </summary> 
		private readonly static char[] invalidPathChars;

		static PathSanitizer()
		{
			// set up the two arrays -- sorted once for speed. 
			invalidFilenameChars = System.IO.Path.GetInvalidFileNameChars();
			invalidPathChars = System.IO.Path.GetInvalidPathChars();
			Array.Sort(invalidFilenameChars);
			Array.Sort(invalidPathChars);

		}

		/// <summary> 
		/// Cleans a filename of invalid characters 
		/// </summary> 
		/// <param name="input">the string to clean</param> 
		/// <param name="errorChar">the character which replaces bad characters</param> 
		/// <returns></returns> 
		public static string SanitizeFilename(string input, char errorChar)
		{
			return Sanitize(input, invalidFilenameChars, errorChar);
		}

		/// <summary> 
		/// Cleans a path of invalid characters 
		/// </summary> 
		/// <param name="input">the string to clean</param> 
		/// <param name="errorChar">the character which replaces bad characters</param> 
		/// <returns></returns> 
		public static string SanitizePath(string input, char errorChar)
		{
			return Sanitize(input, invalidPathChars, errorChar);
		}

		/// <summary> 
		/// Cleans a string of invalid characters. 
		/// </summary> 
		/// <param name="input"></param> 
		/// <param name="invalidChars"></param> 
		/// <param name="errorChar"></param> 
		/// <returns></returns> 
		private static string Sanitize(string input, char[] invalidChars, char errorChar)
		{
			// null always sanitizes to null 
			if (input == null) { return null; }
			StringBuilder result = new StringBuilder();
			foreach (var characterToTest in input)
			{
				// we binary search for the character in the invalid set. This should be lightning fast. 
				if (Array.BinarySearch(invalidChars, characterToTest) >= 0)
				{
					// we found the character in the array of  
					result.Append(errorChar);
				}
				else
				{
					// the character was not found in invalid, so it is valid. 
					result.Append(characterToTest);
				}
			}

			// we're done. 
			return result.ToString();
		}
	} 
}

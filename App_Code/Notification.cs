using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using Sunbelt.Security;
using System.Text;

/// <summary>
/// Summary description for Notification
/// </summary>

public class Notification
{
	public static bool EmailReportAsExcel(DataTable dtReportData, string Title, string EmailAddress, string Message, bool emailReportAttach, out string outFileWithPath, DataTable dtReportMetaData, bool formatReportXML)
    {
		string wcan = string.Empty;
		string replacement = string.Empty;
		outFileWithPath = string.Empty;
		StringBuilder attachments = new StringBuilder(string.Empty);

		try
        {
            string fileName = Title + ".csv";
			#region RegexBuddy
			// [\\/:\*\?"<>\|]
			// 
			// Match a single character present in the list below �[\\/:\*\?"<>\|]�
			//    A \ character �\\�
			//    One of the characters "/:" �/:�
			//    A * character �\*�
			//    A ? character �\?�
			//    One of the characters ""<>" �"<>�
			//    A | character �\|�
			#endregion
			fileName = Regex.Replace(fileName, "[\\\\/:\\*\\?\"<>\\|]", "");

            if (fileName.Contains(":"))
                fileName = fileName.Replace(":", "");
			if (fileName.Contains("'"))
				fileName = fileName.Replace("'", "");
			if (dtReportData.Rows.Count > 0)
            {
                // "\\fmdevapp01\c$\CommandCenterReportTemp\8743282\Invoice History EVANKO.csv"
                //DirectoryInfo subDir = Directory.CreateDirectory((HttpContext.Current.Server.MapPath("~/Temp/")) + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());

                string ReportTempServerDirectory = ConfigurationSettings.AppSettings["ReportTempServerDirectory"];
                if (!Directory.Exists(ReportTempServerDirectory))
                {
                    Sunbelt.Log.LogActivity("Report", string.Format("ReportTempServerDirectory does not exist - {0}", ReportTempServerDirectory) );
                    Sunbelt.Email.SendEmail(ConfigurationManager.AppSettings["ProductionSupportEmail"].ToString(), ConfigurationManager.AppSettings["SunbeltEmail"].ToString(), "", "", "CustomerWebService - Config Setting ReportTempServerDirectory is missing.  server: " + ConfigurationManager.AppSettings["EmailQueuePath"], "", false, "", Sunbelt.Email.CleanupType.None);
                    return false;
                }

                DirectoryInfo subDir = Directory.CreateDirectory(ReportTempServerDirectory + "\\" + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString());
                FileInfo file = new FileInfo(subDir.FullName + "\\" + fileName);
                //file = new FileInfo(@"c:\inetpub\wwwroot\CustomerWebService\Temp\7562697\" + fileName); // testing, keeps generated files in the same directory.  Will append file though


                if (formatReportXML == false) // send either CSV or XML, CSV check
				{
					System.Text.StringBuilder sb = SBNet.Data.DataTableToCSV(dtReportData);
					using (StreamWriter swWriteFile = file.CreateText())
					{
						swWriteFile.Write(sb.ToString());
					}
					attachments.Append( file.FullName );
				}

                if (formatReportXML == true )
				{
					string xmlFileFullName = Path.ChangeExtension(file.FullName, ".xml");
					using (StreamWriter swWriteFile2 = new StreamWriter(xmlFileFullName, append:false))
					{
						for (int oo = dtReportMetaData.Columns.Count - 1; oo >= 0; oo--)
							dtReportMetaData.Columns[oo].ColumnMapping = MappingType.Attribute;

						for (int oo = dtReportData.Columns.Count - 1; oo >= 0; oo--)
							dtReportData.Columns[oo].ColumnMapping = MappingType.Attribute;

						DataSet ReportMetaData = new DataSet("ReportMetaData");
						ReportMetaData.Tables.Add(dtReportMetaData);
						string reportName = dtReportMetaData.Rows[0]["ReportName"].ToString();
						DataSet dsReportData = new DataSet("ReportData");
						dtReportData.TableName = "DataRow";
						dsReportData.Tables.Add(dtReportData);

						swWriteFile2.Write("<SunbeltRentalsReport LayoutVersion=\"1.0\">");
						ReportMetaData.WriteXml(swWriteFile2, XmlWriteMode.IgnoreSchema);
						dsReportData.WriteXml(swWriteFile2, XmlWriteMode.IgnoreSchema);
						swWriteFile2.Write("</SunbeltRentalsReport>");
					}
					attachments.Append( xmlFileFullName );
				}
				// EVANKO Send email to each recipient.  Last recipient will cleanup the attachment
				Sunbelt.Email.CleanupType LastEmailCleanupTypeFlag;
				string[] sTo = Sunbelt.Email.StringToStringArray(EmailAddress);
				for (int ii = 0; ii < sTo.Length; ii++)
				{
					GenerateWCANWithEmail( Message, sTo[ii].Trim(), out wcan,  out replacement );

					LastEmailCleanupTypeFlag = (ii+1 == sTo.Length) ? Sunbelt.Email.CleanupType.AttachmentsAndFolder : Sunbelt.Email.CleanupType.None;
                    LastEmailCleanupTypeFlag = Sunbelt.Email.CleanupType.None;   //override for now, some emails were failing to find the attachments even though same report send to other recipients 


                    if (emailReportAttach == true)
					{
						//Sunbelt.Email.SendEmail(sTo[ii].Trim(), ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						//"", ConfigurationManager.AppSettings["SunbeltEmail"].ToString(), fileName.Remove(fileName.Length - 4),
						//Message.Replace(replacement, wcan), true, file.FullName, LastEmailCleanupTypeFlag);
						string sBCC = ConfigurationManager.AppSettings["SunbeltEmail"].ToString(); // + ";" + (formatReportXML ? "michael.evanko@sunbeltrentals.com" : string.Empty);

						Sunbelt.Email.SendEmail(sTo[ii].Trim(), ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						  "", sBCC, fileName.Remove(fileName.Length - 4),
						Message.Replace(replacement, wcan), true, attachments.ToString(), LastEmailCleanupTypeFlag);
					}
					else
						Sunbelt.Email.SendEmail(sTo[ii].Trim(), ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						"", ConfigurationManager.AppSettings["SunbeltEmail"].ToString(), fileName.Remove(fileName.Length - 4),
						Message.Replace(replacement, wcan), true);

				}
				outFileWithPath = file.FullName;
            }
            else
            {
				string[] sTo = Sunbelt.Email.StringToStringArray(EmailAddress);
				string[] sCC = new string[] { "" };
                string[] sBCC = new string[] { ConfigurationManager.AppSettings["SunbeltEmail"].ToString() };

				// EVANKO:  New stuff.  Iterate over emailaddress list and send each one individually
				foreach (string whoTo in sTo)
				{
					GenerateWCANWithEmail(Message, whoTo.Trim(), out wcan, out replacement);
					Sunbelt.Email.SendEmail(new string[] { whoTo.Trim() }, ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
						sCC, sBCC, fileName.Remove(fileName.Length - 4), Message.Replace(replacement, wcan), true);
				}
				// EVANKO:  New stuff.  End

				//Sunbelt.Email.SendEmail(sTo, ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
				//    sCC, sBCC, fileName.Remove(fileName.Length - 4), Message, true);
            }
            return true;
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogError(ex, "Unable to create excel file or email.  Please check Notification.cs");
            return false;
        }
    }

	private static void GenerateWCANWithEmail(string Message, string email, out string wcan, out string replacement)
	{
		int startOfQueryString = Message.LastIndexOf('{');
		int endOfQueryString = Message.LastIndexOf('}');
		replacement = Message.Substring(startOfQueryString, (endOfQueryString - startOfQueryString) + 1);
		int reportIDOffset = Message.IndexOf("ReportID=", startOfQueryString);
		string reportID = string.Empty;
		Regex regex = new Regex("[0-9]+");
		Match m = regex.Match(Message.Substring(reportIDOffset + 9, 5));
		if (m.Success == true)
			reportID = m.Value;

		SecureQueryString sqs = new SecureQueryString();
		sqs.Add("email", HttpUtility.UrlDecode(email));
		sqs.Add("reportID", HttpUtility.UrlDecode(reportID));
		wcan = "wcan=" + sqs.ToString();
	}
}

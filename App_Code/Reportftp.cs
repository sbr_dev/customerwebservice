﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;

/// <summary>
/// Summary description for ftp
/// </summary>
public class ReportFTP
{
	public string ftpServerIP { get; set; }
	public string ftpUserID { get; set; }
	public string ftpPassword { get; set; }
	public string destinationPath { get; set; }
	public string filename { get; set; }

	public ReportFTP(string ServerIP, string DestinationPath, string UserName, string Password, string SourceFullFileName)
	{ 
		ftpServerIP = ServerIP;
		destinationPath = DestinationPath;
		ftpUserID = UserName;
		ftpPassword = Password;
		filename = SourceFullFileName;
	}

	public ReportFTP()
	{
		//
		// TODO: Add constructor logic here
		//
	}
	public void SendFile()
	{

		//ftpServerIP = "ftp://70.61.47.205";
		//ftpUserID = "sunbelt";
		//ftpPassword = "j0BfpooiEtvr1zOBwQre";
		//destinationPath = @"/ReportUploads/";

		//string ftpServerIP = "ftp://sunwebapp01.sunbeltrentals.com";
		//string destinationPath = @"/invoiceReprint/";
		//string ftpUserID = "as400";
		//string ftpPassword = "Sun400";
		Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Reporting.ToString(), string.Format("FTP Start Report {0} ", filename) );  
		FileInfo fileInf = new FileInfo(filename);
		try
		{
			FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpServerIP + destinationPath + Path.GetFileName(filename));
			request.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
			request.Method = WebRequestMethods.Ftp.UploadFile;

			request.UseBinary = false;
			request.UsePassive = false;

			request.ContentLength = fileInf.Length;
			// The buffer size is set to 2kb
			int buffLength = 2048;
			byte[] buff = new byte[buffLength];
			int contentLen;
			// Opens a file stream (System.IO.FileStream) to read the file to be uploaded
			FileStream fs = fileInf.OpenRead();
			try
			{
				// Stream to which the file to be upload is written
				Stream strm = request.GetRequestStream();
				// Read from the file stream 2kb at a time
				contentLen = fs.Read(buff, 0, buffLength);
				// Until Stream content ends
				while (contentLen != 0)
				{
					// Write Content from the file stream to the FTP Upload Stream
					strm.Write(buff, 0, contentLen);
					contentLen = fs.Read(buff, 0, buffLength);
				}
				// Close the file stream and the Request Stream
				strm.Close();
				fs.Close();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Reporting.ToString(), string.Format("FTP End Report {0} ", filename));  
		}
		catch (Exception Ex)
		{
			throw Ex;
		}
	}
}

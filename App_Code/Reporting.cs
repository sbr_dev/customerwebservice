using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using Sunbelt.Common.DAL;
using SBNet.Extensions;

/// <summary>
/// Summary description for Reporting
/// </summary>
public class Reporting
{
	protected static string databaseCall;
	
	private static string csBaseData
	{
		get { return ConfigurationManager.ConnectionStrings["BaseData"].ConnectionString; }
	}
	private static string csSecurityData
	{
		get { return ConfigurationManager.ConnectionStrings["SecurityData"].ConnectionString; }
	}
	
	public static DataTable GetReportById(int ID)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportInfo.sb_SelReportMetadataByReportID", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("ReportId", ID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();
                    return dt;
                }
			}
		}
    }

        public static DataTable GetReportDetailsByReportId(int ReportId, int customerNumber = -1, int corpLinkNumber = -1, int companyID = -1)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportInfo.sb_SelReportColumnMetadata", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("ReportId", ReportId);
				if (customerNumber != -1)
					command.Parameters.AddWithValue("@CustomerNumber", customerNumber);
				if (corpLinkNumber != -1)
					command.Parameters.AddWithValue("@CorpLinkNumber", corpLinkNumber);
				if (companyID != -1)
					command.Parameters.AddWithValue(Sunbelt.CommonStrings.SqlParameterCompanyID, companyID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();
                    return dt;
                }
			}
		}
	}

	public static DataTable GetDynamicReport(string sp, List<SqlParameter> paramList, string OrderBy)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand(sp, conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.CommandTimeout = 150;
				foreach (SqlParameter p in paramList)
				{
					command.Parameters.Add(p.ParameterName, p.SqlDbType, p.Size).Value = p.Value;
				}
				if (OrderBy != "")
					command.Parameters.AddWithValue("OrderBy", OrderBy);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();			
                    return dt;
                }
			}
		}
	}

	public static DataTable GetDynamicReportByCustomerNumber(string sp, List<SqlParameter> paramList, string OrderBy, int AccountNumber, bool IsCorpLinkAccount, int CompanyID = Sunbelt.CommonValues.USCompanyID)
    {
		DataTable dt = new DataTable();
		string theDatabaseCall = string.Empty;
		try
		{
			theDatabaseCall = getDatabaseCall(sp, paramList, OrderBy, AccountNumber, IsCorpLinkAccount, CompanyID);
			using (SqlConnection conn = new SqlConnection(csBaseData))
			{
				conn.Open();
				using (SqlCommand command = new SqlCommand(sp, conn))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.CommandTimeout = 900;
					foreach (SqlParameter p in paramList)
					{
						command.Parameters.Add(p.ParameterName, p.SqlDbType, p.Size).Value = p.Value;
					}
					command.Parameters.AddWithValue("CustomerNumber", AccountNumber);
					command.Parameters.AddWithValue("IsCorpLinkAccount", IsCorpLinkAccount);
					command.Parameters.AddWithValue("CompanyID", CompanyID);


					if (OrderBy != "")
					{
						command.Parameters.AddWithValue("OrderBy", OrderBy);
					}

                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);

                        databaseCall = command.CommandAsSql();
                        //command.Dispose();
                        return dt;
                    }
				}
			}
		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, theDatabaseCall);
			//ex.Source = theDatabaseCall + ex.Source;
			throw ex;
		}
	}

	public static DataTable GetUserReportByID(int UserReportID)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(csBaseData))
            {
                conn.Open();
                using (SqlCommand command = new SqlCommand("ReportSpec.sb_SelReport", conn))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("UserReportId", UserReportID);
                    command.CommandTimeout = 900;  // 15 minutes

                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        DataTable dt = new DataTable();
                        adapter.Fill(dt);
                        return dt;
                    }
                }
            }
        }
        catch(Exception ex)
        {
			Sunbelt.Log.LogError(ex, "ReportSpec.sb_SelReport");
            throw ex;
        }
	}

	public static DataTable GetUserReportColumnsByUserReportID(int UserReportId)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportSpec.sb_SelReportColumns", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserReportId", UserReportId);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();
                    return dt;
                }
			}
		}
	}

    public static DataTable GetUserReportFiltersByUserReportID(int UserReportID)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportSpec.sb_SelReportFilters", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserReportId", UserReportID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();			
                    return dt;
                }
			}
		}
	}

    public static DataTable GetUserReportDefaultFiltersByReportID(int ReportID)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportInfo.sb_SelReportColumnFilters", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("ReportID", ReportID);
				command.Parameters.AddWithValue("DefaultFiltersOnly", true);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();			
                    return dt;
                }
			}
		}
	}

    public static DataTable GetUserReportFrequency(int ReportID)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportSpec.sb_SelReportFrequenciesByUserReportId", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserReportId", ReportID);

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    //command.Dispose();			
                    return dt;
                }
			}
		}
	}

    public static bool VerifyAccount(Guid UserID, int AccountNumber, bool IsCorpLink, int CompanyID = 1)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using( SqlCommand command = new SqlCommand("ReportSpec.sb_ValidateCustomerAccount", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserId", UserID);
				command.Parameters.AddWithValue("CustomerNumber", AccountNumber);
				command.Parameters.AddWithValue("IsCorpLinkAccount", IsCorpLink);
				command.Parameters.AddWithValue("CompanyID", CompanyID);
				SqlParameter p = command.Parameters.Add("ReturnValue", SqlDbType.Int);
				p.Direction = ParameterDirection.ReturnValue;

				command.ExecuteNonQuery();

				return Convert.ToBoolean(p.Value);
			}
		}
	}

    public static DataTable GetUserEmailAddress(Guid UserID)
    {
		using (SqlConnection conn = new SqlConnection(csSecurityData))
		{
			conn.Open();
			using( SqlCommand command = new SqlCommand("dbo.aspnet_Membership_GetUserByUserId", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserId", UserID);
				//Convert this to a SqlDbType.VarChar.
				command.Parameters.Add("CurrentTimeUtc", SqlDbType.VarChar, 10).Value = DateTime.Now.TimeOfDay.ToString();

                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    DataTable dt = new DataTable();
                    adapter.Fill(dt);
                    return dt;
                }
			}
		}
	}

	public static string GetDM_EmailAddress(int PC)
	{
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
            using (SqlCommand command = new SqlCommand("[BusinessObjects].[sb_GetProfitCenterDMInfo]", conn))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("PC", PC);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader["DMEmail"].ToString();
                    }
                }
                return null;
            }
		}
	}

	// No longer needed, use webservice GetAdditionalCCEmailAddresses() 
    //public static string GetCCEmailAddresses(int PC, int accountNumber )
    //{
    //    string ExtraCCs = Reporting.GetPC_CCEmailAddresses( PC, accountNumber );
    //    string parsedExtraCCs = string.Empty;
    //    if (!string.IsNullOrEmpty(ExtraCCs))
    //    {
    //        ExtraCCs = ExtraCCs.Replace(';', ',');
    //        string[] EmailAddresses = ExtraCCs.Split(',');
    //        for (int ii = 0; ii < EmailAddresses.Length; ii++)
    //        {
    //            if(  string.IsNullOrEmpty(EmailAddresses[ii].Trim()) == true )
    //                continue;
    //            parsedExtraCCs += "," + (Convert.ToBoolean(ConfigurationManager.AppSettings["ProductionSite"]) == false ? "QA_" : "") + EmailAddresses[ii];
    //        }
    //    }
    //    return parsedExtraCCs;
    //}

    //private static string GetPC_CCEmailAddresses(int PC, int accountNumber )
    //{
    //    using (SPData sp = new SPData("[config].[sb_GetReservationEmailsExtraCCs]", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData)))
    //    {
    //        string ExtraCCs = string.Empty;
    //        sp.AddParameterWithValue("PC", SqlDbType.Int, 4, ParameterDirection.Input, PC);
    //        if( accountNumber > 0 )
    //            sp.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input, accountNumber);
    //        SqlParameter SqlExtraCCs = sp.AddParameter("@ExtraCCs", SqlDbType.VarChar, 256, ParameterDirection.Output);
    //        DataTable dt = sp.ExecuteDataTable();

    //        ExtraCCs = SqlExtraCCs.Value.ToString();

    //        return ExtraCCs.Trim();
    //    }
    //}

    public static int ReportNotificationComplete(int ScheduleId, string Action, double runTimeInSeconds = 0 )
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportSpec.sb_SetScheduleCompleted", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("SchedulePendingID", ScheduleId);
				command.Parameters.AddWithValue("Actions", Action);
				command.Parameters.AddWithValue("ProcessingSeconds", runTimeInSeconds);

				return command.ExecuteNonQuery();
			}
		}
	}

    //This will delete everything for the report including columns and filters
    public static int DeleteReport(int UserReportID)
    {
		using (SqlConnection conn = new SqlConnection(csBaseData))
		{
			conn.Open();
			using (SqlCommand command = new SqlCommand("ReportSpec.sb_DeleteReport", conn))
			{
				command.CommandType = CommandType.StoredProcedure;
				command.Parameters.AddWithValue("UserReportId", UserReportID);

				return command.ExecuteNonQuery();
			}
		}
	}

    public static DataTable GetUserReportData(int UserReportId, bool IsCorpLinkAccount, Guid UserId, bool ShowTotals,
            int AccountNumber, List<SqlParameter> plist, DataTable dtColumns, string Sorting, int CompanyID)
    {
        DataTable dtAllData = new DataTable();
        int ReportID = 0;
        DataTable dtUser = Reporting.GetUserReportByID(Convert.ToInt32(UserReportId));
        ReportID = Convert.ToInt32(dtUser.Rows[0]["ReportId"].ToString());
		//int CompanyID = Convert.ToInt32(dtUser.Rows[0]["CompanyID"].ToString());

        DataTable dtReport = Reporting.GetReportById(ReportID);
        string sp = dtReport.Rows[0]["ProcedureFullName"].ToString();
        bool blnCustomerNumberRequired = Convert.ToBoolean(dtReport.Rows[0]["CustomerNumberRequired"].ToString());

        if (blnCustomerNumberRequired == true || AccountNumber > 0)
        {
            dtAllData = Reporting.GetDynamicReportByCustomerNumber(sp, plist, Sorting, AccountNumber, IsCorpLinkAccount, CompanyID);
        }
        else
            dtAllData = Reporting.GetDynamicReport(sp, plist, Sorting);

        bool blnExists = false;
        DataTable dt = Reporting.GetReportDetailsByReportId(ReportID);
        int iCount = 0;
        for (int i = 0; i < dtAllData.Columns.Count + iCount; i++)
        {
            //Check columns to see if they were selected.  if no rows then default to all columns.
            if (dtColumns != null)
            {
                foreach (DataRow dr in dtColumns.Rows)
                {
                    if (dr["Header"].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                        blnExists = true;
                }
            }

            //Rename the column headers.
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                if (dt.Rows[j]["Header"] != null && dt.Rows[j]["ColumnName"].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                    dtAllData.Columns[i - iCount].ColumnName = dt.Rows[j]["Header"].ToString();
            }
            if (dtColumns != null)
            {
                if (blnExists == false)
                {
                    dtAllData.Columns.RemoveAt(i - iCount);
                    iCount++;
                }
                else
                    blnExists = false;
            }

        }

        //Get the total amount for each column if the checkbox show totals is selected
        if (ShowTotals == true)
        {
			dtAllData = Reporting.GetTotals(dtAllData, ReportID, AccountNumber, CompanyID);
        }

        return dtAllData;
    }

    public static DataTable GetTotals(DataTable CurrentView, int ReportId, int AccountNumber, int companyID)
    {
		DataTable dtAllowedTotals = Reporting.GetReportDetailsByReportId(ReportId, AccountNumber, corpLinkNumber: -1, companyID: companyID);

        DataRow drTotals = CurrentView.NewRow();
        foreach (DataColumn dc in CurrentView.Columns)
        {
            foreach (DataRow drFindTotals in dtAllowedTotals.Rows)
            {
                if (drFindTotals["Header"].ToString() == dc.ColumnName)
                {
                    if (Sunbelt.Tools.ConvertTools.ToBoolean(drFindTotals["EnableTotal"], false) == true)
                    {
                        if (dc.DataType.Name == "Int32")
                        {
                            int intTotals = 0;
                            foreach (DataRow dr in CurrentView.Rows)
                            {
                                intTotals += Sunbelt.Tools.ConvertTools.ToInt32(dr[dc]);
                            }
                            drTotals[dc] = intTotals;
                        }
                        else if (dc.DataType.Name == "Decimal")
                        {
                            decimal dTotals = 0;
                            foreach (DataRow dr in CurrentView.Rows)
                            {
                                dTotals += Sunbelt.Tools.ConvertTools.ToDecimal(dr[dc]);
                            }
                            drTotals[dc] = dTotals;
                        }
                        else if (dc.DataType.Name == "Int16")
                        {
                            int intTotals = 0;
                            foreach (DataRow dr in CurrentView.Rows)
                            {
                                intTotals = Sunbelt.Tools.ConvertTools.ToInt32(dr[dc]);
                            }
                            drTotals[dc] = intTotals;
                        }
                    }
                }
            }
        }
        CurrentView.Rows.Add(drTotals);
        return CurrentView;
    }

    public static DataTable FormatDataTable(DataTable dt)
    {
        //Formatting the datatable since the gridview autogenerate columns is set to true.
        DataTable dtFormattedData = new DataTable();
        foreach (DataColumn dc in dt.Columns)
        {
            dtFormattedData.Columns.Add(dc.ColumnName);
        }

        for (int j = 0; j < dt.Rows.Count; j++)
        {
            DataRow newDataRow = dtFormattedData.NewRow();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Columns[i].DataType.Name == "Decimal")
                {
                    newDataRow[i] = String.Format("{0:$#,0.00}", dt.Rows[j].ItemArray[i]); //String.Format("{0:0,0.00;(0,0.00)}", dt.Rows[j].ItemArray[i]) // if negative and ()
                }
				else if (dt.Columns[i].DataType.Name == "Double" && dt.Columns[i].ToString().IndexOf('%') >= 0)
				{
					newDataRow[i] = String.Format("{0:P2}", dt.Rows[j].ItemArray[i]);
				}
				else if (dt.Columns[i].DataType.Name == "DateTime")
                {
                    newDataRow[i] = String.Format("{0:d}", dt.Rows[j].ItemArray[i]);
                }
                else
                {
                    newDataRow[i] = Convert.ToString(dt.Rows[j].ItemArray[i]);
                }
            }
            dtFormattedData.Rows.Add(newDataRow);
        }
        return dtFormattedData;
    }

    public static DataTable ReorderColumns(DataTable data, DataTable columns)
    {
        //Create a new DatagridTableStyle
        for (int i = 0; i < columns.Rows.Count; i++)
        {
            for (int j = 0; j < data.Columns.Count; j++)
            {
                if (data.Columns[j].ColumnName == columns.Rows[i]["Header"].ToString())
                    data.Columns[j].SetOrdinal(i);
            }
        }
        return data;
    }

	public class DateFilter
	{
		public DateFilter() { }
		public int ReportDateFilterTypeID { get; set; }
		public string TimeUnits { get; set; }
		public string DisplayLabel { get; set; }
		public int MaxRangeValue { get; set; }
		public bool AllowFullCompletedOption { get; set; }
		public bool ShowManualInput { get; set; }
	}

	public static DateFilter BuildDateFilter(SqlDataReader dr)
	{
		DateFilter df = new DateFilter();
		df.ReportDateFilterTypeID = Convert.ToInt16(dr["ReportDateFilterTypeID"]);
		df.TimeUnits = dr["TimeUnits"].ToString();
		df.DisplayLabel = dr["DisplayLabel"].ToString();
		df.MaxRangeValue = Convert.ToInt16(dr["MaxRangeValue"]);
		df.AllowFullCompletedOption = Convert.ToBoolean(dr["AllowFullCompletedOption"]);
		df.ShowManualInput = Convert.ToBoolean(dr["ShowManualInput"]);
		return df;
	}


	public static Dictionary<string, string> CalculateEffectiveDateRange(string reportDateFilterTypeID,
												   string unitCount,
												   string dateFilterFullCompletedOption,
												   string customDateRange)
	{
		Dictionary<string, string> EffectiveDateData = new Dictionary<string, string>();
		try
		{
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			{
				conn.Open();
				using (SqlCommand command = new SqlCommand("ReportSpec.sb_CalculateEffectiveDateRange", conn))
				{
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("@ReportDateFilterTypeID", Convert.ToInt32(reportDateFilterTypeID));
					command.Parameters.AddWithValue("@DateFilterUnitCount", Convert.ToInt32(unitCount));
					command.Parameters.AddWithValue("@DateFilterFullCompletedOption", Convert.ToBoolean(dateFilterFullCompletedOption));
					command.Parameters.AddWithValue("@CustomDateRange", customDateRange);
					command.Parameters.Add("@EffectiveDateRange", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output;
					command.Parameters.Add("@EffectiveDateRangeDisplay", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output;
					command.Parameters.Add("@EffectiveDateRangeDisplayWithDetails", SqlDbType.VarChar, 255).Direction = ParameterDirection.Output;

					command.ExecuteNonQuery();

					EffectiveDateData.Add("EffectiveDateRange", command.Parameters["@EffectiveDateRange"].Value.ToString());
					EffectiveDateData.Add("EffectiveDateRangeDisplay", command.Parameters["@EffectiveDateRangeDisplay"].Value.ToString());
					EffectiveDateData.Add("EffectiveDateRangeDisplayWithDetails", command.Parameters["@EffectiveDateRangeDisplayWithDetails"].Value.ToString());
				}
			}
		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem accessing report data on SQL server.");
			throw ex;
		}
		return EffectiveDateData;
	}

    static public string getDatabaseCall(string sp, List<SqlParameter> paramList, string OrderBy, int AccountNumber, bool IsCorpLinkAccount, int CompanyID)
	{
		string theDatabaseCall = "Unable to generate formatted database call";

		try
		{
			theDatabaseCall = sp;
			foreach (SqlParameter p in paramList)
				theDatabaseCall += string.Format(" @{0}='{1}',", p.ParameterName, p.Value);

			theDatabaseCall += string.Format(" @{0}={1},", "CustomerNumber", AccountNumber);
            theDatabaseCall += string.Format(" @{0}={1},", "IsCorpLinkAccount", IsCorpLinkAccount);
            theDatabaseCall += string.Format(" @{0}={1},", "CompanyID", CompanyID);

			if (OrderBy != "")
				theDatabaseCall += string.Format("@{0}='{1}'", "OrderBy", OrderBy);
		}
		catch (Exception ex)
		{
			theDatabaseCall += ex.Message;
		}


		return theDatabaseCall;
	}
}

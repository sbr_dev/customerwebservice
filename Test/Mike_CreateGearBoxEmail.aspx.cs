﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sunbelt.Queue;
using System.Configuration;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using Sunbelt.AS400;

public partial class Test_Mike_CreateReservationEmail : System.Web.UI.Page
{
    public static int  C;
    protected void Page_Load(object sender, EventArgs e)
    {
        //#if false
        //        List<CribContract> cribContractList = null;
        //        List<CribRentalReturn> cribRentalReturnList = null;
        //        List<CribSaleInvoice> cribSaleInvoiceList = null;

        //        Sunbelt.BusinessObjects.CustomerData.GetPendingCribRecords(out cribContractList, out cribRentalReturnList, out cribSaleInvoiceList);

        //        string Recipients, CCRecipients, BCCRecipients;

        //        var cribRentalReturnByBatchID = cribRentalReturnList.GroupBy(x => x.BatchID);

        //        foreach (var batchGroup in cribRentalReturnByBatchID)
        //        {
        //            List<CribRentalReturn> cribRentalReturnBatched = batchGroup.ToList();
        //            int batchid = cribRentalReturnBatched[0].BatchID;
        //            int contractnumber = cribRentalReturnBatched[0].ContractNumber;
        //            int location = cribRentalReturnBatched[0].Location;

        //            try
        //            {
        //                Sunbelt.AS400.ToolCrib.RentalReturn(cribRentalReturnBatched);
        //                GearBoxEmailAddressList GBoxEmails = Sunbelt.BusinessObjects.CustomerData.SetBatchReturnCompleted(batchid, out Recipients, out CCRecipients, out BCCRecipients);
        //                //string subjectLineContractNumbers = getSubjectLineContractSummary(cribRentalReturnBatched);  // Groups the contracts distinctly followed by the number of items for each contract ex. #34309243(3), #32...
        //                //string logMessage = string.Format("Return Rental.  {0} Items, Batch ID {1}, Contract {2}", cribRentalReturnBatched.Count, batchid.ToString(), subjectLineContractNumbers);
        //                //Sunbelt.Log.LogActivity("ToolCrib", logMessage);
        //                //Log(logMessage, true);

        //                //SendEmail(cribRentalReturnBatched.DumpCollection(true), "ToolCrib - Rental Return Created, Contract " + subjectLineContractNumbers, true);

        //                var cribRentalReturnByBatchAndContract = cribRentalReturnList.GroupBy(x => x.ContractNumber);
        //                foreach (var contractGroup in cribRentalReturnByBatchAndContract)
        //                {
        //                    List<CribRentalReturn> cribRRByContract = contractGroup.ToList();
        //                    C = cribRRByContract[0].ContractNumber;
        ////                    int offset = GBoxEmails.FindIndex((delegate(GearBoxEmailAddress e) { return (e.ContractNumber == cNumber); }));
        //                    int offset = GBoxEmails.FindIndex(FindContract); 
        //                    Recipients = GBoxEmails[offset].Recipients;
        //                    CCRecipients = GBoxEmails[offset].CCRecipients;
        //                    BCCRecipients = GBoxEmails[offset].BCCRecipients;

        //                    //SendEmailWithContract(GearBox_TransactionType.RentalContract,
        //                    //    cribRRByContract[0].CustomerNumber,
        //                    //    cribRRByContract[0].ContractNumber,
        //                    //    batchid,
        //                    //    cribRRByContract[0].Location,
        //                    //    cribRRByContract[0].ReturnedBy,
        //                    //    Recipients, CCRecipients, BCCRecipients);
        //                }
        //            }
        //            catch (CribException cribException)
        //            {
        //                string logMessage = string.Format("*** ToolCrib Exception - Return Rental.  Items {0}, Contract {1}, BatchId {2}, TransactionId {3}", cribRentalReturnBatched.Count, contractnumber.ToString(), cribException.batchId, cribException.transactionId);
        //                Sunbelt.Log.LogError(cribException, logMessage);
        //                //Log(logMessage, true);

        //               // List<CribRentalReturn> rr = cribRentalReturn.Where(t => t.TransactionID == cribException.transactionId).ToList();
        //                //SendEmail(cribException.Message + HTML_BREAK + rr[0].DumpToHtml(true), "*** ToolCrib Exception - Return Rental", false);
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //        }
        //       // Log("End of ProcessCribReturnRentals()", true);

        //    }

        //#else
        QueueMailMessage q;
        string NewReservationQueuePath = @"FormatName:DIRECT=OS:SUNWEBAPPDEV\PRIVATE$\SunbeltReservationQueue";
        WebService ws = new WebService();

        string email = "michael.evanko@sunbeltrentals.com";

        //  sales Invoice
        Int32 batchId = 344;
        Int32 location = 833;
        Int32 accountNumber = 35;
        Int32 contractID = 32660260; 

         //contractID = 32660257;  -- yes
        // 32660258 -- yes

         int contractOrInvoiceSequenceNumber = 0;

        
        string user = "Wagner, David";
        GearBox_TransactionType transType = GearBox_TransactionType.RentalReturn;
        q = ws.CreateGearBoxEmail(transType,
                                    accountNumber,
                                    contractID,
                                    batchId,
                                    location,
                                    "\\\\SUNWEBAPPDEV\\InvoiceReprint\\" + contractID.ToString() + "00" + contractOrInvoiceSequenceNumber.ToString() + ".pdf",
                                    user);
        q.To = "michael.evanko@sunbeltrentals.com";
        q.From = ConfigurationManager.AppSettings["EService_Email"];
        q.Label = string.Format("{0} {1} {2}-{3} - {4}", "GearBox", transType.ToString(), contractID, contractOrInvoiceSequenceNumber, DateTime.Now.ToString());

        WebReservationQueueMessage wrqm = new WebReservationQueueMessage(string.Format("GearBox  {0} - {1}  {2}", contractID, 0, DateTime.Now.ToString()), //label
                                                                            WebReservationSource.SunbeltRentalsWebSite,                                  //source
                                                                            contractID,     // contractNumber
                                                                            contractOrInvoiceSequenceNumber,              // sequenceNumber
                                                                            contractID,     // reservationNumber
                                                                            WebReservationQueueMessage.DocumentType.Contract, //docType
                                                                            contractID.ToString(), // referenceNumber
                                                                            "mike", //placedBy
                                                                            88,     //pc
                                                                            0,      // salesRep
                                                                            email,   //emailAddress
                                                                            "SUNWEBAPPDEV",
                                                                            "InvoiceReprint",
                                                                            q);
        wrqm.Label = string.Format("{0} {1} {2}-{3} - {4}", "GearBox", transType.ToString(), contractID, contractOrInvoiceSequenceNumber, DateTime.Now.ToString());
        WebReservationQueue.SendReservation(wrqm, NewReservationQueuePath);
    }                // Explicit predicate delegate.
        private static bool FindContract(GearBoxEmailAddress gBoxEmail )
        {

            if (gBoxEmail.ContractNumber == C)
            {
                return true;
            }
            {
                return false;
            }

        }

        //(delegate(GearBoxEmailAddress e) { return (e.ContractNumber == cNumber); });
   
}

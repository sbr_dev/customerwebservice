﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sunbelt.Queue;
using System.Configuration;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using Sunbelt.AS400;
using System.Data;
using System.Data.SqlClient;

using SBNetWebControls;
using SBNet;

public partial class Test_Mike_CreateReservationEmail : System.Web.UI.Page
{
    public static int  C;
	private const string EMAIL_UNSENT = "Unsent";
	public static bool isCustomerEquipment = false;


    protected void Page_Load(object sender, EventArgs e)
    {
		//SBNet.Tools.IncludeSBNetClientScript(this);
         if (!Page.IsPostBack)
        {
			
        }
		// btnSendEmail.Attributes.Add("onclick", "javascript:retrun alertPrompt('Batch ID must be entered and loaded.');");

    }  
  
    private void SendEmailWithContract(GearBox_TransactionType gearBox_TransactionType,
                                    Int32 accountNumber,
                                    Int32 contractNumber,
                                    int batchId,
                                    int location,
                                    string user,
                                    string Recipients, string CCRecipients, string BCCRecipients, int InvoiceOrContractSequenceNumber, bool isCustomerEquipment)
    {
        //gearBox_TransactionType = GearBox_TransactionType.RentalContract;
        //accountNumber = 4226;
        //location = 833;
        //contractNumber = 32660269;
        //batchId = 352;
        //user = "Wagner, David";
        //InvoiceOrContractSequenceNumber = 0;

        try
        {
            WebReservationQueueMessage.DocumentType documentType = WebReservationQueueMessage.DocumentType.Contract;
            if (gearBox_TransactionType != GearBox_TransactionType.RentalContract)
                documentType = WebReservationQueueMessage.DocumentType.Invoice;

            string logmessage = String.Format("GearBox_TransactionType: {0}, accountNumber: {1},  contractNumber: {2}-{3}, batchId: {4}, location: {5}, user: {6}, documentType: {7}",
                                                            gearBox_TransactionType, accountNumber, contractNumber, InvoiceOrContractSequenceNumber, batchId, location, user, documentType);
            Sunbelt.Log.LogActivity("GearBox", logmessage);
            //Log(logmessage, true);

            string EmailsForTesting = string.Empty;
            if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "false")
                EmailsForTesting = ConfigurationManager.AppSettings["EmailsForTesting"];

            string pdfPathWithFilename = string.Format("\\\\{0}\\{1}\\{2:D8}{3:D3}.pdf", ConfigurationManager.AppSettings["FTPServer"], ConfigurationManager.AppSettings["PDFShare"], contractNumber, InvoiceOrContractSequenceNumber);

            WebService customerWebService = new WebService();
            QueueMailMessage queueMailMessage = customerWebService.CreateGearBoxEmail((GearBox_TransactionType)gearBox_TransactionType,
                                                                                                                    accountNumber,
                                                                                                                    contractNumber,
                                                                                                                    batchId,
                                                                                                                    location,
                                                                                                                    pdfPathWithFilename,
                                                                                                                    user,
                                                                                                                    isCustomerEquipment);
            queueMailMessage.From = ConfigurationManager.AppSettings["EmailNoReply"];
            queueMailMessage.To =  tbRecipients.Text; //  = EmailsForTesting + "," + customerWebService.EmailAddressQAAdjuster(Recipients);
            queueMailMessage.CC =  tbCCRecipients.Text; // customerWebService.EmailAddressQAAdjuster(CCRecipients);
			queueMailMessage.BCC = tbBCCRecipients.Text + "," + ConfigurationManager.AppSettings["EmailNoReply"]; // customerWebService.EmailAddressQAAdjuster(BCCRecipients) + "," + ConfigurationManager.AppSettings["EmailNoReply"] + "," + ConfigurationManager.AppSettings["AdditionalBCCAddresses"] + "," + ConfigurationManager.AppSettings["EmailSuccessAdmin"];
						                                       

            queueMailMessage.Label = string.Format("{0} {1} {2}-{3:D3} - {4}", "GearBox", gearBox_TransactionType.ToString(), contractNumber, InvoiceOrContractSequenceNumber, DateTime.Now.ToString());

			WebReservationQueueMessage wrqm = new WebReservationQueueMessage(string.Format("Gearbox  {0}-{1}  {2}", contractNumber, 0, DateTime.Now.ToString()),   //label
                                                                    WebReservationSource.SunbeltRentalsWebSite, //source  - WebReservationSource.SunbeltRentalsWebSite
                                                                    contractNumber, // contractNumber
                                                                    InvoiceOrContractSequenceNumber, // sequenceNumber
                                                                    contractNumber,      // reservationNumber
                                                                    documentType, //docType PDF Request service will need to be updated to handle GearBox requests.
                                                                    contractNumber.ToString(), // referenceNumber
                                                                    user, //placedBy
                                                                    ConvertTools.ToInt32(location), //pc
                                                                    0, // salesRep
                                                                    "",   //emailAddress  none here, all in the queueMailMessage returned from CreateGearBoxEmail()un
                                                                    ConfigurationManager.AppSettings["FTPServer"],
                                                                    ConfigurationManager.AppSettings["PDFShare"],
                                                                    toSunbeltQueueMessage(queueMailMessage));
            wrqm.Label = queueMailMessage.Label;
			if (isCustomerEquipment == true)
				Sunbelt.Email.SendEmail(toSunbeltQueueMessage(queueMailMessage));
			else
				WebReservationQueue.SendReservation(wrqm, ConfigurationManager.AppSettings["ReservationQueuePath"]);
			Sunbelt.BusinessObjects.CustomerData.SetBatchEmailSent(batchId);
			btnSendEmail.Enabled = false;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private QueueMailMessage toSunbeltQueueMessage( QueueMailMessage inQ)
    {
        QueueMailMessage outQ = new QueueMailMessage(inQ.Label, inQ.To, inQ.From, inQ.CC, inQ.BCC,
                                                     inQ.Subject, inQ.Body, inQ.AsHTML, inQ.Attachment,
                                                     (QueueMailMessage.CleanupType)inQ.CleanupAttachments);

        return outQ;
    }

    private static bool FindContract(GearBoxEmailAddress gBoxEmail )
    {

        if (gBoxEmail.ContractNumber == C)
        {
            return true;
        }
        {
            return false;
        }

    }

    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        GearBox_TransactionType gearBox_TransactionType;
        Int32 accountNumber;
        Int32 contractNumber;
        int batchId;
        int location;
        string user;
        string Recipients, CCRecipients, BCCRecipients;
        int InvoiceOrContractSequenceNumber;


        gearBox_TransactionType = (GearBox_TransactionType)Convert.ToInt32(ddlGearBoxTransType.SelectedValue);
        accountNumber = Convert.ToInt32(tbAccountNumber.Text);
        contractNumber = Convert.ToInt32(tbContractNumber.Text);
        batchId = Convert.ToInt32(tbBatchId.Text);
        location = Convert.ToInt32(tbLocation.Text);
        user = tbUser.Text;


        Recipients = (tbRecipients.Text == "none") ? "" : tbRecipients.Text;
        CCRecipients = (tbCCRecipients.Text == "none") ? "" : tbCCRecipients.Text;
        BCCRecipients = (tbBCCRecipients.Text == "none") ? "" : tbBCCRecipients.Text;

        InvoiceOrContractSequenceNumber = Convert.ToInt32(tbInvoiceOrContractSequenceNumber.Text);

        SendEmailWithContract(gearBox_TransactionType,
                               accountNumber,
                               contractNumber,
                               batchId,
                               location,
                               user,
							   Recipients, CCRecipients, BCCRecipients, InvoiceOrContractSequenceNumber, isCustomerEquipment);
    }
    protected void btnLoadBatch_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtBatchIDToLoad.Text) == true)
            return;
        
        Int32 batchID = Convert.ToInt32( txtBatchIDToLoad.Text.Trim() );
        LoadDetailsOfBatch(batchID);
    }



    public void LoadDetailsOfBatch(int batchID)
    {
        //Recipients = string.Empty;
        //CCRecipients = string.Empty;
        //BCCRecipients = string.Empty;

        DataTable dt = new DataTable();
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CribMasterCustom"].ConnectionString))
        {
            conn.Open();

            SqlCommand command = new SqlCommand("Crib.GetBatchInfo", conn);
            command.CommandType = CommandType.StoredProcedure;

            command.Parameters.AddWithValue("@PendingBatchID ", batchID);

            SqlDataAdapter da = new SqlDataAdapter(command);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                bool isReturn = Convert.ToBoolean(dt.Rows[0]["isReturn"]);
                string ContractType = dt.Rows[0]["ContractType"].ToString();
                switch (ContractType)
                {
                    case "Rental":
                        if( isReturn == true )
                            ddlGearBoxTransType.SelectedIndex = (int)GearBox_TransactionType.RentalReturn;
                        else
                            ddlGearBoxTransType.SelectedIndex = (int)GearBox_TransactionType.RentalContract;
                        break;
                    case "Sales":
                        ddlGearBoxTransType.SelectedIndex = (int)GearBox_TransactionType.SalesInvoice;
                        break;
                }

                tbAccountNumber.Text = dt.Rows[0]["CustomerNumber"].ToString();
                tbContractNumber.Text = dt.Rows[0]["ContractNumber"].ToString();
                tbBatchId.Text = dt.Rows[0]["PendingBatchID"].ToString();
                tbLocation.Text = dt.Rows[0]["PC"].ToString();
                tbUser.Text = dt.Rows[0]["OrderedBy"].ToString();
				isCustomerEquipment = Convert.ToBoolean(dt.Rows[0]["IsCustomerEquipment"]);

                WebService customerWebService = new WebService();

				tbRecipients.Text =  customerWebService.EmailAddressQAAdjuster(dt.Rows[0]["Recipients"].ToString());
				tbCCRecipients.Text = customerWebService.EmailAddressQAAdjuster(dt.Rows[0]["CCRecipients"].ToString());
				tbBCCRecipients.Text =  customerWebService.EmailAddressQAAdjuster(dt.Rows[0]["BCCRecipients"].ToString())
					                                        + "," + ConfigurationManager.AppSettings["AdditionalBCCAddresses"]
				                                        + "," + ((isCustomerEquipment == true) ? ConfigurationManager.AppSettings["AdditionalBCCAddressesCustomerEqpEmail"] : ConfigurationManager.AppSettings["AdditionalBCCAddressesSunbeltEqpEmail"]);
				tbBCCRecipients.Text = HttpUtility.HtmlEncode(tbBCCRecipients.Text);

                lblEmailSent.Text = dt.Rows[0]["EmailSent"].ToString();
                tbInvoiceOrContractSequenceNumber.Text = dt.Rows[0]["SequenceNumber"].ToString();
				bool wasEmailSent = !string.IsNullOrEmpty(dt.Rows[0]["EmailSent"].ToString());
				lblEmailSent.Text = (wasEmailSent == true ? dt.Rows[0]["EmailSent"].ToString() : EMAIL_UNSENT);
				btnSendEmail.Enabled = true; // !wasEmailSent;
			}
        }
    }
}

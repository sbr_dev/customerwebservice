﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendGearBoxEmail.aspx.cs" Inherits="Test_Mike_CreateReservationEmail"  ValidateRequest="false" %>

<%--<%@ Register TagPrefix="Sunbelt" Namespace="SBNetWebControls" Assembly="Sunbelt.Library" %>
--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script language="javascript" type="text/javascript">
	//<![CDATA[
	function openWindow(theURL, windowName, theWidth, theHeight) {
		var winl = (screen.width - theWidth) / 2;
		var wint = (screen.height - theHeight) / 2;
		window.open(theURL, windowName, 'scrollbars=yes,directories=no,resizable=no,toolbar=no,menubar=no,fullscreen=no,titlebar=no,status=no,location=no,width=' + theWidth + ',height=' + theHeight + ',top=' + wint + ',left=' + winl + '');
	}

	function newwindow() {
		var load = window.open('http://qaweb.sunbeltrentals.com/equipment/shiftrates.aspx', 'mywindow', 'width=550,height=475,toolbar=no,location=yes,directories=yes,status=yes,menubar=no,scrollbars=yes,copyhistory=yes,resizable=yes');
	}
	function SendEmail_EmailHasBeenSent() {
		SBNetWebControls.Util.ConfirmBox("Send Email", "Email has already been sent.", null, null, "okonly");
		return false;
	}
	function LoadBatch_MissingBatchID() {
		SBNet.Util.ConfirmBox("Load Batch", "Enter Batch ID.", null, null, "okonly");
		return false;
	}

	function alertPrompt( message ) {
		alert("Enter and Load a Batch ID .");
		return false;
	}

	//]]>	
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 40px">
        <table style="width: 886px">
            <tr>
                <td valign="middle" ><asp:Label ID="lblBatchid" runat="server" Text="Batch ID: "/>&nbsp;
                    <asp:TextBox ID="txtBatchIDToLoad" runat="server" />&nbsp;&nbsp;
                    <asp:Button ID="btnLoadBatch" runat="server" Text="Load Batch" onclick="btnLoadBatch_Click" /></td>
            </tr>
        </table>
   </div>
   
    <div style="height: 310px">
    <table style="width: 786px">
        <tr>
            <td><asp:Label ID="Label1" runat="server" Text="Label"/></td>
            <td><asp:DropDownList ID="ddlGearBoxTransType" runat="server">
                        <asp:ListItem Text="RentalContract" Value="0"></asp:ListItem>
                        <asp:ListItem Text="RentalReturn" Value="1"></asp:ListItem>
                        <asp:ListItem Text="SalesInvoice" Value="2"></asp:ListItem>
                    </asp:DropDownList></td>
        </tr>
         <tr>
            <td><asp:Label ID="Label2" runat="server" Text="Account Number: "/></td>
            <td><asp:TextBox ID="tbAccountNumber" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
            <td><asp:Label ID="Label3" runat="server" Text="Contract Number: "/></td>
            <td><asp:TextBox ID="tbContractNumber" runat="server"></asp:TextBox></td>
        </tr>       
         <tr>
            <td><asp:Label ID="Label4" runat="server" Text="Batch ID: "/></td>
            <td><asp:TextBox ID="tbBatchId" runat="server"></asp:TextBox></td>
        </tr>       
         <tr>
            <td><asp:Label ID="Label5" runat="server" Text="Location: "/></td>
            <td><asp:TextBox ID="tbLocation" runat="server"></asp:TextBox></td>
        </tr>       
         <tr>
            <td><asp:Label ID="Label6" runat="server" Text="User: "/></td>
            <td><asp:TextBox ID="tbUser" runat="server"></asp:TextBox></td>
        </tr>  
         <tr>
            <td><asp:Label ID="Label7" runat="server" Text="Recipients: "/></td>
            <td><asp:TextBox ID="tbRecipients" runat="server" Width="533px"></asp:TextBox></td>
        </tr>  
         <tr>
            <td><asp:Label ID="Label8" runat="server" Text="CCRecipients: "/></td>
            <td><asp:TextBox ID="tbCCRecipients" runat="server" Width="533px"></asp:TextBox></td>
        </tr>  
         <tr>
            <td><asp:Label ID="Label9" runat="server" Text="BCCRecipients: "/></td>
            <td><asp:TextBox ID="tbBCCRecipients" runat="server" Width="533px"></asp:TextBox></td>
        </tr>  
         <tr>
            <td><asp:Label ID="Label10" runat="server" Text="InvoiceOrContractSequenceNumber: "/></td>
            <td><asp:TextBox ID="tbInvoiceOrContractSequenceNumber" runat="server" Width="42px"></asp:TextBox></td>
        </tr>  
          <tr>
            <td><asp:Label ID="lblEmail" runat="server" Text="Email Sent: "/></td>
            <td><asp:Label ID="lblEmailSent" runat="server" /></td>
        </tr>        
         <tr>
            <td>&nbsp;</td>
            <td><asp:Button ID="btnSendEmail" runat="server" Text="Send Emal" 
                    onclick="btnSendEmail_Click" Enabled="false" /></td>
        </tr>        
        
               
        
        

    </table>
    
        
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Common.DAL;

public partial class ReservationRequest : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

	public static string ProperCase(string Input)
	{
		return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Input.ToLower()); 
	}

    public void SetOrderItemsGridview(Int64 OrderId)
    {
        DataTable dt = new DataTable();
        using (SPData sp = new SPData("dbo.ORDERS_GetOrderItems", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.Internet_BaseData)))
        {
            sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 5, ParameterDirection.Input, OrderId);
            dt = sp.ExecuteDataTable();
        }

        string sName = "";
		string ReservationID = "0";
        if (dt.Rows.Count > 0)
        {
			sName = ProperCase(dt.Rows[0]["UserName"].ToString());
            ReservationID = dt.Rows[0]["ContractNumber"].ToString();
        }

        lblRequestConfirmation.Text = ReservationID;
        lblRequestConfirmation2.Text = ReservationID;

        if (!string.IsNullOrEmpty(sName))
        {
            sName = sName.TrimStart(' ');
            if (sName.IndexOf(" ") != -1)
                lblRequestorName.Text = sName.Remove(sName.IndexOf(" ")) + ", thank";
            else
                lblRequestorName.Text = "Thank";
        }
        else
        {
            lblRequestorName.Text = "Thank";
        }

        gvItems.DataSource = dt;
        gvItems.DataBind();
    }
}

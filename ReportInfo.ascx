﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportInfo.ascx.cs" Inherits="ReportInfo" %>

<%
    string CustomerServiceEmail = string.Empty;
    if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true")
        CustomerServiceEmail = "commandcenter@sunbeltrentals.com";
    else
        CustomerServiceEmail = "qanoreply@sunbeltrentals.com";
%>

<div style="width: 100%; font-family: Tahoma; font-size:small">This is an auto-generated report sent from 
    www.sunbeltrentals.com.  If you feel you have received this in error please contact Sunbelt Rentals Customer Service at <a href="tel:1-866-786-2358">1-866-SUNBELT</a>, 
or by emailing <%=@CustomerServiceEmail %>.</div>
<br /><br />
<div id="header" runat="server" style="width: 100%; background-image: url(http://customer.sunbeltrentals.com/App_Themes/Main/Images/dlg_gr_titlebar.png);
     background-color: #7e7d7d; font-size: 12pt; height: 25px;" visible="false">
&nbsp;<a href="http://beta.sunbeltrentals.com/AccountReports/ReportBuilder/DynamicReporting.aspx" style="color: White; font-weight: bold; padding-bottom: 5px; vertical-align: middle; text-decoration: none;">Sunbelt Rentals</a></div>
<table id="tblReportDetail" class="tblDetailInfo" cellpadding="0" cellspacing="8" border="0" style=" width: 100%; background-color: #f3f3ee; border: solid 1px #EBEBEB;">
<tr>
        <td style="vertical-align: top;">
            <table cellpadding="2">
            <tr>
            <td colspan="2">
             <asp:Label ID="lblReportName" runat="server" Text="" Font-Bold="true" Font-Size="12pt" Font-Names="Arial" /><asp:label ID="Label1" ForeColor="#f3f3ee" Font-Size="XX-Small" runat="server">ReportGridView</asp:label><br />
                    <asp:GridView ID="gvGridView" CellPadding="2" Font-Size="XX-Small" EnableViewState="true" runat="server"  Font-Names="Tahoma"
				            AllowSorting="false" ShowFooter="false" AutoGenerateColumns="true" SkinID="Gray" BorderColor="DarkGray" GridLines="Both" ForeColor="Black">
		            </asp:GridView>
             </td>
            </tr>
            </table>
        </td>
    </tr>
</table>
<div style="font-family: Tahoma; font-size:smaller"><br />Report generated <asp:Label ID="lblNumberOfRecords" runat="server" Text="" Font-Names="Arial" /> records.</div>
<div style="font-family: Tahoma; font-size:smaller">Filters Applied:&nbsp;<asp:Label ID="lblFilters" runat="server" Text="" Font-Names="Arial" /></div>
<%--<div style="font-family: Tahoma; font-size:smaller">Reference ID:&nbsp;<asp:Label ID="lblReportID" runat="server" Text="" Font-Names="Arial" /></div>--%>

<p style='font-family: Arial Narrow, Sans-Serif; font-size: 10pt;  color: #666666;'>To discontinue this Sunbelt Rentals Report, click
	<asp:HyperLink NavigateUrl="[SERVERURL]/customerservice/unsubscribe/?{ReportID=[REPORTID]&email=[EMAIL]}" runat="server" ID="hlUnsubscribe">unsubscribe</asp:HyperLink>.</p>

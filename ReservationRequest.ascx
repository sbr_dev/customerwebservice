﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReservationRequest.ascx.cs" Inherits="ReservationRequest" %>
<%
    string CustomerServiceEmail = string.Empty;
    if (ConfigurationManager.AppSettings["ProductionSite"].ToLower() == "true")
        CustomerServiceEmail = "commandcenter@sunbeltrentals.com";
    else
        CustomerServiceEmail = "qanoreply@sunbeltrentals.com";
%>
<div style="font-family: Verdana, Arial; padding-left: 10px; width: 630px;">
    <p style="border-bottom: solid 5px #ebebeb; font-size: 10pt; width: 630px; font-family: Verdana, Arial;">
        <table style="font-family: Verdana, Arial;">
            <tr>
                <td rowspan="2"><asp:Image ID="img" runat="server" ImageUrl="http://customer.sunbeltrentals.com/MasterPages/Controls/LeftNav/Images/leftnav_sblogo.gif" /></td>
                <td style="font-size: 14pt; font-weight: bold;">RESERVATION REQUEST</td>
            </tr>
            <tr>
                <td style="font-size: 12pt; color: #666666;">CONFIRMATION EMAIL</td>
            </tr>
        </table>
    </p>
    <p style="font-size: 10pt; width: 630px; font-family: Verdana, Arial;">
        <asp:Label ID="lblRequestorName" runat="server" Text="John" Font-Names="Verdana" /> you for submitting your Reservation Request to Sunbelt Rentals!
    </p>
    <p style="font-size: 10pt; width: 630px; font-family: Verdana, Arial;">
        Your <b>Reservation Request</b> has been received by Sunbelt Rentals and is currently being <br />
        processed. A member of our team will contact you within <b style="font-style: italic;">
        2 business hours</b> with confirmation <br />
        of your reservation.</p>
    <p style="font-size: 10pt; width: 630px; font-family: Verdana, Arial;">
        <table>
            <tr>
                <td style="font-family: Verdana, Arial; font-weight: bold; font-size: 10pt;">RESERVATION REQUEST SUMMARY</td>
                <td style="font-family: Verdana, Arial; color: Red; font-size: 10pt; font-weight: bold;">Request #: <asp:Label ID="lblRequestConfirmation2" runat="server" Text="00000000" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="false" style="margin-right: 5px; width: 630px;">
                        <HeaderStyle BackColor="#4A843D" ForeColor="White" Font-Size="10pt" Font-Names="Verdana" HorizontalAlign="Center" />
                        <RowStyle HorizontalAlign="Center" BackColor="#F2F8F1" Font-Names="Verdana" Font-Size="9pt" />
                        <AlternatingRowStyle BackColor="White" Font-Size="9pt" Font-Names="Verdana" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>Rental Item</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblTitle" runat="server" Font-Names="Arial" Text='<%#Eval("Title")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>&nbsp;Qty&nbsp;</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblQty" runat="server" Font-Names="Arial" Text='<%#Eval("Quantity")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Rental Start</div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblQty" runat="server" Font-Names="Arial" Text='<%#Eval("StartDate")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>Estimated Return</HeaderTemplate>
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="lblQty" runat="server" Font-Names="Arial" Text='<%#Eval("EndDate")%>' style="padding-left: 2px; padding-right: 2px;"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 7.5pt; font-family: Verdana, Arial;">*Rental Rates are for a single 8-hour shift per day or 40-hours per week, additional charges may apply.</td>
            </tr>
        </table>
    </p>
    <p style="font-size: 10pt; text-align: left; font-family: Verdana, Arial; width: 630px;">
        Quoted delivery and pick up fees are estimates and provided for convenience only.  Actual delivery charges will be quoted when the order is confirmed  based on the 
        rental location.</p>
    <p style="font-size: 10pt; text-align: left; font-family: Verdana, Arial; width: 630px;">
        If you have any questions, or you have received this message in error, please contact <br /> Sunbelt Rentals Customer Service:
        <table style="font-size: 10pt; text-align: left; font-family: Verdana, Arial;">
            <tr>
                <td style="width: 100px;">Toll Free:</td>
                <td style="font-weight: bold;"><a href="tel:1-866-786-2358">1-866-SUNBELT</a></td>
            </tr>
            <tr>
                <td style="width: 100px;">Email:</td>
                <td style="font-weight: bold;"><a href="mailto:<%=@CustomerServiceEmail %>">Customer Service</a></td>
            </tr>
        </table>
    </p>
    <p style="font-size: 10pt; text-align: left; font-family: Verdana, Arial; width: 630px;">
        Please refer to your reservation when contacting us:
        <br />
        Request #: <asp:Label ID="lblRequestConfirmation" runat="server" Text="00000000" />
        <br /><br />
    </p>
    <p>
        <table style="font-size: 10pt; text-align: left; font-family: Verdana, Arial; width: 630px;">
            <tr>
                <td style="color: Red;">PLEASE NOTE: <br /><%=ConfigurationManager.AppSettings["AutomatedEmailResponse"].ToString()%></td>
                <td style="font-weight: bold; text-align: right;"><asp:Hyperlink ID="hlAdobe" NavigateUrl="http://www.adobe.com/products/acrobat/readstep2.html" runat="server" ImageUrl="http://customer.sunbeltrentals.com/Images/get_adobe_reader.gif" /></td>
            </tr>
        </table> 
    </p>
</div>